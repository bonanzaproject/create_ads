// LIBRARY
import fs from 'fs';

// SERVICES
import routes from './routes.js';
/* tiktok */
import get_tk_identities from '../services/tiktok/get_identities.js';
import create_tk_identity from '../services/tiktok/create_identity.js';
import get_tk_locations from '../services/tiktok/get_locations.js';
import get_tk_languages from '../services/tiktok/get_languages.js';
import create_tk_creative from '../services/tiktok/create_creative.js';
import get_tk_video_infos from '../services/tiktok/get_video_infos.js';
/* snapchat */
import get_sp_countries from '../services/snapchat/get_countries.js';
import get_sp_languages from '../services/snapchat/get_languages.js';
import sp_refresh_token from '../services/snapchat/refresh_token.js';
import get_sp_locations_country from '../services/snapchat/get_locations_country.js';
import upload_sp_image from '../services/snapchat/upload_image.js';
import upload_sp_video from '../services/snapchat/upload_video.js';
/* facebook */
import get_fb_places from '../services/facebook/get_places.js';
import get_fb_locales from '../services/facebook/get_locales.js';
import upload_fb_image from '../services/facebook/upload_image.js';
import upload_fb_video from '../services/facebook/upload_video.js';
/* twitter */
import get_tw_locations from '../services/twitter/get_locations.js';
import get_tw_languages from '../services/twitter/get_languages.js';
import upload_tw_chunked_file from '../services/twitter/upload_chunked_file.js';

// RESSOURCES
import parse_body from '../ressources/parse_body.js';
import console_log from '../ressources/console_log.js';

// CONST
import access from '../const/access.js';

const router = async (req) => {
  try {
    let body_parsed;
    let tmpFolders = [];

    // Check if we have a method with param in routes
    const route_requested = routes.filter((route) => {
      if (
        route.method === req.method
        && Object.keys(req.params).length === 1
        && req.params['0'] === route.param
      ) {
        return route;
      }
      return false;
    });

    // Error if no route found
    if (route_requested.length === 0) {
      throw {
        expose: true,
        error_detail: `Route ${req.method}: /${req.params['0']} is not defined`,
      };
    }
    // Error if route_requested is more than 1
    if (route_requested.length > 1) {
      throw {
        expose: false,
        error_detail: `Several route_requested found: ${route_requested}`,
      };
    }

    // Check parameters (queries)
    if (route_requested[0].method === 'GET') {
      // Check if number parameter is correct
      if (Object.keys(req.query).length !== route_requested[0].queries.length) {
        throw {
          expose: true,
          error_detail: `Schema parameters for route ${req.method} /${req.params['0']} is not respected`,
        };
      }

      // Check for each param if it undefined or empty
      const params_check = route_requested[0].queries.filter(
        (query) => !req.query[query] || req.query[query] === '',
      );

      // Error if params_check is not good
      if (params_check.length !== 0) {
        console_log('Error with parameters: ', params_check.join());
        throw {
          expose: true,
          error_detail: `Missing or wrong parameters for route ${req.method} /${req.params['0']}`,
        };
      }

      console_log(
        `Route requested ${req.method} /${req.params['0']}`,
        req.query,
      );
    } else if (route_requested[0].method === 'POST') { // Check body parameters
      try {
        const params = await parse_body(req);
        body_parsed = params.body_parsed;
        tmpFolders = params.tmpFolders;
      } catch (e) {
        throw { expose: false, error_detail: e };
      }

      // Check if number parameter in body is correct
      if (Object.keys(body_parsed).length !== route_requested[0].body.length) {
        throw {
          expose: true,
          error_detail: `Schema parameters for route ${req.method} /${req.params['0']} is not respected`,
        };
      }

      // Check for each param if it undefined or empty
      const body_check = route_requested[0].body.filter(
        (query) => !body_parsed[query] || body_parsed[query] === '',
      );

      // Error if params_check is not good
      if (body_check.length !== 0) {
        console_log('Error with parameters: ', body_check.join());
        throw {
          expose: true,
          error_detail: `Missing or wrong body for route ${req.method} /${req.params['0']}`,
        };
      }

      console_log(
        `Route requested ${req.method} /${req.params['0']}`,
        body_parsed,
      );
    } else {
      throw {
        expose: true,
        error_detail: 'You\'re tring forbidden method request..',
      };
    }

    let job;

    try {
      switch (route_requested[0].param) {
        case 'get_tk_identities':
          job = await get_tk_identities(
            req.query.advertiser_id,
            req.query.access_token,
          );
          break;
        case 'create_tk_identity':
          job = await create_tk_identity(
            body_parsed.avatar_name,
            body_parsed.avatar,
            body_parsed.md5,
            body_parsed.advertiser_id,
            body_parsed.access_token,
          );
          break;
        case 'get_tk_locations':
          job = await get_tk_locations(
            req.query.advertiser_id,
            req.query.access_token,
          );
          break;
        case 'get_tk_languages':
          job = await get_tk_languages(
            req.query.advertiser_id,
            req.query.access_token,
          );
          break;
        case 'create_tk_creative':
          job = await create_tk_creative(
            body_parsed.video_name,
            body_parsed.video,
            body_parsed.md5,
            body_parsed.advertiser_id,
            body_parsed.access_token,
          );
          break;
        case 'get_sp_countries':
          job = await get_sp_countries(req.query.access_token);
          break;
        case 'get_sp_languages':
          job = await get_sp_languages(req.query.access_token);
          break;
        case 'sp_refresh_token':
          job = await await sp_refresh_token(
            access.snapchat.client_id,
            access.snapchat.client_secret,
            req.query.refresh_token,
          );
          break;
        case 'get_sp_locations_country':
          job = await await get_sp_locations_country(
            req.query.country_code,
            req.query.access_token,
          );
          break;
        case 'upload_sp_image':
          job = await await upload_sp_image(
            body_parsed.image,
            body_parsed.ad_account_id,
            body_parsed.access_token,
          );
          break;
        case 'get_fb_places':
          job = await await get_fb_places(
            req.query.search,
            req.query.access_token,
          );
          break;
        case 'get_fb_locales':
          job = await await get_fb_locales(
            req.query.search,
            req.query.access_token,
          );
          break;
        case 'upload_fb_image':
          job = await await upload_fb_image(
            body_parsed.image,
            body_parsed.ad_account_id,
            body_parsed.access_token,
          );
          break;
        case 'get_tk_video_infos':
          job = await await get_tk_video_infos(
            req.query.video_id,
            req.query.advertiser_id,
            req.query.access_token,
          );
          break;
        case 'upload_sp_video':
          job = await await upload_sp_video(
            body_parsed.video,
            body_parsed.ad_account_id,
            body_parsed.access_token,
          );
          break;
        case 'upload_fb_video':
          job = await await upload_fb_video(
            body_parsed.video,
            body_parsed.ad_account_id,
            body_parsed.access_token,
          );
          break;
        case 'get_tw_locations':
          job = await await get_tw_locations(
            req.query.search,
            req.query.access_token,
          );
          break;
        case 'get_tw_languages':
          job = await await get_tw_languages(
            req.query.search,
            req.query.access_token,
          );
          break;
        case 'upload_tw_file':
          job = await await upload_tw_chunked_file(
            body_parsed.file,
            body_parsed.file_name,
            body_parsed.file_type,
            body_parsed.ad_account_id,
            body_parsed.access_token,
          );
          break;
        default:
          throw {
            expose: false,
            error_detail: `${route_requested.param} not found in switch router`,
          };
      }
    } catch (e) {
      console_log('Error in switch routing module.');
      console_log('clean tmpFolders due to ERROR: ', tmpFolders);
      tmpFolders.forEach((folder) => fs.rmSync(folder, { recursive: true, force: true }));
      throw e;
    }
    console_log('clean tmpFolders after request SUCCESS: ', tmpFolders);
    tmpFolders.forEach((folder) => fs.rmSync(folder, { recursive: true, force: true }));

    return job;
  } catch (e) {
    console_log('Error in routing module.');
    throw e;
  }
};

export default router;
