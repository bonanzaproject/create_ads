const routes = [
  {
    method: 'GET',
    param: 'get_tk_identities',
    queries: ['advertiser_id', 'access_token'],
  },
  {
    method: 'POST',
    param: 'create_tk_identity',
    body: ['advertiser_id', 'access_token', 'avatar', 'avatar_name', 'md5'],
  },
  {
    method: 'GET',
    param: 'get_tk_locations',
    queries: ['advertiser_id', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_tk_languages',
    queries: ['advertiser_id', 'access_token'],
  },
  {
    method: 'POST',
    param: 'create_tk_creative',
    body: ['advertiser_id', 'access_token', 'video', 'video_name', 'md5'],
  },
  {
    method: 'GET',
    param: 'get_sp_countries',
    queries: ['access_token'],
  },
  {
    method: 'GET',
    param: 'get_sp_languages',
    queries: ['access_token'],
  },
  {
    method: 'GET',
    param: 'sp_refresh_token',
    queries: ['refresh_token'],
  },
  {
    method: 'GET',
    param: 'get_sp_locations_country',
    queries: ['country_code', 'access_token'],
  },
  {
    method: 'POST',
    param: 'upload_sp_image',
    body: ['image', 'ad_account_id', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_fb_places',
    queries: ['search', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_fb_locales',
    queries: ['search', 'access_token'],
  },
  {
    method: 'POST',
    param: 'upload_fb_image',
    body: ['image', 'ad_account_id', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_tk_video_infos',
    queries: ['video_id', 'advertiser_id', 'access_token'],
  },
  {
    method: 'POST',
    param: 'upload_sp_video',
    body: ['video', 'ad_account_id', 'access_token'],
  },
  {
    method: 'POST',
    param: 'upload_fb_video',
    body: ['video', 'ad_account_id', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_tw_locations',
    queries: ['search', 'access_token'],
  },
  {
    method: 'GET',
    param: 'get_tw_languages',
    queries: ['search', 'access_token'],
  },
  {
    method: 'POST',
    param: 'upload_tw_file',
    body: ['file', 'file_name', 'file_type', 'ad_account_id', 'access_token'],
  },
];

export default routes;
