// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import upload_tk_video_as_tmp_file from './upload_video_as_tmp_file.js';

const create_tk_creative = async (
  video_name,
  video_path,
  md5,
  advertiser_id,
  access_token,
) => {
  try {
    // 1 - UPLOAD VIDEO TO TIKTOK
    const results_upload_file = await upload_tk_video_as_tmp_file(
      video_name,
      video_path,
      md5,
      advertiser_id,
      access_token,
    );

    // 2 - UPLOAD VIDEO WITH FILE_ID PREVIOUSLY UPLOADED
    const data = new FormData();

    data.append('advertiser_id', advertiser_id);
    data.append('file_name', video_name);
    data.append('upload_type', 'UPLOAD_BY_FILE_ID');
    data.append('file_id', results_upload_file.file_id);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/file/video/ad/upload/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'multipart/form-data',
        ...data.getHeaders(),
      },
      data,
    };

    let uploadVideoTiktok_request;
    try {
      uploadVideoTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    if (uploadVideoTiktok_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Upload video: ${video_name} => NOT send: ${config.url}`,
          error: uploadVideoTiktok_request.data,
        },
      };
    }

    const results_upload_vid = uploadVideoTiktok_request.data.data[0];

    console_log(
      `Upload video from file_id: ${video_name} with id ${results_upload_vid.video_id} => send.`,
    );

    return results_upload_vid;
  } catch (e) {
    console_log('Error in create_creative module.');
    throw e;
  }
};

export default create_tk_creative;
