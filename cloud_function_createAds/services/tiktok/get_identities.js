import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_tk_identities = async (advertiser_id, access_token) => {
  try {
    let data = qs.stringify({
      advertiser_id,
      identity_type: 'CUSTOMIZED_USER',
      page: '1',
      page_size: '50',
    });

    let config = {
      method: 'get',
      url: `https://business-api.tiktok.com/open_api/v1.3/identity/get/?advertiser_id=${advertiser_id}`,
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data,
    };

    let pagination = true;
    let results = [];

    // Request page 1
    while (pagination) {
      let getIdentitiesTiktok_request;
      try {
        getIdentitiesTiktok_request = await axios(config);
      } catch (e) {
        throw {
          expose: false,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data.error,
          },
        };
      }
      if (getIdentitiesTiktok_request.data.code !== 0) {
        throw {
          expose: false,
          error_detail: {
            message: `Identities Tiktok: => NOT getted: ${config.url}`,
            error: getIdentitiesTiktok_request.data,
          },
        };
      }

      console_log('getIdentitiesTiktok_request: ', getIdentitiesTiktok_request.data);
      const results_justRequested = getIdentitiesTiktok_request.data.data.identity_list;

      results = [...results, ...results_justRequested];

      if (
        getIdentitiesTiktok_request.data.data.page_info.page
        === getIdentitiesTiktok_request.data.data.page_info.total_page
      ) {
        pagination = false;
      } else {
        data = qs.stringify({
          advertiser_id,
          identity_type: 'CUSTOMIZED_USER',
          page: (
            getIdentitiesTiktok_request.data.data.page_info.page + 1
          ).toString(),
          page_size: '50',
        });

        config = {
          method: 'get',
          url: `https://business-api.tiktok.com/open_api/v1.3/identity/get/?advertiser_id=${advertiser_id}`,
          headers: {
            'Access-Token': access_token,
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          data,
        };
      }
    }

    console_log('Identities paginated Tiktok: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_identities module.');
    throw e;
  }
};

export default get_tk_identities;
