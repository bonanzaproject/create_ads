// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_tk_video_infos = async (video_id, ad_account_id, access_token) => {
  try {
    const data = qs.stringify({
      advertiser_id: ad_account_id,
      video_ids: `["${video_id}"]`,
    });
    const config = {
      method: 'get',
      url: `https://business-api.tiktok.com/open_api/v1.3/file/video/ad/info?advertiser_id=${encodeURI(ad_account_id)}&video_ids=["${encodeURI(video_id)}"]`,
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data,
    };

    let videoInfosTiktok_request;
    try {
      videoInfosTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    if (videoInfosTiktok_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `Video infos: ${video_id} => NOT getted: ${config.url}`,
          error: videoInfosTiktok_request.data,
        },
      };
    }

    const result = videoInfosTiktok_request.data.data.list[0];

    console_log(`Video infos ${result.poster_url} => getted.`);

    return result;
  } catch (e) {
    console_log('Error in get_tk_video_infos module.');
    throw e;
  }
};

export default get_tk_video_infos;
