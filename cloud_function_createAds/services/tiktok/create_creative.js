// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const upload_tk_video_as_file = async (
  video_name,
  video_path,
  md5,
  advertiser_id,
  access_token,
) => {
  try {
    const data = new FormData();

    data.append('advertiser_id', advertiser_id);
    data.append('upload_type', 'UPLOAD_BY_FILE');
    data.append('video_file', fs.createReadStream(video_path));
    data.append('file_name', `${video_name}-${Date.now()}`);
    data.append('video_signature', md5);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/file/video/ad/upload/',
      headers: {
        'Access-Token': access_token,
        ...data.getHeaders(),
      },
      data: data,
    };

    try {
      if (fs.existsSync(video_path)) console_log(`file ${video_path} exist`);
      else console_log(`file ${video_path} NOT exist`);
    } catch (err) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${video_path}`,
          error: err,
        },
      };
    }

    let uploadVideoTiktok_request;
    try {
      uploadVideoTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response,
        },
      };
    }

    if (uploadVideoTiktok_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Upload video: ${video_name} => NOT uploaded: ${config.url}`,
          error: uploadVideoTiktok_request.data,
        },
      };
    }

    const result = uploadVideoTiktok_request.data.data;

    console_log(
      `Upload video tiktok: ${video_name}-${Date.now()} with id ${result.file_id} => created.`,
    );

    return result;
  } catch (e) {
    console_log('Error in upload_tk_video_as_file module.');
    throw e;
  }
};

export default upload_tk_video_as_file;
