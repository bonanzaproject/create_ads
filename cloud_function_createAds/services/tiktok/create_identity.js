// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import upload_tk_image_file from './upload_image_file.js';

const create_tk_identity = async (
  avatar_name,
  avatar_img_path,
  md5,
  advertiser_id,
  access_token,
) => {
  try {
    const resultImageUpload = await upload_tk_image_file(
      avatar_name,
      avatar_img_path,
      md5,
      advertiser_id,
      access_token,
    );

    // 2 - CREATE IDENTITY WITH IMAGE PREVIOUSLY UPLOADED
    const data = JSON.stringify({
      advertiser_id,
      image_uri: resultImageUpload.image_id,
      display_name: avatar_name,
    });

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/identity/create/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/json',
      },
      data,
    };

    let createIdentitiesTiktok_request;
    try {
      createIdentitiesTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    if (createIdentitiesTiktok_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Identity: ${avatar_name} => NOT created: ${config.url}`,
          error: createIdentitiesTiktok_request.data,
        },
      };
    }

    const result = createIdentitiesTiktok_request.data.data.identity_id;

    console_log(`Identity: ${avatar_name} with id ${result} => created.`);

    return result;
  } catch (e) {
    console_log('Error in create_tk_identity module.');
    throw e;
  }
};

export default create_tk_identity;
