// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_tk_locations = async (advertiser_id, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://business-api.tiktok.com/open_api/v1.3/tool/region/?advertiser_id=${advertiser_id}&placements=["PLACEMENT_TIKTOK"]&objective_type=CONVERSIONS&level_range=ALL`,
      headers: {
        'Access-Token': access_token,
      },
    };

    let getLocationTiktok_request;
    try {
      getLocationTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }
    if (getLocationTiktok_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `Locations Tiktok: => NOT getted: ${config.url}`,
          error: getLocationTiktok_request.data,
        },
      };
    }
    const results_location = getLocationTiktok_request.data.data;
    const results = results_location.region_info.map((location) => ({
      id: location.location_id,
      region_code: location.region_code,
      level: location.level,
      name: location.name,
    }));
    console_log('Locations Tiktok: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_tk_locations module.');
    throw e;
  }
};

export default get_tk_locations;
