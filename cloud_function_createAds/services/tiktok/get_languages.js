// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_tk_languages = async (advertiser_id, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://ads.tiktok.com/open_api/v1.3/tool/language/?advertiser_id=${advertiser_id}`,
      headers: {
        'Access-Token': access_token,
      },
    };

    let getLanguagesTiktok_request;
    try {
      getLanguagesTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }
    if (getLanguagesTiktok_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `Languages: => NOT getted: ${config.url}`,
          error: getLanguagesTiktok_request.data,
        },
      };
    }
    const results = getLanguagesTiktok_request.data.data.languages;
    console_log('Languages: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_tk_languages module.');
    throw e;
  }
};

export default get_tk_languages;
