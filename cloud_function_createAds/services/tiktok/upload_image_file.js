// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const upload_tk_image_file = async (
  avatar_name,
  avatar_img_path,
  md5,
  advertiser_id,
  access_token,
) => {
  try {
    // 1 - UPLOAD IMAGE TO TIKTOK
    const data = new FormData();

    data.append('image_file', fs.createReadStream(avatar_img_path));
    data.append('advertiser_id', advertiser_id);
    data.append('upload_type', 'UPLOAD_BY_FILE');
    data.append('image_signature', md5);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/file/image/ad/upload/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'multipart/form-data',
        ...data.getHeaders(),
      },
      data,
    };

    try {
      if (fs.existsSync(avatar_img_path)) console_log(`file ${avatar_img_path} exist`);
      else console_log(`file ${avatar_img_path} NOT exist`);
    } catch (err) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${avatar_img_path}`,
          error: err,
        },
      };
    }

    let uploadImageTiktok_request;
    try {
      uploadImageTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response,
        },
      };
    }

    if (uploadImageTiktok_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `File: ${avatar_name} => NOT send to Tiktok: ${config.url}`,
          error: uploadImageTiktok_request.data,
        },
      };
    }

    const result = uploadImageTiktok_request.data.data;

    console_log(
      `File: ${avatar_name} with id: ${result.image_id} => send to Tiktok.`,
    );

    return result;
  } catch (e) {
    console_log('Error in upload_tk_image_file module.');
    throw e;
  }
};

export default upload_tk_image_file;
