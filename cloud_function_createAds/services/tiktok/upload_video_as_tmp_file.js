// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const upload_tk_video_as_tmp_file = async (
  video_name,
  video_path,
  md5,
  advertiser_id,
  access_token,
) => {
  try {
    const data = new FormData();
    data.append('advertiser_id', advertiser_id);
    data.append('upload_type', 'FILE');
    data.append('content_type', 'video');
    data.append('file', fs.createReadStream(video_path));
    data.append('signature', md5);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/file/temporarily/upload/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'multipart/form-data',
        ...data.getHeaders(),
      },
      data,
    };

    try {
      if (fs.existsSync(video_path)) console_log(`file ${video_path} exist`);
      else console_log(`file ${video_path} NOT exist`);
    } catch (err) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${video_path}`,
          error: err,
        },
      };
    }

    let uploadFileTiktok_request;
    try {
      uploadFileTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response,
        },
      };
    }

    if (uploadFileTiktok_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Upload file: ${video_name} => NOT uploaded: ${config.url}`,
          error: uploadFileTiktok_request.data,
        },
      };
    }

    const result = uploadFileTiktok_request.data.data;

    console_log(
      `Upload file temporary: ${video_name} with id ${result.file_id} => created.`,
    );

    return result;
  } catch (e) {
    console_log('Error in upload_tk_video_as_tmp_file module.');
    throw e;
  }
};

export default upload_tk_video_as_tmp_file;
