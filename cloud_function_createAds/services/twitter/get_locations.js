// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const get_tw_locations = async (query, access_token) => {
  try {
    const header = await set_oauth_header(
      access_token,
      'GET',
      `https://ads-api.twitter.com/${access.twitter.version_api}/targeting_criteria/locations`,
      { q: query },
    );

    const config = {
      method: 'get',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/targeting_criteria/locations?q=${query}`,
      headers: {
        Authorization: header,
      },
    };

    let getLocationTwitter_request;
    try {
      getLocationTwitter_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getLocationTwitter_request.status !== 200) {
      throw {
        expose: false,
        error_detail: {
          message: `Locations Twitter: => NOT getted: ${config.url} with code ${getLocationTwitter_request.status}`,
          error: getLocationTwitter_request.errors,
        },
      };
    }

    const results_location = getLocationTwitter_request.data.data;

    const results = results_location.map((location) => ({
      id: location.targeting_value,
      region_code: location.country_code,
      level: location.location_type,
      name: location.name,
    }));

    console_log('Locations Twitter: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_tw_locations module.');
    throw e;
  }
};

export default get_tw_locations;
