// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

const upload_chunked_file_APPEND = async (media_id, media_data, segment_index, access_token) => {
  try {
    const data = new FormData();

    data.append('command', 'APPEND');
    data.append('media_id', media_id);
    data.append('media', fs.createReadStream(media_data));
    data.append('segment_index', segment_index);

    const header = await set_oauth_header(
      access_token,
      'POST',
      'https://upload.twitter.com/1.1/media/upload.json',
      {},
    );

    const config = {
      method: 'post',
      url: 'https://upload.twitter.com/1.1/media/upload.json',
      headers: {
        Authorization: header,
        ...data.getHeaders(),
      },
      data: data,
    };

    let postChunkFileTwitter_request;
    try {
      postChunkFileTwitter_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (postChunkFileTwitter_request.status !== 204) {
      throw {
        expose: false,
        error_detail: {
          message: 'upload_chunked_file_APPEND Twitter: => NOT done',
          error: postChunkFileTwitter_request.errors,
        },
      };
    }

    const results = postChunkFileTwitter_request.data;

    console_log(`upload_chunked_file_APPEND Twitter index: ${segment_index}: => done.`);

    return results;
  } catch (e) {
    console_log('Error in upload_chunked_file_APPEND module.');
    throw e;
  }
};

export default upload_chunked_file_APPEND;
