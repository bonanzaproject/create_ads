// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

const link_media_to_library = async (account_id_library, media_key, file_name, access_token) => {
  try {
    const data = new FormData();

    data.append('media_key', media_key);
    data.append('name', file_name);

    const header = await set_oauth_header(
      access_token,
      'POST',
      `https://ads-api.twitter.com/11/accounts/${account_id_library}/media_library`,
      {},
    );

    const config = {
      method: 'post',
      url: `https://ads-api.twitter.com/11/accounts/${account_id_library}/media_library`,
      headers: {
        Authorization: header,
        ...data.getHeaders(),
      },
      data: data,
    };

    let pushMediaLibraryTwitter_request;
    try {
      pushMediaLibraryTwitter_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (pushMediaLibraryTwitter_request.status !== 201) {
      throw {
        expose: false,
        error_detail: {
          message: `Media Library ${file_name} Twitter: => NOT pushed: ${config.url} with code ${pushMediaLibraryTwitter_request.status}`,
          error: pushMediaLibraryTwitter_request.errors,
        },
      };
    }

    const results = pushMediaLibraryTwitter_request.data.data;

    console_log(`Media Library ${file_name} Twitter: => pushed with media_key ${media_key}.`);

    return results;
  } catch (e) {
    console_log('Error in link_media_to_library module.');
    throw e;
  }
};

export default link_media_to_library;
