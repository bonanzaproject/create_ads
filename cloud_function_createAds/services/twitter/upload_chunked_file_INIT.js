// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

const upload_chunked_file_INIT = async (file_size, file_type, access_token) => {
  try {
    const data = qs.stringify({
      command: 'INIT',
      total_bytes: file_size,
      media_type: file_type,
      // if not api no return media_key
      media_category: file_type.split('/')[0] === 'image' ? 'TWEET_IMAGE' : 'AMPLIFY_VIDEO',
      additional_owners: '1524730251900764160', // dev owner id
    });

    console_log('INIT DATA: ', data);

    const header = await set_oauth_header(
      access_token,
      'POST',
      'https://upload.twitter.com/1.1/media/upload.json',
      {
        command: 'INIT',
        total_bytes: file_size,
        media_type: file_type,
        media_category: file_type.split('/')[0] === 'image' ? 'TWEET_IMAGE' : 'AMPLIFY_VIDEO',
        additional_owners: '1524730251900764160',
      },
    );

    const config = {
      method: 'post',
      url: 'https://upload.twitter.com/1.1/media/upload.json',
      headers: {
        Authorization: header,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: data,
    };

    let postChunkFileTwitter_request;
    try {
      postChunkFileTwitter_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (postChunkFileTwitter_request.status !== 202) {
      throw {
        expose: false,
        error_detail: {
          message: 'upload_chunked_file_INIT Twitter: => NOT done',
          error: postChunkFileTwitter_request.errors,
        },
      };
    }

    const results = postChunkFileTwitter_request.data;

    console_log(`upload_chunked_file_INIT Twitter: => done with media_id ${results.media_id}.`);

    return results;
  } catch (e) {
    console_log('Error in upload_chunked_file_INIT module.');
    throw e;
  }
};

export default upload_chunked_file_INIT;
