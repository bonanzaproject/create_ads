// LIBRARY
// import axios from 'axios';
import fs from 'fs';
import splitFile from 'split-file';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import upload_chunked_file_INIT from './upload_chunked_file_INIT.js';
import upload_chunked_file_APPEND from './upload_chunked_file_APPEND.js';
import upload_chunked_file_FINALIZE from './upload_chunked_file_FINALIZE.js';
import link_media_to_library from './link_media_to_library.js';

const upload_tw_chunked_file = async (file, file_name, file_type, ad_acount_id, access_token) => {
  try {
    const delay = (time) => new Promise((resolve) => setTimeout(resolve, time * 1000));

    const file_size = fs.statSync(file).size;
    console_log('file_size: ', file_size, access_token, ad_acount_id, file_name);

    // 0 - Init twitter whith what we gonna upload
    const request_init = await upload_chunked_file_INIT(
      file_size,
      file_type,
      access_token,
    );

    console_log('request_init: ', request_init);

    const media_id = request_init.media_id_string;

    // 1 - determine how much chunk (900 Ko) we need
    const nbChunk = Math.ceil(file_size / 900000);
    // 2 - Split the file & encode it in base64
    let splitedFile;
    if (nbChunk <= 1) {
      splitedFile = [file];
    } else {
      splitedFile = await splitFile.splitFile(file, nbChunk);
    }
    // 3 - upload chunks by segment_index
    const arrayAppendrequest = await Promise.all(splitedFile.map((chunk, index) => (
      // 3.1 - Encode chunk to base64
      upload_chunked_file_APPEND(media_id, chunk, index, access_token)
    )));

    console_log('arrayAppendrequest: ', arrayAppendrequest);

    // 4 - finalize request and get the media_id
    let request_finalize;
    let request_status = 'pending';
    while (request_status === 'pending' || request_status === 'in_progress') {
      console_log('request_status: ', request_status);
      request_finalize = await upload_chunked_file_FINALIZE(media_id, access_token);
      console_log('request_finalize: ', request_finalize);

      if (request_finalize.hasOwnProperty('image')) {
        request_status = 'succeeded';
      } else {
        request_status = request_finalize.processing_info.state;
      }

      if (request_finalize.hasOwnProperty('processing_info')
        && request_finalize.processing_info.hasOwnProperty('check_after_secs')) {
        // 4.1 - Check if request is not pending
        await delay(request_finalize.processing_info.check_after_secs);
      }
    }

    console_log('request_finalize: ', request_finalize);

    // 5 - Link to the media_Library
    const request_linkMedia = await link_media_to_library(
      ad_acount_id,
      request_finalize.media_key,
      file_name,
      access_token,
    );

    console_log('Upload media Twitter: => done.');

    return request_linkMedia;
  } catch (e) {
    console_log('Error in upload_chunked_file module.');
    throw e;
  }
};

export default upload_tw_chunked_file;
