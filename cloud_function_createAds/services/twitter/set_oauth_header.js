// LIBRARY
import crypto from 'crypto';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// CONST
import access from '../../const/access.js';

const set_oauth_header = async (access_token, method, base_url, queryParameters) => {
  try {
    const oauth_consumer_key = access.twitter.oauth_consumer.key;
    const oauth_token = access_token;

    const consumer_secret = access.twitter.oauth_consumer.secret;
    const token_secret = access.twitter.token_secret;

    const oauth_timestamp = Math.floor(Date.now() / 1000);
    const oauth_nonce = crypto.randomBytes(32).toString('hex');

    const parameters = {
      ...queryParameters,
      oauth_consumer_key: oauth_consumer_key,
      oauth_token: oauth_token,
      oauth_signature_method: 'HMAC-SHA1',
      oauth_timestamp: oauth_timestamp,
      oauth_nonce: oauth_nonce,
      oauth_version: '1.0',
    };

    const ordered = {};
    Object.keys(parameters).sort().forEach((key) => {
      ordered[key] = parameters[key];
    });
    let encodedParameters = '';
    Object.keys(ordered).forEach((k) => {
      const encodedValue = encodeURIComponent(escape(ordered[k]));
      const encodedKey = encodeURIComponent(k);
      if (encodedParameters === '') {
        encodedParameters += encodeURIComponent(`${encodedKey}=${encodedValue}`);
      } else {
        encodedParameters += encodeURIComponent(`&${encodedKey}=${encodedValue}`);
      }
    });

    const encodedUrl = encodeURIComponent(base_url);
    const signature_base_string = `${method}&${encodedUrl}&${encodedParameters}`;

    const signing_key = `${consumer_secret}&${token_secret}`;
    const oauth_signature_base_64 = crypto.createHmac('sha1', signing_key).update(signature_base_string).digest('').toString('base64');
    const encoded_oauth_signature = encodeURIComponent(oauth_signature_base_64);
    const authorization_header = `OAuth oauth_consumer_key="${oauth_consumer_key}", oauth_nonce="${oauth_nonce}", oauth_signature="${encoded_oauth_signature}", oauth_signature_method="HMAC-SHA1", oauth_timestamp="${oauth_timestamp}", oauth_token="${oauth_token}", oauth_version="1.0"`;

    console_log('authorization_header twitter: ', authorization_header);
    return authorization_header;
  } catch (e) {
    console_log('Error in set_oauth_header module.');
    throw e;
  }
};

export default set_oauth_header;
