// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const get_tw_languages = async (query, access_token) => {
  try {
    const header = await set_oauth_header(
      access_token,
      'GET',
      `https://ads-api.twitter.com/${access.twitter.version_api}/targeting_criteria/languages`,
      { q: query },
    );

    const config = {
      method: 'get',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/targeting_criteria/languages?q=${query}`,
      headers: {
        Authorization: header,
      },
    };

    let getLanguagesTwitter_request;
    try {
      getLanguagesTwitter_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getLanguagesTwitter_request.status !== 200) {
      throw {
        expose: false,
        error_detail: {
          message: `Languages Twitter: => NOT getted: ${config.url} with code ${getLanguagesTwitter_request.status}`,
          error: getLanguagesTwitter_request.errors,
        },
      };
    }

    const results_languages = getLanguagesTwitter_request.data.data;

    const results = results_languages.map((lang) => ({
      id: lang.targeting_value,
      name: lang.name,
    }));

    console_log('Languages Twitter: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_tw_languages module.');
    throw e;
  }
};

export default get_tw_languages;
