// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_fb_infos_video = async (video_id, ad_account_id, access_token) => {
  try {
    let results_check = {};

    // - Have to waiting facebook generate source url on his side.
    //   While it's not created, fb response request will not contain source url.
    while (!results_check.hasOwnProperty('source')) {
      if (Object.keys(results_check).length !== 0) {
        console_log('waiting 1s to get video infos');
        await new Promise((resolve) => setTimeout(resolve, 10000));
      }
      const data = new FormData();

      const config = {
        method: 'get',
        url: `https://graph.facebook.com/v14.0/${video_id}?access_token=${access_token}&fields=['id','title','source']`,
        headers: {
          ...data.getHeaders(),
        },
        data: data,
      };

      let getInfosVideoFB_request;

      try {
        getInfosVideoFB_request = await axios(config);
      } catch (e) {
        throw {
          expose: false,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data,
          },
        };
      }
      if (getInfosVideoFB_request.data.error) {
        throw {
          expose: true,
          error_detail: {
            message: `Facebook infos video: => NOT getted: ${config.url}`,
            error: getInfosVideoFB_request.data.message,
          },
        };
      }
      results_check = getInfosVideoFB_request.data;
    }
    console_log('Facebook infos video: => getted.');

    return results_check;
  } catch (e) {
    console_log('Error in get_fb_infos_video module.');
    throw e;
  }
};

export default get_fb_infos_video;
