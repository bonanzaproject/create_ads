// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_fb_places = async (search, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://graph.facebook.com/v14.0/search?access_token=${access_token}&type=adgeolocation&location_types=['country', 'region', 'country_group', 'city']&q=${encodeURI(search)}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify({}),
    };

    let getPlacesFB_request;
    try {
      getPlacesFB_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getPlacesFB_request.data.error) {
      throw {
        expose: true,
        error_detail: {
          message: `Facebook places: => NOT getted: ${config.url}`,
          error: getPlacesFB_request.data.message,
        },
      };
    }

    const results = getPlacesFB_request.data.data.map(
      (result, index) => ({ ...result, index }),
    );

    console_log('Facebook places: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_fb_places module.');
    throw e;
  }
};

export default get_fb_places;
