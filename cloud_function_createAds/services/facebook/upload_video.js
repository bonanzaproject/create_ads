// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import get_fb_infos_video from './get_infos_video.js';

const upload_fb_video = async (
  video_path,
  ad_account_id,
  access_token,
) => {
  try {
    // 0 - Test if file to upload exist
    try {
      if (fs.existsSync(video_path)) console_log(`file ${video_path} exist`);
      else console_log(`file ${video_path} NOT exist`);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${video_path}`,
          error: e.data,
        },
      };
    }

    const video_name = video_path.split('/')[video_path.split('/').length - 1];

    // 1 - Upload file to Facebook mediathèque
    const dataObj = new FormData();

    dataObj.append('filename', fs.createReadStream(video_path));
    dataObj.append('access_token', access_token);
    dataObj.append('name', video_name);

    const config = {
      method: 'post',
      url: `https://graph.facebook.com/v14.0/act_${ad_account_id}/advideos`,
      headers: {
        ...dataObj.getHeaders(),
      },
      data: dataObj,
    };

    let uploadFBVideo_request;
    try {
      uploadFBVideo_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (uploadFBVideo_request.data.error) {
      throw {
        expose: true,
        error_detail: {
          message: `Facebook video: => NOT uploaded: ${config.url}`,
          error: uploadFBVideo_request.data.message,
        },
      };
    }

    const resultVideoUploaded = uploadFBVideo_request.data;

    console_log(`Facebook video with id ${resultVideoUploaded.id} => created.`);

    // 2 - Get permanent_url video for the front
    const infosVideo = await get_fb_infos_video(
      resultVideoUploaded.id,
      ad_account_id,
      access_token,
    );

    const result = infosVideo;

    console_log(`Facebook get info video with id ${result.id} => created and verified.`);

    return result;
  } catch (e) {
    console_log('Error in upload_fb_video module.');
    throw e;
  }
};

export default upload_fb_video;
