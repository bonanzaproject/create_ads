// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import get_fb_infos_image from './get_infos_image.js';

const upload_fb_image = async (
  image_path,
  ad_account_id,
  access_token,
) => {
  try {
    // 0 - Test if file to upload exist
    try {
      if (fs.existsSync(image_path)) console_log(`file ${image_path} exist`);
      else console_log(`file ${image_path} NOT exist`);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${image_path}`,
          error: e.response.data,
        },
      };
    }

    const image_name = image_path.split('/')[image_path.split('/').length - 1];

    // 1 - Upload file to Facebook mediathèque
    const dataObj = new FormData();

    dataObj.append('filename', fs.createReadStream(image_path));
    dataObj.append('access_token', access_token);

    const config = {
      method: 'post',
      url: `https://graph.facebook.com/v14.0/act_${ad_account_id}/adimages`,
      headers: {
        ...dataObj.getHeaders(),
      },
      data: dataObj,
    };

    let uploadFBImage_request;
    try {
      uploadFBImage_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (uploadFBImage_request.data.error) {
      throw {
        expose: true,
        error_detail: {
          message: `Facebook image: => NOT uploaded: ${config.url}`,
          error: uploadFBImage_request.data.message,
        },
      };
    }

    const resultImageUploaded = uploadFBImage_request.data;

    console_log(`Facebook image with hash ${resultImageUploaded.images[image_name].hash} => created.`);

    // 2 - Get permanent_url image for the front
    const infosImage = await get_fb_infos_image(
      resultImageUploaded.images[image_name].hash,
      ad_account_id,
      access_token,
    );

    const result = {
      ...infosImage.data[0],
      ...resultImageUploaded.images[image_name],
    };

    console_log(`Facebook image with id ${result.id} => created.`);

    return result;
  } catch (e) {
    console_log('Error in upload_fb_image module.');
    throw e;
  }
};

export default upload_fb_image;
