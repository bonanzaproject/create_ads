// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_fb_infos_image = async (hash_image, ad_account_id, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://graph.facebook.com/v14.0/act_${ad_account_id}/adimages?hashes=["${hash_image}"]&access_token=${access_token}&fields=permalink_url`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify({}),
    };

    let getInfosImageFB_request;
    try {
      getInfosImageFB_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getInfosImageFB_request.data.error) {
      throw {
        expose: true,
        error_detail: {
          message: `Facebook infos image: => NOT getted: ${config.url}`,
          error: getInfosImageFB_request.data.message,
        },
      };
    }

    const results = getInfosImageFB_request.data;

    console_log('Facebook infos image: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_fb_infos_image module.');
    throw e;
  }
};

export default get_fb_infos_image;
