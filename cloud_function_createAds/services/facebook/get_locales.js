// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_fb_locales = async (search, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://graph.facebook.com/v14.0/search?access_token=${access_token}&type=adlocale&q=${encodeURI(search)}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify({}),
    };

    let getLocalesFB_request;
    try {
      getLocalesFB_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getLocalesFB_request.data.error) {
      throw {
        expose: true,
        error_detail: {
          message: `Facebook places: => NOT getted: ${config.url}`,
          error: getLocalesFB_request.data.message,
        },
      };
    }

    const results = getLocalesFB_request.data.data.map(
      (result, index) => ({ ...result, index }),
    );

    console_log('Facebook locales: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_fb_locales module.');
    throw e;
  }
};

export default get_fb_locales;
