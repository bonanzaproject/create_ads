// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_sp_countries = async (access_token) => {
  try {
    const config = {
      method: 'get',
      url: 'https://adsapi.snapchat.com/v1/targeting/geo/country',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let getCountriesSP_request;
    try {
      getCountriesSP_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getCountriesSP_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat countries: => NOT getted: ${config.url}`,
          error: getCountriesSP_request.data,
        },
      };
    }

    const results = getCountriesSP_request.data.targeting_dimensions.map(
      (result, index) => ({ ...result.country.country, index }),
    );

    console_log('Snapchat countries: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_countries module.');
    throw e;
  }
};

export default get_sp_countries;
