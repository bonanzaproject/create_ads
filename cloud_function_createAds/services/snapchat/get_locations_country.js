// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import get_sp_regions from './get_regions.js';
import get_sp_metros from './get_metros.js';

const get_sp_locations_country = async (country_code, access_token) => {
  try {
    const results_region = await get_sp_regions(country_code, access_token);
    const results_metro = await get_sp_metros(country_code, access_token);

    console_log('results_region: ', results_region);
    console_log('results_metro: ', results_metro);

    const results = [...results_region, ...results_metro];

    console_log('Snapchat locations: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_sp_locations_country module.');
    throw e;
  }
};

export default get_sp_locations_country;
