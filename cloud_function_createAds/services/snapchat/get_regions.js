// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_sp_regions = async (country_code, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://adsapi.snapchat.com/v1/targeting/geo/${country_code}/region`,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let getRegionsSP_request;
    try {
      getRegionsSP_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getRegionsSP_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat regions: => NOT getted: ${config.url}`,
          error: getRegionsSP_request.data,
        },
      };
    }

    let results = getRegionsSP_request.data.targeting_dimensions.map(
      (result, index) => {
        if (!result.region.hasOwnProperty('deprecated')) {
          return {
            type: 'region',
            id: result.region.region.id,
            code: result.region.region.code,
            name: result.region.region.name,
            index,
          };
        }
        return false;
      },
    );

    results = results.filter((result) => result !== false);

    console_log('Snapchat regions: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_sp_regions module.');
    throw e;
  }
};

export default get_sp_regions;
