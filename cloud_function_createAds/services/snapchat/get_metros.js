// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_sp_metros = async (country_code, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://adsapi.snapchat.com/v1/targeting/geo/${country_code}/metro`,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let getMetrosSP_request;
    try {
      getMetrosSP_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getMetrosSP_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat metros: => NOT getted: ${config.url}`,
          error: getMetrosSP_request.data,
        },
      };
    }

    const results = getMetrosSP_request.data.targeting_dimensions.map(
      (result, index) => ({
        type: 'metro',
        id: result.metro.metro.id,
        region: result.metro.metro.regions,
        name: result.metro.metro.name,
        index,
      }),
    );

    console_log('Snapchat metros: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_sp_metros module.');
    throw e;
  }
};

export default get_sp_metros;
