// LIBRARY
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// SERVICES
import create_sp_media from './create_media.js';

const upload_sp_image = async (
  image_path,
  ad_account_id,
  access_token,
) => {
  try {
    const media = await create_sp_media('IMAGE', ad_account_id, access_token);

    try {
      if (fs.existsSync(image_path)) console_log(`file ${image_path} exist`);
      else console_log(`file ${image_path} NOT exist`);
    } catch (err) {
      throw {
        expose: false,
        error_detail: {
          message: `File exist error: ${image_path}`,
          error: err,
        },
      };
    }

    const data = new FormData();

    data.append('file', fs.createReadStream(image_path));

    const config = {
      method: 'post',
      url: `https://adsapi.snapchat.com/v1/media/${media.id}/upload`,
      headers: {
        Authorization: `Bearer ${access_token}`,
        ...data.getHeaders(),
      },
      data,
    };

    let uploadSPImage_request;
    try {
      uploadSPImage_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (uploadSPImage_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat image: => NOT uploaded: ${config.url}`,
          error: uploadSPImage_request.data,
        },
      };
    }

    const resultImageUpload = uploadSPImage_request.data;

    console_log(`Snapchat image with id ${resultImageUpload.result.id} => created.`);

    const result = {
      ...resultImageUpload.result,
      media_created: media,
    };

    return result;
  } catch (e) {
    console_log('Error in upload_sp_image module.');
    throw e;
  }
};

export default upload_sp_image;
