// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const sp_refresh_token = async (
  client_id,
  client_secret,
  refresh_token,
) => {
  try {
    const data = qs.stringify({
      refresh_token,
      client_id,
      client_secret,
      grant_type: 'refresh_token',
    });

    const config = {
      method: 'post',
      url: 'https://accounts.snapchat.com/login/oauth2/access_token',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data,
    };

    let refreshTokenSnapchat_request;
    try {
      refreshTokenSnapchat_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (refreshTokenSnapchat_request.data.request_status === 'ERROR') {
      throw {
        expose: false,
        error_detail: {
          message: `Snapchat request failed: ${config.url}`,
          error: refreshTokenSnapchat_request.data,
        },
      };
    }

    const result = refreshTokenSnapchat_request.data.access_token;
    console_log('Snapchat token has been refreshed');

    return result;
  } catch (e) {
    console_log('Error in sp_refresh_token module.');
    throw e;
  }
};

export default sp_refresh_token;
