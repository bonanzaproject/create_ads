// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_sp_languages = async (access_token) => {
  try {
    const config = {
      method: 'get',
      url: 'https://adsapi.snapchat.com/v1/targeting/demographics/languages',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let getLanguagesSP_request;
    try {
      getLanguagesSP_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (getLanguagesSP_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat languages: => NOT getted: ${config.url}`,
          error: getLanguagesSP_request.data,
        },
      };
    }

    const results = getLanguagesSP_request.data.targeting_dimensions.map(
      (locale) => locale.languages,
    );

    console_log('Snapchat languages: => getted.');

    return results;
  } catch (e) {
    console_log('Error in get_sp_languages module.');
    throw e;
  }
};

export default get_sp_languages;
