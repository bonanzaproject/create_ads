// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_sp_media = async (type, ad_account_id, access_token) => {
  try {
    const data = JSON.stringify({
      media: [
        {
          name: 'autoMedia-story-createAds',
          type: type,
          ad_account_id,
        },
      ],
    });

    const config = {
      method: 'post',
      url: `https://adsapi.snapchat.com/v1/adaccounts/${ad_account_id}/media`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`,
      },
      data,
    };

    let createMediaSP_request;
    try {
      createMediaSP_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (createMediaSP_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat media: => NOT created: ${config.url}`,
          error: createMediaSP_request.data,
        },
      };
    }

    const result = createMediaSP_request.data.media[0].media;

    console_log(`Snapchat media with id ${result.id} => created.`);

    return result;
  } catch (e) {
    console_log('Error in create_sp_media module.');
    throw e;
  }
};

export default create_sp_media;
