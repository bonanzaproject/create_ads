# createAds

CreateAds is a hub nodejs library for Bonanza front platform do request data to Tiktok, Facebook, Instagram, Twitter and Snapchat Ads manager.

## Linter

Use eslint to clean your code

npm run lint

## Run localy the cloud function

It Use functions-framework to simulate google cloud function in your machine.

npm run start

## Deploy

Use gcloud command to deploy functions with the current git branch name

npm run deploy