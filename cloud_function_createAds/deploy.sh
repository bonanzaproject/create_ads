#!/bin/bash

#$1 = name of the project
#$2 = environment variable to set
echo "Deploy on cloud function: $1-$(git branch 2> /dev/null | sed -n -e 's/^\* \(.*\)/\1/p')";
echo "Environment variable: $2";
gcloud functions deploy `echo $1-$(git branch 2> /dev/null | sed -n -e 's/^\* \(.*\)/\1/p')` --set-env-vars `echo $2` --timeout=540 --runtime=nodejs16 --trigger-http --entry-point=start_f --allow-unauthenticated --region=europe-west1;