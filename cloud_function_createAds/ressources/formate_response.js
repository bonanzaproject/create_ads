// LIBRARY
import util from 'util';
import moment from 'moment';

// RESSOURCES
import header_from from './header_from.js';
import console_log from './console_log.js';

// CONST
import access from '../const/access.js';

// SERVICE
import push_message from '../services/slack/push_message.js';

const formate_response = async (request, response, status, result_request) => {
  try {
    const fromRequest = await header_from(request);
    console_log('Header from: ', fromRequest);

    if (status === 'success') {
      const formatted_return = {
        success: true,
        from: fromRequest,
        when: moment().locale('fr').format('LLLL'),
        message: 'ok',
        datas: result_request,
      };

      // 0 - log on stdout
      console_log(
        'SUCCESS: ',
        util.inspect(formatted_return, {
          showHidden: false,
          depth: null,
          colors: process.env.ENV_LOCAL === 'true' || false,
        }),
      );
      // 1 - Log on slack
      await push_message(
        util.inspect(formatted_return, {
          showHidden: false,
          depth: null,
          colors: false,
        }),
        access.slack.token,
      );

      // 2 - return 200 request
      response.status(200).json(formatted_return);
    } else {
      const formatted_err = {
        success: false,
        from: fromRequest,
        when: moment().locale('fr').format('LLLL'),
        message: 'Une erreur est survenue',
        error_detail: result_request.error_detail,
      };

      // 0 - log on stderr
      console_log(
        'Error: ',
        util.inspect(formatted_err, {
          showHidden: false,
          depth: null,
          colors: process.env.ENV_LOCAL === 'true' || false,
        }),
      );
      // 1 - Log on slack
      await push_message(
        util.inspect(formatted_err, {
          showHidden: false,
          depth: null,
          colors: false,
        }),
        access.slack.token,
      );
      // 3 - Return 500 http error
      // 3.1 - Hide detail in return request if expose is set to false.
      if (result_request.expose) {
        response.status(500).json(formatted_err);
      } else {
        response.status(500).json({
          message: 'Un erreur est survenue',
          error_detail: 'Go to journal to get the detail',
        });
      }
    }
    return response;
  } catch (e) {
    console_log('Error in formate_response module.');
    throw e;
  }
};

export default formate_response;
