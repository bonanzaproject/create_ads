import path from 'path';
import os from 'os';
import fs from 'fs';
import Busboy from 'busboy';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

// GOOGLE BUSBOY CODE SOURCE
// https://cloud.google.com/functions/docs/writing/http?hl=fr#multipart_data

// RESSOURCES
import console_log from './console_log.js';

const parse_body = async (req) => {
  try {
    const busboy = Busboy({ headers: req.headers });
    const fields = {};
    const files = [];
    const tmpdir = os.tmpdir();
    const tmp_folder = `${uuidv4()}-${moment().format('x')}`;
    const tmpFolders = [];
    let fileUploaded = false;

    // When "NORMAL FIELD" is detected in form-data
    busboy.on('field', (key, value) => {
      fields[key] = value;
    });

    const promiseOnFile = new Promise((resolve, reject) => {
      try {
        // When "FILE" is detected in form-data
        busboy.on(
          'file',
          async (fieldname, file, filename) => {
            fileUploaded = true;
            // const oldfilepath = path.join(tmpdir, filename.filename);

            // 1 - Create new random file name
            const newfileName = `file-${uuidv4()}.${
              filename.mimeType.split('/')[1]
            }`;
            // 2 - Create new random folder name
            const folderpath = path.join(tmpdir, `createAds/${tmp_folder}`);
            // 3 - Get path of the futur file
            const filepath = path.join(folderpath, newfileName);
            // 4 - Create the random folder named previously
            fs.mkdirSync(folderpath, { recursive: true });

            // 5 - Create file from write stream (file => byte Buffer)
            //     with random name in specific folder.
            const writeStream = fs.createWriteStream(filepath);

            const promiseW = async (fileS, ws) => {
              fileS.pipe(ws);
              fileS.on('end', () => {
                ws.end();
              });
              ws.on('close', () => {
                files[fieldname] = filepath;
                tmpFolders.push(folderpath);
                resolve();
              });
              ws.on('error', () => false);
            };

            // 6 - Promisify the writing image/video file to force
            //     the function to wait return of the promise.
            await promiseW(file, writeStream);
          },
        );
      } catch (err) {
        reject();
      }
    });

    const promiseOnFinish = new Promise((resolve, reject) => {
      try {
        busboy.on('finish', async () => {
          if (fileUploaded) {
            await promiseOnFile;
          }

          const result = {
            body_parsed: {
              ...fields,
              ...files,
            },
            tmpFolders,
          };

          resolve(result);
        });
      } catch (err) {
        reject();
      }
    });

    busboy.end(req.rawBody);

    return await promiseOnFinish;
  } catch (e) {
    console_log('Error in parse_body module.');
    throw e;
  }
};

export default parse_body;
