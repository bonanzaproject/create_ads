const console_log = (...logs) => {
  try {
    if (process.env.ENV_LOCAL === 'true') {
      // eslint-disable-next-line no-console
      console.log(...logs);
    } else {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(logs).replace(/\\n/g, ''));
    }
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('Error in console_log module.');
    throw e;
  }
};

export default console_log;
