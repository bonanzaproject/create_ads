// RESSOURCES
import console_log from './ressources/console_log.js';
import formate_response from './ressources/formate_response.js';

// ROUTING
import router from './routing/index.js';

/* eslint-disable import/prefer-default-export */
export const start_f = async (req, res) => {
  try {
    console_log('*** NEW Request ***');
    console_log('process.env.ENV_LOCAL: ', process.env.ENV_LOCAL);

    // Allow * as origin for cors request
    res.set('Access-Control-Allow-Origin', '*');

    // Manage OPTIONS request for cors
    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Methods', 'POST');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      return res.status(204).send('');
    }

    const result_request = await router(req, res);

    const response = await formate_response(req, res, 'success', result_request);

    return response;
  } catch (err) {
    const response = await formate_response(req, res, 'error', err);

    return response;
  }
};
