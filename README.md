# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary logic.
    * dev branch is use by developpers.
    * test branch is use by opex team to test feature on test-open.bonanza.co
    * master branch is use by production.

### How do I get set up? ###

* Deployment instructions
    - Pass the code from dev branch into the test branch with git merge --no-ff and merge into the master branch and mimified code.
        - instructions:
            - git add [$FILE]
            - git commit -m "$MESSAGE" [$FILES]
            - git push origin dev
            - Change the imported script from Bitbucket with the good one branch (custom code tab on webflow setting) and publish on dev-opex.bonanza.co on webflow.
            - git checkout test
            - git merge dev --no-ff
            - git push origin test
            - Change the imported script from Bitbucket with the good one branch (custom code tab on webflow setting) and publish on test-opex.bonanza.co on webflow.
            - git checkout master
            - git merge test --no-ff
            - mimify the index.js code
            - git push origin master
            - Change the imported script from Bitbucket with the good one branch (custom code tab on webflow setting) and publish on test-opex.bonanza.co on webflow.


