document.addEventListener("alpine:init", () => {
  Alpine.data('body_data', () => ({
      initialized: false,
  }));

  Alpine.store('campaign', {
    initialized: false,
    isTrafficCampaign: false,
    isDev: null,
    urlApi: null,
    access: {
        airtable: {
            token: null,
            record_id: null,
            table_id: null
        },
        facebook: {
            token: null,
            ad_account_id: null
        },
        snapchat: {
            token: null,
            refresh_token: null,
            ad_account_id: null
        },
        tiktok: {
            token: null,
            ad_account_id: null
        },
        twitter: {
            token: null,
            ad_account_id: null
        }
    },
    client: null,
    name: null,
    groups: {
        facebook: [],
        snapchat: [],
        tiktok: [],
        twitter: []
    },
    spinners: {
        send_data_btn: false,
        send_open_modal_btn: false,
        send_open_modal_tiktok_btn: false,
        send_open_modal_twitter_btn: false,
        send_pic_facebook: false,
        send_pic_snapchat: false,
        send_pic_tiktok: false,
        send_pic_twitter: false,
        send_create_identity_tiktok: false
    },
    country_code_selected: "FR",
    async init() {
        /* -- MANAGE MODALS CLASS -- */
        $('#spinner_box').removeClass('hide');
        $('#spinner_box_2').removeClass('hide');
        $('#spinner_box_3').removeClass('hide');
        $('#spinner_box_4').removeClass('hide');
        $('#spinner_box_5').removeClass('hide');
        $('#spinner_box_6').removeClass('hide');
        $('#spinner_box_7').removeClass('hide');
        $('#spinner_box_8').removeClass('hide');
        $('#spinner_box_9').removeClass('hide');
        $('#modal_update_campaign_name').removeClass('hide');
        $('#modal_facebook').removeClass('hide');
        $('#modal_snapchat').removeClass('hide');
        $('#modal_tiktok').removeClass('hide');
        $('#modal_twitter').removeClass('hide');
        $('#loadingPage-body').removeClass('hide');

        /* -- MANAGE ENVIRONMENT -- */
        var test_in_bonanza_dev = true;
        if (window.location.hostname === "opex.bonanza.co") {
            test_in_bonanza_dev = false;
            this.isDev = false;
            this.urlApi = "https://europe-west1-bonanza-217113.cloudfunctions.net/create-ads-master"
            this.access.airtable.table_id = "appthuKmJunJtSHQO";
        } else if (window.location.hostname === "test-opex.bonanza.co") {
            this.access.airtable.table_id = "appohR6BsGlAlWkWE";
            this.isDev = true;
            this.urlApi = "https://europe-west1-bonanza-217113.cloudfunctions.net/create-ads-test"
        } else if (window.location.hostname === "dev-opex.bonanza.co") {
            this.access.airtable.table_id = "appiIZMzM8oMyTzTq";
            this.isDev = true;
            this.urlApi = "https://europe-west1-bonanza-217113.cloudfunctions.net/create-ads-dev"
        } else {
            alert('Error: dns unknow !');
        }

        console.log(`You are with Sandbox mode set in: ${test_in_bonanza_dev} cause domain is ${window.location.hostname}`);

        // FACEBOOK
        const fb_ad_account_id_prod = "26765043";
        const fb_ad_account_id_bonanza_dev = "872552453455574";
        // SNAPCHAT 
        const snap_ad_account_id_prod = "9714ced3-4528-4cd8-af68-83cb2a36bd2d";
        const snap_ad_account_id_bonanza_dev = "c41b16f1-474b-4799-bdbe-61cef3905389";
        // TIKTOK 
        const tiktok_ad_account_id_prod = "6839251180112052230";
        const tiktok_ad_account_id_bonanza_dev = "7070162555771043842";
        // TWITTER 
        const twitter_ad_account_id_prod = '18ce54gi81f';
        const twitter_ad_account_id_bonanza_dev = "18ce55hivn1";

        if (test_in_bonanza_dev) {
          this.access.facebook.ad_account_id = fb_ad_account_id_bonanza_dev;
          this.access.snapchat.ad_account_id = snap_ad_account_id_bonanza_dev;
          this.access.tiktok.ad_account_id = tiktok_ad_account_id_bonanza_dev;
          this.access.twitter.ad_account_id = twitter_ad_account_id_bonanza_dev;
        } else {
          this.access.facebook.ad_account_id = fb_ad_account_id_prod;
          this.access.snapchat.ad_account_id = snap_ad_account_id_prod;
          this.access.tiktok.ad_account_id = tiktok_ad_account_id_prod;
          this.access.twitter.ad_account_id = twitter_ad_account_id_prod;
        }

        /* -- MANAGE URL PARAM AND INIT-- */
        //get url
        const queryString = window.location.search;
        //get params object
        const urlParams = new URLSearchParams(queryString);

        const campaign_record_id = urlParams.get('campaign_id');
        const fb_token = urlParams.get('fb_token');
        const snap_refresh_token = urlParams.get('snap_refresh_token');
        const tiktok_token = urlParams.get('tiktok_token');
        const twitter_token = urlParams.get('twitter_token');
        const airtable_token = urlParams.get('airtable_token');

        if(!fb_token || !snap_refresh_token || !tiktok_token || !twitter_token || !airtable_token || !campaign_record_id) {
          window.location.href = `/error-modify`;
        }
        console.log("this.access: ", this.access);
        this.access.facebook.token = fb_token;
        this.access.snapchat.refresh_token = snap_refresh_token;
        this.access.tiktok.token = tiktok_token;
        this.access.twitter.token = twitter_token;
        this.access.airtable.token = airtable_token;
        this.access.airtable.record_id = campaign_record_id;
        console.log("this.access: ", this.access);

        const params = await this.getJsonAirtableRecord(this.access.airtable.record_id, this.access.airtable.table_id);

        if (!params.hasOwnProperty('json')) {
          console.log("json PAS detecté cest un CREATE: ");
          this.client = params['Compte'];
          this.name = params['Nom de la campagne'];
        } else {
          console.log("json detecté cest un UPDATE: ", JSON.parse(params.json));
          this.client = JSON.parse(params.json).client;
          this.name = JSON.parse(params.json).name;
          this.country_code = JSON.parse(params.json).country_code;
          this.groups = JSON.parse(params.json).groups;

          //Pour que toute les campagnes antérieur à la version 0.2.5 ne soit pas bug
          if (!JSON.parse(params.json).hasOwnProperty('isTrafficCampaign')) {
            this.isTrafficCampaign = false
          } else {
            this.isTrafficCampaign = JSON.parse(params.json).isTrafficCampaign;
          }
        }

        console.log("getJsonAirtableRecord return: ", params);

        /* -- MANAGE TRANSITIONS-- */
        var a = document.querySelector("#modal_update_campaign_name").firstChild.firstChild;
        a.setAttribute("x-transition", "");
        var b = document.querySelector("#modal_facebook").firstChild.firstChild;
        b.setAttribute("x-transition", "");
        var c = document.querySelector("#modal_snapchat").firstChild.firstChild;
        c.setAttribute("x-transition", "");
        var d = document.querySelector("#modal_tiktok").firstChild.firstChild;
        d.setAttribute("x-transition", "");
        var e = document.querySelector("#modal_twitter").firstChild.firstChild;
        e.setAttribute("x-transition", "");
        console.log("init store");

        /* -- INIT DONE -- */

        this.initialized = true;
        console.log('store initialized', this);
    },
    async getJsonAirtableRecord(record_id, table_id) {
      try {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${this.access.airtable.token}`);

        var requestOptions = {
          method: 'GET',
          headers: myHeaders,
          redirect: 'follow'
        };

        const get_record_request = await fetch(`https://api.airtable.com/v0/${table_id}/Suivi%20Campagne/${record_id}`, requestOptions);
        const get_record_data = await get_record_request.json();

        console.log("get_record_request: ", get_record_request);
        console.log("get_record_data: ", get_record_data);

        if (get_record_request.status !== 200) throw {...get_record_request, ...get_record_data};

        return get_record_data.fields;
      } catch (e) {
        console.log("Error getJsonAirtableRecord:", e)
      }
    },
    async sendAirtable() {
      try {
        console.log(this);
        console.log(this.access.airtable.record_id);
        const dataToSend = JSON.stringify(this);
        this.spinners.send_data_btn = true;
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${this.access.airtable.token}`);
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Cookie", "brw=brwyExQCyl51dN17C");

        var raw = JSON.stringify({
          "records": [
            {
              "id": this.access.airtable.record_id,
              "fields": {
                "json": dataToSend,
              }
            }
          ]
        });

        var requestOptions = {
          method: 'PATCH',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        var notyf = new Notyf({position: {x: 'right', y: 'top'}});

        let requestAirtable = await fetch(`https://api.airtable.com/v0/${this.access.airtable.table_id}/Suivi%20Campagne`, requestOptions);
        let resultRequest = await requestAirtable.json();
        console.log('send data: ',requestAirtable, resultRequest);
        if (requestAirtable.status !== 200){
          notyf.error('Erreur lors de la sauvegarde de la campagne');
          throw {error: 'requestAirtable Error', ...requestAirtable};
        } else {
          notyf.success('La campagne à été sauvegardé');
          document.getElementById("btn_send_data").classList.add('green');
        }

        this.spinners.send_data_btn = false;
        console.log("raw", raw);
        console.log("token airtable", this.access.airtable.token);
        console.log("record id airtable", this.access.airtable.record_id);
        console.log(requestAirtable);
        console.log(resultRequest);
      } catch (e) {
        console.log("Error in sendAirtable() ==> ", e);
        alert('Erreur quand les données sont envoyées à airtable');
      }
    }
  });

  Alpine.store('modal_update_campaign_name', {
    open: false,
    country_code_options: [
      {code: 'AF', name: 'Afghanistan'},
      {code: 'AL', name: 'Albania'},
      {code: 'DZ', name: 'Algeria'},
      {code: 'AS', name: 'American Samoa'},
      {code: 'AD', name: 'Andorra'},
      {code: 'AO', name: 'Angola'},
      {code: 'AI', name: 'Anguilla'},
      {code: 'AQ', name: 'Antarctica'},
      {code: 'AG', name: 'Antigua and Barbuda'},
      {code: 'AR', name: 'Argentina'},
      {code: 'AM', name: 'Armenia'},
      {code: 'AW', name: 'Aruba'},
      {code: 'AU', name: 'Australia'},
      {code: 'AT', name: 'Austria'},
      {code: 'AZ', name: 'Azerbaijan'},
      {code: 'BS', name: 'Bahamas (the)'},
      {code: 'BH', name: 'Bahrain'},
      {code: 'BD', name: 'Bangladesh'},
      {code: 'BB', name: 'Barbados'},
      {code: 'BY', name: 'Belarus'},
      {code: 'BE', name: 'Belgium'},
      {code: 'BZ', name: 'Belize'},
      {code: 'BJ', name: 'Benin'},
      {code: 'BM', name: 'Bermuda'},
      {code: 'BT', name: 'Bhutan'},
      {code: 'BO', name: 'Bolivia (Plurinational State of)'},
      {code: 'BQ', name: 'Bonaire, Sint Eustatius and Saba'},
      {code: 'BA', name: 'Bosnia and Herzegovina'},
      {code: 'BW', name: 'Botswana'},
      {code: 'BV', name: 'Bouvet Island'},
      {code: 'BR', name: 'Brazil'},
      {code: 'IO', name: 'British Indian Ocean Territory (the)'},
      {code: 'BN', name: 'Brunei Darussalam'},
      {code: 'BG', name: 'Bulgaria'},
      {code: 'BF', name: 'Burkina Faso'},
      {code: 'BI', name: 'Burundi'},
      {code: 'CV', name: 'Cabo Verde'},
      {code: 'KH', name: 'Cambodia'},
      {code: 'CM', name: 'Cameroon'},
      {code: 'CA', name: 'Canada'},
      {code: 'KY', name: 'Cayman Islands (the)'},
      {code: 'CF', name: 'Central African Republic (the)'},
      {code: 'TD', name: 'Chad'},
      {code: 'CL', name: 'Chile'},
      {code: 'CN', name: 'China'},
      {code: 'CX', name: 'Christmas Island'},
      {code: 'CC', name: 'Cocos (Keeling) Islands (the)'},
      {code: 'CO', name: 'Colombia'},
      {code: 'KM', name: 'Comoros (the)'},
      {code: 'CD', name: 'Congo (the Democratic Republic of the)'},
      {code: 'CG', name: 'Congo (the)'},
      {code: 'CK', name: 'Cook Islands (the)'},
      {code: 'CR', name: 'Costa Rica'},
      {code: 'HR', name: 'Croatia'},
      {code: 'CU', name: 'Cuba'},
      {code: 'CW', name: 'Curaçao'},
      {code: 'CY', name: 'Cyprus'},
      {code: 'CZ', name: 'Czechia'},
      {code: 'CI', name: 'Côte d\'Ivoire'},
      {code: 'DK', name: 'Denmark'},
      {code: 'DJ', name: 'Djibouti'},
      {code: 'DM', name: 'Dominica'},
      {code: 'DO', name: 'Dominican Republic (the)'},
      {code: 'EC', name: 'Ecuador'},
      {code: 'EG', name: 'Egypt'},
      {code: 'SV', name: 'El Salvador'},
      {code: 'GQ', name: 'Equatorial Guinea'},
      {code: 'ER', name: 'Eritrea'},
      {code: 'EE', name: 'Estonia'},
      {code: 'SZ', name: 'Eswatini'},
      {code: 'ET', name: 'Ethiopia'},
      {code: 'FK', name: 'Falkland Islands (the) [Malvinas]'},
      {code: 'FO', name: 'Faroe Islands (the)'},
      {code: 'FJ', name: 'Fiji'},
      {code: 'FI', name: 'Finland'},
      {code: 'FR', name: 'France'},
      {code: 'GF', name: 'French Guiana'},
      {code: 'PF', name: 'French Polynesia'},
      {code: 'TF', name: 'French Southern Territories (the)'},
      {code: 'GA', name: 'Gabon'},
      {code: 'GM', name: 'Gambia (the)'},
      {code: 'GE', name: 'Georgia'},
      {code: 'DE', name: 'Germany'},
      {code: 'GH', name: 'Ghana'},
      {code: 'GI', name: 'Gibraltar'},
      {code: 'GR', name: 'Greece'},
      {code: 'GL', name: 'Greenland'},
      {code: 'GD', name: 'Grenada'},
      {code: 'GP', name: 'Guadeloupe'},
      {code: 'GU', name: 'Guam'},
      {code: 'GT', name: 'Guatemala'},
      {code: 'GG', name: 'Guernsey'},
      {code: 'GN', name: 'Guinea'},
      {code: 'GW', name: 'Guinea-Bissau'},
      {code: 'GY', name: 'Guyana'},
      {code: 'HT', name: 'Haiti'},
      {code: 'HM', name: 'Heard Island and McDonald Islands'},
      {code: 'VA', name: 'Holy See (the)'},
      {code: 'HN', name: 'Honduras'},
      {code: 'HK', name: 'Hong Kong'},
      {code: 'HU', name: 'Hungary'},
      {code: 'IS', name: 'Iceland'},
      {code: 'IN', name: 'India'},
      {code: 'ID', name: 'Indonesia'},
      {code: 'IR', name: 'Iran (Islamic Republic of)'},
      {code: 'IQ', name: 'Iraq'},
      {code: 'IE', name: 'Ireland'},
      {code: 'IM', name: 'Isle of Man'},
      {code: 'IL', name: 'Israel'},
      {code: 'IT', name: 'Italy'},
      {code: 'JM', name: 'Jamaica'},
      {code: 'JP', name: 'Japan'},
      {code: 'JE', name: 'Jersey'},
      {code: 'JO', name: 'Jordan'},
      {code: 'KZ', name: 'Kazakhstan'},
      {code: 'KE', name: 'Kenya'},
      {code: 'KI', name: 'Kiribati'},
      {code: 'KP', name: 'Korea (the Democratic People\'s Republic of)'},
      {code: 'KR', name: 'Korea (the Republic of)'},
      {code: 'KW', name: 'Kuwait'},
      {code: 'KG', name: 'Kyrgyzstan'},
      {code: 'LA', name: 'Lao People\'s Democratic Republic (the)'},
      {code: 'LV', name: 'Latvia'},
      {code: 'LB', name: 'Lebanon'},
      {code: 'LS', name: 'Lesotho'},
      {code: 'LR', name: 'Liberia'},
      {code: 'LY', name: 'Libya'},
      {code: 'LI', name: 'Liechtenstein'},
      {code: 'LT', name: 'Lithuania'},
      {code: 'LU', name: 'Luxembourg'},
      {code: 'MO', name: 'Macao'},
      {code: 'MG', name: 'Madagascar'},
      {code: 'MW', name: 'Malawi'},
      {code: 'MY', name: 'Malaysia'},
      {code: 'MV', name: 'Maldives'},
      {code: 'ML', name: 'Mali'},
      {code: 'MT', name: 'Malta'},
      {code: 'MH', name: 'Marshall Islands (the)'},
      {code: 'MQ', name: 'Martinique'},
      {code: 'MR', name: 'Mauritania'},
      {code: 'MU', name: 'Mauritius'},
      {code: 'YT', name: 'Mayotte'},
      {code: 'MX', name: 'Mexico'},
      {code: 'FM', name: 'Micronesia (Federated States of)'},
      {code: 'MD', name: 'Moldova (the Republic of)'},
      {code: 'MC', name: 'Monaco'},
      {code: 'MN', name: 'Mongolia'},
      {code: 'ME', name: 'Montenegro'},
      {code: 'MS', name: 'Montserrat'},
      {code: 'MA', name: 'Morocco'},
      {code: 'MZ', name: 'Mozambique'},
      {code: 'MM', name: 'Myanmar'},
      {code: 'NA', name: 'Namibia'},
      {code: 'NR', name: 'Nauru'},
      {code: 'NP', name: 'Nepal'},
      {code: 'NL', name: 'Netherlands (the)'},
      {code: 'NC', name: 'New Caledonia'},
      {code: 'NZ', name: 'New Zealand'},
      {code: 'NI', name: 'Nicaragua'},
      {code: 'NE', name: 'Niger (the)'},
      {code: 'NG', name: 'Nigeria'},
      {code: 'NU', name: 'Niue'},
      {code: 'NF', name: 'Norfolk Island'},
      {code: 'MP', name: 'Northern Mariana Islands (the)'},
      {code: 'NO', name: 'Norway'},
      {code: 'OM', name: 'Oman'},
      {code: 'PK', name: 'Pakistan'},
      {code: 'PW', name: 'Palau'},
      {code: 'PS', name: 'Palestine, State of'},
      {code: 'PA', name: 'Panama'},
      {code: 'PG', name: 'Papua New Guinea'},
      {code: 'PY', name: 'Paraguay'},
      {code: 'PE', name: 'Peru'},
      {code: 'PH', name: 'Philippines (the)'},
      {code: 'PN', name: 'Pitcairn'},
      {code: 'PL', name: 'Poland'},
      {code: 'PT', name: 'Portugal'},
      {code: 'PR', name: 'Puerto Rico'},
      {code: 'QA', name: 'Qatar'},
      {code: 'MK', name: 'Republic of North Macedonia'},
      {code: 'RO', name: 'Romania'},
      {code: 'RU', name: 'Russian Federation (the)'},
      {code: 'RW', name: 'Rwanda'},
      {code: 'RE', name: 'Réunion'},
      {code: 'BL', name: 'Saint Barthélemy'},
      {code: 'SH', name: 'Saint Helena, Ascension and Tristan da Cunha'},
      {code: 'KN', name: 'Saint Kitts and Nevis'},
      {code: 'LC', name: 'Saint Lucia'},
      {code: 'MF', name: 'Saint Martin (French part)'},
      {code: 'PM', name: 'Saint Pierre and Miquelon'},
      {code: 'VC', name: 'Saint Vincent and the Grenadines'},
      {code: 'WS', name: 'Samoa'},
      {code: 'SM', name: 'San Marino'},
      {code: 'ST', name: 'Sao Tome and Principe'},
      {code: 'SA', name: 'Saudi Arabia'},
      {code: 'SN', name: 'Senegal'},
      {code: 'RS', name: 'Serbia'},
      {code: 'SC', name: 'Seychelles'},
      {code: 'SL', name: 'Sierra Leone'},
      {code: 'SG', name: 'Singapore'},
      {code: 'SX', name: 'Sint Maarten (Dutch part)'},
      {code: 'SK', name: 'Slovakia'},
      {code: 'SI', name: 'Slovenia'},
      {code: 'SB', name: 'Solomon Islands'},
      {code: 'SO', name: 'Somalia'},
      {code: 'ZA', name: 'South Africa'},
      {code: 'GS', name: 'South Georgia and the South Sandwich Islands'},
      {code: 'SS', name: 'South Sudan'},
      {code: 'ES', name: 'Spain'},
      {code: 'LK', name: 'Sri Lanka'},
      {code: 'SD', name: 'Sudan (the)'},
      {code: 'SR', name: 'Suriname'},
      {code: 'SJ', name: 'Svalbard and Jan Mayen'},
      {code: 'SE', name: 'Sweden'},
      {code: 'CH', name: 'Switzerland'},
      {code: 'SY', name: 'Syrian Arab Republic'},
      {code: 'TW', name: 'Taiwan'},
      {code: 'TJ', name: 'Tajikistan'},
      {code: 'TZ', name: 'Tanzania, United Republic of'},
      {code: 'TH', name: 'Thailand'},
      {code: 'TL', name: 'Timor-Leste'},
      {code: 'TG', name: 'Togo'},
      {code: 'TK', name: 'Tokelau'},
      {code: 'TO', name: 'Tonga'},
      {code: 'TT', name: 'Trinidad and Tobago'},
      {code: 'TN', name: 'Tunisia'},
      {code: 'TR', name: 'Turkey'},
      {code: 'TM', name: 'Turkmenistan'},
      {code: 'TC', name: 'Turks and Caicos Islands (the)'},
      {code: 'TV', name: 'Tuvalu'},
      {code: 'UG', name: 'Uganda'},
      {code: 'UA', name: 'Ukraine'},
      {code: 'AE', name: 'United Arab Emirates (the)'},
      {code: 'GB', name: 'United Kingdom of Great Britain and Northern Ireland (the)'},
      {code: 'UM', name: 'United States Minor Outlying Islands (the)'},
      {code: 'US', name: 'United States of America (the)'},
      {code: 'UY', name: 'Uruguay'},
      {code: 'UZ', name: 'Uzbekistan'},
      {code: 'VU', name: 'Vanuatu'},
      {code: 'VE', name: 'Venezuela (Bolivarian Republic of)'},
      {code: 'VN', name: 'Viet Nam'},
      {code: 'VG', name: 'Virgin Islands (British)'},
      {code: 'VI', name: 'Virgin Islands (U.S.)'},
      {code: 'WF', name: 'Wallis and Futuna'},
      {code: 'EH', name: 'Western Sahara'},
      {code: 'YE', name: 'Yemen'},
      {code: 'ZM', name: 'Zambia'},
      {code: 'ZW', name: 'Zimbabwe'},
      {code: 'AX', name: 'Åland Islands'}
    ],
    toggle() {
        this.open = ! this.open;
        console.log(`open was updated: ${this.open}`);
    },
    updateCampaign(name) {
        Alpine.store('campaign').name = name;
        console.log(`campaign: name was updated: ${Alpine.store('campaign').name}`);
        this.toggle();
    },
    async switchCampaignType(choice) {
      if(Alpine.store('campaign').isTrafficCampaign != choice) {
        Alpine.store('campaign').isTrafficCampaign = choice;
        
        Alpine.store('campaign').groups = {
          facebook: [],
          snapchat: [],
          tiktok: [],
          twitter: []
        };
          
        console.log('groups was removed: ', Alpine.store('campaign').groups);

        console.log(`switchCampaignType was updated: ${Alpine.store('campaign').isTrafficCampaign}`);
      } else {
        console.log('no switch campaign type because the same choice previously');
      }
    },
  });

  Alpine.store('modal_add_facebook_group', {
    form: {
        name_input: "",
        price_input: "",
        date_input: "",
        age_min_select: "",
        age_max_select: "",
        name_ads: "",
        url_destination: "",
        title_ads: "",
        description_ads: "",
        main_text: "",
        fb_feed: "",
        fb_story: "",
        places: [],
        ads: [],
        locales: [],
    },
    open: false,
    errors: {
        name: "",
        price: "",
        date: "",
        age: "",
        places: "",
        ads: "",
        locales: ""
    },
    modify: false,
    init() {
        //Enable date picker's modals
        flatpickr(".date", {
            enableTime: false,
            dateFormat: "Y-m-d",
            minDate: new Date().fp_incr(1),
            onChange: function(dateObj, dateStr) {
                console.info(dateObj);
                console.info(dateStr);
                Alpine.store('modal_add_facebook_group').form.date_input = dateStr
            }
        });

        //init autocomplete for places
        const wikiUrl = `${Alpine.store('campaign').urlApi}/get_fb_places`;
        const params = `access_token=${Alpine.store('campaign').access.facebook.token}`;

        new Autocomplete('#autocomplete', {
          search: input => {
            const url = `${wikiUrl}?${params}&search=${encodeURI(input)}`

            return new Promise(resolve => {
              if (input.length < 3) {
                return resolve([])
              }

              fetch(url)
                .then(response => response.json())
                .then(data => {
                  console.log("get api places: ", data.datas);
                  resolve(data.datas)})
                .catch(e => console.log("Error autocomplete:", e));
            });
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            console.log(props);
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.name} <span class="snippet">(${result.type}, ${result.country_name}, ${result.region})</span>
                </div>
              </li>
            `
          },
          getResultValue: result => '',
          onSubmit: result => {
            console.log('clicked on:',  result);
            if (result.type === 'city') {
                if (document.getElementById('radius').value !== '') {
                    this.addPlaces({...result, ...{radius: parseInt(document.getElementById('radius').value), distance_unit: 'kilometer'}});
                } else {
                    this.addPlaces({...result, ...{radius: 17, distance_unit: 'kilometer'}});
                }
            } else {
                this.addPlaces(result);
            }
            console.log(this.addPlaces);
          }
        });

        //init autocomplete for locales
        const localesUrl = `${Alpine.store('campaign').urlApi}/get_fb_locales`;
        const localesparams = `access_token=${Alpine.store('campaign').access.facebook.token}`;

        new Autocomplete('#autocomplete-locales', {
          search: input => {
            const url = `${localesUrl}?${localesparams}&search=${encodeURI(input)}`

            return new Promise(resolve => {
              if (input.length < 3) {
                return resolve([])
              }

              fetch(url)
                .then(response => response.json())
                .then(data => {
                  console.log("get api locales: ", data.datas);
                  resolve(data.datas)
                })
                .catch(e => console.log("Error autocomplete:", e));
            })
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            console.log(props);
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.name}
                </div>
              </li>
            `
          },
          getResultValue: result => '',
          onSubmit: result => {
            console.log('clicked on:',  result);
            this.addLocales(result);
          }
        });
    },
    openModalCreate() {
        this.open = true;
        this.modify = false;

        this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.facebook.length + 1}`;
        this.form.price_input = "30";
        this.form.date_input = "";
        this.form.age_min_select = "18";
        this.form.age_max_select = "65";
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.title_ads = "Postulez 👈";
        this.form.description_ads = `par ${Alpine.store('campaign').client}`;
        this.form.main_text = "";
        this.form.fb_feed = "";
        this.form.fb_story = "";
        this.form.places = [];
        this.form.ads = [];
        this.form.locales = [{
            "name": "Français (France)",
            "key": 9
        }];
    },
    openModalModify(key) {
        this.open = true;
        this.modify = key;

        const ad_group_fb_toModify = Alpine.store('campaign').groups.facebook.filter(group => group.key === key)[0];

        console.log("ad_group_fb: ", ad_group_fb_toModify);

        this.form.name_input = ad_group_fb_toModify.name;
        this.form.price_input = ad_group_fb_toModify.price;
        this.form.date_input = ad_group_fb_toModify.date;
        this.form.age_min_select = ad_group_fb_toModify.age_min;
        this.form.age_max_select = ad_group_fb_toModify.age_max;
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.title_ads = "Postulez 👈";
        this.form.description_ads = `par ${Alpine.store('campaign').client}`;
        this.form.main_text = "";
        this.form.fb_feed = "";
        this.form.fb_story = "";

        document.getElementById('fb_feed').value = "";
        document.getElementById('fb_story').value = "";

        this.form.places = ad_group_fb_toModify.places;
        this.form.ads = ad_group_fb_toModify.ads;
        this.form.locales = ad_group_fb_toModify.locales;
    },
    closeModal() {
        this.open = false;
    },
    checkErrorForm() {
        let errorForm = false;

        this.errors = {
            name: "",
            price: "",
            date: "",
            age: "",
            places: "",
            ads: "",
            locales: "",
        };

        if (this.form.name_input === ""){
            this.errors.name = "Champs Nom vide";
            errorForm = true;
        }

        console.log("modify:", this.modify);
        if (this.modify !== false) {
            const ad_group_fb_key_noCheck = Alpine.store('campaign').groups.facebook.filter(group => group.key === this.modify)[0].key;
            console.log("ad_group_fb_key_noCheck:", ad_group_fb_key_noCheck);
            if (Alpine.store('campaign').groups.facebook.filter(group => (group.name === this.form.name_input && group.key !== ad_group_fb_key_noCheck))[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        } else {
            if (Alpine.store('campaign').groups.facebook.filter(group => group.name === this.form.name_input)[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        }
        if (this.form.price_input === ""){
            this.errors.price = "Champs Prix vide";
            errorForm = true;
        } 
        if (this.form.date_input === ""){
            console.log("yolo pourquoi c'est true ? ==> date_input:", this.form.date_input)
            this.errors.date = "Champs Date vide";
            errorForm = true;
        } 
        if (this.form.age_min_select > this.form.age_max_select) {
            this.errors.age = "Veuillez avoir un age min < age max";
            errorForm = true;
        } 
        if (this.form.places.length === 0) {
            this.errors.places = "Veuillez mettre au moins un endroit.";
            errorForm = true;
        }
        if (this.form.ads.length === 0) {
            this.errors.ads = "Veuillez mettre au moins une Pub.";
            errorForm = true;
        }
        if (this.form.locales.length === 0) {
            this.errors.locales = "Veuillez mettre au moins une langue.";
            errorForm = true;
        }

        console.log("pass into checkErrorForm:", this);
        console.log("ErrorForm:", errorForm);

        return errorForm;
    },
    checkPlacesError(place) {
        let errorPlaces = false;
        if (!place.radius) {
            errorPlaces = false;
        } else if (place.radius > 80 || place.radius < 17){
            errorPlaces = true;
        } else {
            errorPlaces = false;
        }

        return errorPlaces;
    },
    async addGroup() {
        console.log('add news Group Facebook ads');

        const formValid = this.checkErrorForm();
        if(!formValid) {
            const new_ad_group_fb = {
                key: `gp_fb_${Alpine.store('campaign').groups.facebook.length}`,
                name: this.form.name_input.trim(),
                price: this.form.price_input,
                date: this.form.date_input,
                age_min: this.form.age_min_select,
                age_max: this.form.age_max_select,
                places: this.form.places,
                ads: this.form.ads,
                locales: this.form.locales,
                network: "facebook"
            };

            Alpine.store('campaign').groups.facebook.push(new_ad_group_fb);

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_fb was updated:', Alpine.store('campaign').groups.facebook);
        }

        return true;
    },
    async modifyGroup(key) {
        console.log('modify Group Facebook ads');
        const formValid = this.checkErrorForm();
        console.log(`group key to modify: ${key}`);
        if(!formValid) {
            Alpine.store('campaign').groups.facebook.forEach((group, index) => {
                if(group.key === this.modify){
                    Alpine.store('campaign').groups.facebook[index].name = this.form.name_input.trim();
                    Alpine.store('campaign').groups.facebook[index].price = this.form.price_input;
                    Alpine.store('campaign').groups.facebook[index].date = this.form.date_input;
                    Alpine.store('campaign').groups.facebook[index].age_min = this.form.age_min_select;
                    Alpine.store('campaign').groups.facebook[index].age_max = this.form.age_max_select;
                    Alpine.store('campaign').groups.facebook[index].places = this.form.places;
                    Alpine.store('campaign').groups.facebook[index].ads = this.form.ads;
                    Alpine.store('campaign').groups.facebook[index].locales = this.form.locales;
                }
            });

            this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.facebook.length + 1}`;
            this.form.price_input = "20";
            this.form.date_input = "";
            this.form.age_min_select = "18";
            this.form.age_max_select = "65";

            this.form.name_ads = "";
            this.form.url_destination = "";
            this.form.title_ads = "Postulez 👈";
            this.form.description_ads = `par ${Alpine.store('campaign').client}`;
            this.form.main_text = "";

            this.form.fb_feed = "";
            this.form.fb_story = "";

            this.form.places = [];
            this.form.ads = [];
            this.form.locales = [];

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_fb was updated:', Alpine.store('campaign').groups.facebook);
        }

        return true;
    },
    removeGroup(index) {
        console.log(`Remove group with index ${index - 1}`);
        console.log(Alpine.store('campaign').groups.facebook);
        Alpine.store('campaign').groups.facebook.splice(index - 1, 1);
        console.log(Alpine.store('campaign').groups.facebook);
    },
    addPlaces(place) {
        console.log('passing by addPlaces: ', place);
        this.checkErrorForm();
        let placeError = this.checkPlacesError(place);
        if (!placeError) {
          console.log('CHECK DOUBLE: ', this.form.places.filter(placeAdded => placeAdded.key === place.key).length);
          if (this.form.places.filter(placeAdded => placeAdded.key === place.key).length > 0) {
            alert('vous avez dejà ajouté ce lieu a ce groupe de campagne !');
          } else {
            this.form.places.push(place);
            this.checkErrorForm();
            console.log('places was updated: ', this.form.places);
          }
        } else {
            alert("La range pour un ville doit se situer entre 17 et 80 km.")
        }
    },
    resetPlaces() {
        this.form.places = [];
        console.log('places was removed: ', this.form.places);
    },
    removePlace(id) {
      let indexPlace = this.form.places.findIndex(place => id === place.key);
      console.log('indexPlace: ', indexPlace);
      this.form.places.splice(indexPlace, 1);
      console.log(`place with id ${id} was removed: `, this.form.places);
    },
    addLocales(locales) {
      console.log('passing by addLocales: ', locales);
      console.log('CHECK DOUBLE: ', this.form.locales.filter(localeAdded => localeAdded.key === locales.key).length);
      if (this.form.locales.filter(localeAdded => localeAdded.key === locales.key).length > 0) {
        alert('vous avez dejà ajouté ce language a ce groupe de campagne !');
      } else {
        this.form.locales.push(locales);
        this.checkErrorForm();
        console.log('locales was updated: ', this.form.locales);
      }
    },
    resetLocales() {
        this.form.locales = [];
        console.log('places was removed: ', this.form.locales);
    },
    async checkFormat(ref_input, format, valueForm) {
      this.form[ref_input] = valueForm;

      let reader = new FileReader();
      let formatCalculed = format === "9x16" ? 9/16 : 1;
      var file = this.form[ref_input][0];
      var errorsFormat = [];

      const reset_upload = () => {
        console.log("reset_upload");
        this.form[ref_input] = "";
        document.getElementById(ref_input).value = "";
      }

      if (file.type.split('/')[0] === 'image') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 4999999) {
          errorsFormat.push(`Cette image est trop lourde ! (5 Mo max).`)
        }
        
        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              let img = new Image();
              img.src = e.target.result;

              img.onload = function () {
                var height = this.height;
                var width = this.width;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);

                //Check extension type
                if (!['image/png', 'image/jpeg', 'image/jpg'].includes(file.type)) {
                  errorsFormat.push(`Image have not a valid type extension => ${file.type}.`);
                }

                //Check ratio Image
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Image have not a valid ${format} format.`);
                  resolve(false);
                }

                //Check resolution Image
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1920).`);
                  }
                }

                resolve(true);
              }
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with image file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      } else if (file.type.split('/')[0] === 'video') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 9999999) {
          errorsFormat.push(`Video est trop lourd ! (10 Mo max) .`);
          document.getElementById(ref_input).value = "";
        }

        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              var dataUrl =  reader.result;
              var videoId = "videoMain";
              var $videoEl = $('<video id="' + videoId + '"></video>');
              $("body").append($videoEl);
              $videoEl.attr('src', dataUrl);

              var videoTagRef = $videoEl[0];

              videoTagRef.addEventListener('loadedmetadata', function(e) {
                e.target.dataset.width = 1080; 
                e.target.dataset.height = 1920;

                var height = videoTagRef.videoHeight;
                var width = videoTagRef.videoWidth;
                var duration = videoTagRef.duration;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);
                console.log("duration: ", duration);

                //Check extension type
                if (!['video/mp4'].includes(file.type)) {
                  errorsFormat.push(`Video have not a valid type extension => ${file.type}.`);
                }

                //Check ratio video
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Video have not a valid ratio ${format} format.`);
                }

                //Check resolution video
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1920).`);
                  }
                }

                //Check video duration
                if (duration > 20 || duration < 5) {
                  errorsFormat.push(`Video have not a valid duration (5s < video < 20).`);
                }

                resolve(true);
              })
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with video file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      }
    },
    removeAd(index) {
        console.log(this.form.ads, index);

        this.form.ads.splice(index, 1);

        console.log(this.form.ads); 
    },
    isValidUrl(_string) {
      const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
      return matchpattern.test(_string);
    },
    async addAd(ad) {
        this.errors.ads = "";
        console.log("url: ", this.isValidUrl(this.form.url_destination));

        if (this.form.name_ads === "") {
            this.errors.ads = 'Champs Nom vide';
            return true;
        } else if (this.form.ads.filter(ad => ad.name === this.form.name_ads)[0] !== undefined){
            this.errors.ads = "Champs Nom visuel identique à un autre groupe de pub";
            return true;
        } else if (this.form.url_destination === "") {
            this.errors.ads = 'Champs Url vide';
            return true;
        } else if (this.isValidUrl(this.form.url_destination) === false) {
            this.errors.ads = 'Champs Url Invalide';
            return true;
        } else if (this.form.title_ads === "") {
            this.errors.ads = 'Champs Titre vide';
            return true;
        } else if (this.form.description_ads === "") {
            this.errors.ads = 'Champs description vide';
            return true;
        } else if (this.form.main_text === "") {
            this.errors.ads = 'Champs Texte principal vide';
            return true;
        } else if (this.form.fb_feed.length === 0) {
            this.errors.ads = 'Champs placement 1x1 vide';
            return true;
        } else if (this.form.fb_story.length === 0) {
            this.errors.ads = 'Champs placement 9x16 vide';
            return true;
        } else {
            Alpine.store('campaign').spinners.send_pic_facebook = true;
            /* --- UPLOAD FACEBOOK MEDIA --- */
            //Upload fb_feed (1x1)

            let fetchMediaFeed = null;
            let resultFeed = null;

            const media_type_feed = this.form.fb_feed[0].type.split('/')[0];
            console.log('media_type_feed: ', media_type_feed);
            if (media_type_feed === "image") {
              var formdata = new FormData();
              formdata.append("access_token", Alpine.store('campaign').access.facebook.token);
              formdata.append("ad_account_id", Alpine.store('campaign').access.facebook.ad_account_id);
              formdata.append("image", this.form.fb_feed[0], this.form.fb_feed[0].name);

              var requestOptionsFeed = {
                method: 'POST',
                body: formdata,
                redirect: 'follow'
              };

              fetchMediaFeed = await fetch(`${Alpine.store('campaign').urlApi}/upload_fb_image`, requestOptionsFeed);
              resultFeed = await fetchMediaFeed.json();
            } else if (media_type_feed === "video") {
              var formdata = new FormData();
              formdata.append("access_token", Alpine.store('campaign').access.facebook.token);
              formdata.append("ad_account_id", Alpine.store('campaign').access.facebook.ad_account_id);
              formdata.append("video", this.form.fb_feed[0], this.form.fb_feed[0].name);

              var requestOptionsStory = {
                method: 'POST',
                body: formdata,
                redirect: 'follow'
              };

              fetchMediaFeed = await fetch(`${Alpine.store('campaign').urlApi}/upload_fb_video`, requestOptionsStory);
              resultFeed = await fetchMediaFeed.json();
            } else {
              throw 'Error with media_type_feed video / image';
            }

            //Upload fb_story (9x16)
            const media_type_story_int = this.form.fb_story[0].type.split('/')[0];
            console.log('media_type_story_int: ', media_type_story_int);

            let fetchMediaStory = null;
            let resultStory = null;

            if (media_type_story_int === "image") {
              var formdataStory = new FormData();
              formdataStory.append("access_token", Alpine.store('campaign').access.facebook.token);
              formdataStory.append("ad_account_id", Alpine.store('campaign').access.facebook.ad_account_id);
              formdataStory.append("image", this.form.fb_story[0], this.form.fb_story[0].name);

              var requestOptionsStory = {
                method: 'POST',
                body: formdataStory,
                redirect: 'follow'
              };

              fetchMediaStory = await fetch(`${Alpine.store('campaign').urlApi}/upload_fb_image`, requestOptionsStory);
              resultStory = await fetchMediaStory.json();
            } else if (media_type_story_int === "video") {
              var formdataStory = new FormData();
              formdataStory.append("access_token", Alpine.store('campaign').access.facebook.token);
              formdataStory.append("ad_account_id", Alpine.store('campaign').access.facebook.ad_account_id);
              formdataStory.append("video", this.form.fb_story[0], this.form.fb_story[0].name);

              var requestOptionsStory = {
                method: 'POST',
                body: formdataStory,
                redirect: 'follow'
              };

              fetchMediaStory = await fetch(`${Alpine.store('campaign').urlApi}/upload_fb_video`, requestOptionsStory);
              resultStory = await fetchMediaStory.json();
            } else {
              throw 'Error with media_type_story_int video / image';
            }

            if (fetchMediaFeed.status === 200 && fetchMediaStory.status === 200) {
                console.log("resultStory: ", resultStory);
                console.log("resultFeed: ", resultFeed);

                this.form.ads.push({
                    key: (this.form.ads.length - 1) + 1,
                    name: this.form.name_ads.trim(),
                    url: this.form.url_destination.trim(),
                    title_ads: this.form.title_ads.trim(),
                    main_text: this.form.main_text.trim(),
                    description: this.form.description_ads,
                    feed_fb: { //format 1:1
                      type: media_type_feed,
                      hash: media_type_feed == 'video' ? resultFeed.datas.id : resultFeed.datas.hash,
                      url_preview: media_type_feed == 'video' ? resultFeed.datas.source : resultFeed.datas.permalink_url,
                    },
                    feed_inst: { //format 1:1
                      type: media_type_feed,
                      hash: media_type_feed == 'video' ? resultFeed.datas.id : resultFeed.datas.hash,
                      url_preview: media_type_feed == 'video' ? resultFeed.datas.source : resultFeed.datas.permalink_url,
                    },
                    stories_inst: { //format 9:16
                      type: media_type_story_int,
                      hash: media_type_story_int == 'video' ? resultStory.datas.id : resultStory.datas.hash,
                      url_preview: media_type_story_int == 'video' ? resultStory.datas.source : resultStory.datas.permalink_url,
                    }
                });
                Alpine.store('campaign').spinners.send_pic_facebook = false;

                console.log('Ads was updated: ', this.form.ads);
                return true;
            } else {
                alert("Une erreur est survenue lors de l\'upload de media");
                console.log("request uplood Feed: ", resultFeed);
                console.log("request uplood story: ", resultStory);
                Alpine.store('campaign').spinners.send_pic_facebook = false;
                return false;
            }
        }
    },
  });

  Alpine.store('modal_add_snapchat_group', {
    form: {
        name_input: "",
        price_input: "",
        date_input: "",
        age_min_select: "",
        age_max_select: "",
        name_ads: "",
        url_destination: "",
        brand_name: "",
        headline: "",
        snap_story: "",
        places: [],
        places_geos: [],
        ads: [],
        locales: [],
        locale_options: [],
        switchToMapLocation: false,
        country_options: [],
        country_selected: null,
        snap_search_list_options: [],
        map_google: null
    },
    open: false,
    errors: {
        name: "",
        price: "",
        date: "",
        age: "",
        places: "",
        ads: "",
        locales: ""
    },
    modify: false,
    async init() {
        //Enable date picker's modals
        flatpickr(".date_snap", {
            enableTime: false,
            dateFormat: "Y-m-d",
            minDate: new Date().fp_incr(1),
            onChange: function(dateObj, dateStr) {
                console.info(dateObj);
                console.info(dateStr);
                Alpine.store('modal_add_snapchat_group').form.date_input = dateStr
            }
        });

        //await this.getAccessTokenSnapchat(Alpine.store('campaign').access.snapchat.refresh_token);

        //Country autocomplete
        new Autocomplete('#autocomplete-snap-country', {
          search: async input => {
            if(input.length === 0) {
              return ([{
                "id": "250",
                "name": "france",
                "code": "fr",
              }]);
            }
            if (input.length < 2) {
              return [];
            }

            return this.form.country_options.filter(country => {
              return country.name.toLowerCase()
                .startsWith(input.toLowerCase())
            });
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.name} <span class="snippet">(${result.code})</span>
                </div>
              </li>
            `
          },
          getResultValue: result => result.name,
          onSubmit: async result => {
            await this.addCountry(result);
            await this.getAllLocationSnapchat(this.form.country_selected.code);
            await this.updateMapGoogle();
            console.log("Country selected: ", result);
          }
        });

        //Places autocomplete
        new Autocomplete('#autocomplete-snap-places', {
          search: async input => {
            console.log('je tape et je recherche: ', input, this.form.snap_search_list_options);
            return this.form.snap_search_list_options.filter(option => {
              return option.name.toLowerCase()
                .startsWith(input.toLowerCase())
            });
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.name} <span class="snippet">(${result.type})</span>
                </div>
              </li>
            `
          },
          getResultValue: result => "",
          onSubmit: async result => {
            this.addPlace(result);
            console.log("Places selected: ", result);
          }
        });

        //INIT autocomplete googleMapSeach
        new Autocomplete('#autocomplete-snap-places-google', {
          search: async input => {
            try {
              if(input.length > 2) {
                console.log("input: ", input);
                const results = await this.geocodeAddress(input);

                console.log("Autocomplete google: ", results);

                return results;
              } else {
                return [];
              }
            } catch (e) {
              console.log("Autocomplete google failed: ", e);
            }
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.formatted_address}
                </div>
              </li>
            `
          },
          getResultValue: result => "",
          onSubmit: async result => {
            console.log(result.geometry.location.lat(), result.geometry.location.lng());

            let radius = 20
            if (document.getElementById('radius-snap').value !== '') {
                radius = parseInt(document.getElementById('radius-snap').value);
            }
            this.addPlaceGeo({...result, radius: radius, lat: result.geometry.location.lat(), lng: result.geometry.location.lng()});
            await this.updateMapGoogle();
            
            console.log("Places selected: ", result);
          }
        });

        //init autocomplete for locales
        new Autocomplete('#autocomplete-locales-snap', {
          search: input => {
            if(input.length === 0) {
              return ([{
                name: 'French',
                id: 'fr'
              }]);
            }
            if (input.length < 2) {
              return [];
            }

            return this.form.locale_options.filter(locale => {
              return locale.name.toLowerCase()
                .startsWith(input.toLowerCase())
            });
          },
          debounceTime: 500,
          renderResult: (result, props) => {
            console.log(props);
            return `
              <li ${props} style="list-style-type: none;cursor: pointer;">
                <div class="title">
                  ${result.name}
                </div>
              </li>
            `
          },
          getResultValue: result => '',
          onSubmit: result => {
            console.log('clicked on:',  result);
            this.addLocales(result);
          }
        });
    },
    async updateMapGoogle() {
      try {
        console.log("passing by updateMapGoogle");
        let places_geo = this.form.places_geos;

        //Init map with the country_selected
        if (this.form.map_google === null) {
            console.log("First init map with france");
            const country_selected = "france";
            const geocoder = new google.maps.Geocoder();

            const geo_request = await geocoder.geocode( {'address' : country_selected});
            this.form.map_google = new google.maps.Map(document.getElementById("map"), {
              center: geo_request.results[0].geometry.location,
              zoom: 5,
              maxZoom: 10
            });
        }
        if (this.form.places_geos.length === 0) {
          console.log("this.form.places_geos.length: ", this.form.places_geos.length)
          if (!this.form.country_selected) {
            console.log("init map with france");
            const country_selected = "france";
            const geocoder = new google.maps.Geocoder();

            const geo_request = await geocoder.geocode( {'address' : country_selected});
            Alpine.store('modal_add_snapchat_group').form.map_google.setCenter(geo_request.results[0].geometry.location);
          } else {
            console.log("init map with: ", this.form.country_selected.name);
            const country_selected = this.form.country_selected.name;
            const geocoder = new google.maps.Geocoder();

            const geo_request = await geocoder.geocode({'address' : country_selected});
            Alpine.store('modal_add_snapchat_group').form.map_google.setCenter(geo_request.results[0].geometry.location);
          }
        }
        //Set map with markers and circles
        else {
          /* Déclaration de l'objet qui définira les limites de la map */ 
          let bounds = new google.maps.LatLngBounds();

          /* Chargement de la carte avec un type ROADMAP */
          let map = Alpine.store('modal_add_snapchat_group').form.map_google;

          /* Boucle sur les points afin d'ajouter les markers à la map
          et aussi d'étendre ses limites (bounds) grâce à la méthode extend */ 
          this.form.places_geos.forEach(place => {
            let point = new google.maps.LatLng(place.lat, place.lng);
            let marker = new google.maps.Marker({position: point});

            const circle = new google.maps.Circle({
              strokeColor: "#f38038",
              strokeOpacity: 0.4,
              strokeWeight: 2,
              fillColor: "#f38038",
              fillOpacity: 0.25,
              map: map,
              center: new google.maps.LatLng(place.lat, place.lng),
              radius: place.radius * 1000, //km
            });

            bounds.extend(point);
            marker.setMap(map);
            circle.setMap(map);
          })

          /* Ici, on ajuste le zoom de la map en fonction des limites  */ 
          map.fitBounds(bounds);
        }
      } catch (e) {
        console.log("Error updateMapGoogle: ", e);
      }
    },
    async geocodeAddress(city_searched) {
      try {
        var geocoder = new google.maps.Geocoder();
        var mapG = Alpine.store('modal_add_snapchat_group').form.map_google;

        const results = await geocoder
          .geocode({
            address: city_searched,
            componentRestrictions: {
              country: Alpine.store('modal_add_snapchat_group').form.country_selected.code,
            },
        });

        const test_results = await geocoder
          .geocode({
            address: city_searched,
        });


        console.log("results: ", results);

        const result_filtered_by_locality = results.results.filter(result => result.types.includes("locality"));

        console.log("result_filtered_by_locality: ", result_filtered_by_locality);
        console.log("result_filtered places: ", test_results.results);

        return result_filtered_by_locality;
      } catch (e) {
        console.log("Geocode was not successful for the following reason: " + e);
        return [];
      }
    },
    isNum(val) {
      return !isNaN(val)
    },
    async openModalCreate() {
        Alpine.store('campaign').spinners.send_open_modal_btn = true;
        await this.getAccessTokenSnapchat(Alpine.store('campaign').access.snapchat.refresh_token);

        this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.snapchat.length + 1}`;
        this.form.price_input = "20";
        this.form.date_input = "";
        this.form.age_min_select = "18";
        this.form.age_max_select = "50";
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.brand_name = `${Alpine.store('campaign').client}`;
        this.form.headline = "Rejoignez-nous !";
        this.form.snap_story = "";
        this.form.places = [];
        this.form.places_geos = [];
        this.form.ads = [];
        this.form.locales = [{
            "name": "French",
            "id": "fr"
        }];
        this.form.locale_options = [];
        this.form.switchToMapLocation = false;
        this.form.country_options = [];
        this.form.country_selected = null;
        this.form.snap_search_list_options = [];
        this.form.map_google = null;

        await this.updateMapGoogle();
        await this.getAccessTokenSnapchat(Alpine.store('campaign').access.snapchat.refresh_token);
        await this.getCountrySnapchat();
        await this.getLanguageSnapchat();

        document.getElementById('snap_story').value = "";
        document.getElementById('snap_country').value = "";

        this.open = true;
        this.modify = false;

        Alpine.store('campaign').spinners.send_open_modal_btn = false;
    },
    async openModalModify(key) {
        Alpine.store('campaign').spinners.send_open_modal_btn = true;
        await this.getAccessTokenSnapchat(Alpine.store('campaign').access.snapchat.refresh_token);

        this.modify = key;
        const ad_group_sp_toModify = Alpine.store('campaign').groups.snapchat.filter(group => group.key === key)[0];

        console.log("ad_group_fb: ", ad_group_sp_toModify);

        this.form.locale_options = [];
        this.form.country_options = [];
        this.form.snap_search_list_options = [];
        this.form.places = ad_group_sp_toModify.places;
        this.form.places_geos = ad_group_sp_toModify.places_geos;
        this.form.map_google = null;

        await this.updateMapGoogle();
        await this.getCountrySnapchat();
        await this.getLanguageSnapchat();
        await this.getAllLocationSnapchat(ad_group_sp_toModify.country_selected.code);

        this.form.name_input = ad_group_sp_toModify.name;
        this.form.price_input = ad_group_sp_toModify.price;
        this.form.date_input = ad_group_sp_toModify.date;
        this.form.age_min_select = ad_group_sp_toModify.age_min;
        this.form.age_max_select = ad_group_sp_toModify.age_max;
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.brand_name = `${Alpine.store('campaign').client}`;
        this.form.headline = "Rejoignez-nous !";
        this.form.snap_story = "";
        this.form.country_selected = ad_group_sp_toModify.country_selected;
        this.form.switchToMapLocation = ad_group_sp_toModify.switchToMapLocation;

        document.getElementById('snap_story').value = "";
        document.getElementById('snap_country').value = ad_group_sp_toModify.country_selected.name;

        this.form.ads = ad_group_sp_toModify.ads;
        this.form.locales = ad_group_sp_toModify.locales;

        this.open = true;
        Alpine.store('campaign').spinners.send_open_modal_btn = false;
    },
    closeModal() {
        this.open = false;
    },
    checkErrorForm() {
        let errorForm = false;

        this.errors = {
            name: "",
            price: "",
            date: "",
            age: "",
            places: "",
            ads: "",
            locales: "",
        };

        if (this.form.name_input === ""){
            this.errors.name = "Champs Nom vide";
            errorForm = true;
        }

        console.log("modify:", this.modify);
        if (this.modify !== false) {
            const ad_group_sp_key_noCheck = Alpine.store('campaign').groups.snapchat.filter(group => group.key === this.modify)[0].key;
            console.log("ad_group_sp_key_noCheck:", ad_group_sp_key_noCheck);
            if (Alpine.store('campaign').groups.snapchat.filter(group => (group.name === this.form.name_input && group.key !== ad_group_sp_key_noCheck))[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        } else {
            if (Alpine.store('campaign').groups.snapchat.filter(group => group.name === this.form.name_input)[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        }
        if (this.form.price_input === ""){
            this.errors.price = "Champs Prix vide";
            errorForm = true;
        } 
        if (this.form.price_input < 20){
            this.errors.price = "Veuillez mettre un minimum de 30 Euros";
            errorForm = true;
        } 
        if (this.form.date_input === ""){
            this.errors.date = "Champs Date vide";
            errorForm = true;
        } 
        if (this.form.age_min_select > this.form.age_max_select) {
            this.errors.age = "Veuillez avoir un age min < age max";
            errorForm = true;
        } 
        if (!this.form.country_selected) {
            this.errors.places = "Veuillez mettre au moins un pays.";
            errorForm = true;
        }
        if (this.form.places.includes(null)) {
          this.errors.places = "Une erreur est survenue avec l'endroit de diffusion de votre groupe.";
          errorForm = true;
        }
        /*
        if (this.form.places.length === 0) {
          if (this.form.places_geos.length === 0) {
            this.errors.places = "Veuillez mettre au moins un endroit.";
            errorForm = true;
          }
        }
        if (this.form.places_geos.length === 0) {
          if (this.form.places.length === 0) {
            this.errors.places = "Veuillez mettre au moins un endroit.";
            errorForm = true;
          }
        }
        */
        if (this.form.ads.length === 0) {
            this.errors.ads = "Veuillez mettre au moins une Pub.";
            errorForm = true;
        }
        if (this.form.locales.length === 0) {
            this.errors.locales = "Veuillez mettre au moins une langue.";
            errorForm = true;
        }

        console.log("pass into checkErrorForm:", this);
        console.log("ErrorForm:", errorForm);
        console.log("this.form:", this.form);

        return errorForm;
    },
    checkErrorFormPlaceGeo (place) {
      console.log("passing by checkErrorFormPlaceGeo: ", place);
      let errorPlaces = false;
      if (!place.radius) {
          errorPlaces = false;
      } else if (place.radius > 100 || place.radius < 0){
          errorPlaces = true;
      } else {
          errorPlaces = false;
      }

        return errorPlaces;
    },
    async addGroup() {
        console.log('add news Group Snapchat ads');

        const formValid = this.checkErrorForm();
        if(!formValid) {
            const new_ad_group_sp = {
                key: `gp_sp_${Alpine.store('campaign').groups.snapchat.length}`,
                name: this.form.name_input.trim(),
                price: this.form.price_input,
                date: this.form.date_input,
                age_min: this.form.age_min_select,
                age_max: this.form.age_max_select,
                places: this.form.places,
                places_geos: this.form.places_geos,
                country_selected: this.form.country_selected,
                switchToMapLocation: this.form.switchToMapLocation,
                ads: this.form.ads,
                locales: this.form.locales,
                network: "snapchat"
            };

            Alpine.store('campaign').groups.snapchat.push(new_ad_group_sp);

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_sp was updated:', Alpine.store('campaign').groups.snapchat);
        }

        return true;
    },
    async modifyGroup(key) {
        console.log('modify Group Snapchat ads');
        const formValid = this.checkErrorForm();
        console.log(`group key to modify: ${key}`);
        if(!formValid) {
            Alpine.store('campaign').groups.snapchat.forEach((group, index) => {
                if(group.key === this.modify){
                    Alpine.store('campaign').groups.snapchat[index].name = this.form.name_input.trim();
                    Alpine.store('campaign').groups.snapchat[index].price = this.form.price_input;
                    Alpine.store('campaign').groups.snapchat[index].date = this.form.date_input;
                    Alpine.store('campaign').groups.snapchat[index].age_min = this.form.age_min_select;
                    Alpine.store('campaign').groups.snapchat[index].age_max = this.form.age_max_select;
                    Alpine.store('campaign').groups.snapchat[index].places = this.form.places;
                    Alpine.store('campaign').groups.snapchat[index].places_geos = this.form.places_geos;
                    Alpine.store('campaign').groups.snapchat[index].ads = this.form.ads;
                    Alpine.store('campaign').groups.snapchat[index].locales = this.form.locales;
                    Alpine.store('campaign').groups.snapchat[index].country_selected = this.form.country_selected;
                    Alpine.store('campaign').groups.snapchat[index].switchToMapLocation = this.form.switchToMapLocation;
                }
            });

            this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.snapchat.length + 1}`;
            this.form.price_input = "20";
            this.form.date_input = "";
            this.form.age_min_select = "18";
            this.form.age_max_select = "50";

            this.form.name_ads = "";
            this.form.url_destination = "";
            this.form.brand_name = `par ${Alpine.store('campaign').client}`;
            this.form.headline = "Rejoignez-nous !";

            this.form.snap_story = "";
            this.form.map_google = null;

            this.form.places = [];
            this.form.places_geos = [];
            this.form.ads = [];
            this.form.locales = [];

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_sp was updated:', Alpine.store('campaign').groups.snapchat);
        }

        return true;
    },
    removeGroup(index) {
        console.log(`Remove group with index ${index - 1}`);
        console.log(Alpine.store('campaign').groups.snapchat);
        Alpine.store('campaign').groups.snapchat.splice(index - 1, 1);
        console.log(Alpine.store('campaign').groups.snapchat);
    },
    async addCountry(country) {
        console.log('passing by addCountry: ', country);
        this.form.country_selected = country;
        await this.resetPlaces();
        console.log('country was updated: ', this.form.country_selected);
    },
    addPlace(place) {
      console.log('passing by addPlace: ', place);
      console.log('CHECK DOUBLE: ', this.form.places.filter(placeAdded => placeAdded.id === place.id).length);
      if (this.form.places.filter(placeAdded => placeAdded.id === place.id).length > 0) {
        alert('vous avez dejà ajouté ce lieu a ce groupe de campagne !');
      } else {
        this.form.places.push(place);
        this.checkErrorForm();
        console.log('places was updated: ', this.form.places);
      }
    },
    addPlaceGeo(geo) {
      console.log('passing by addPlaceGeo: ', geo);
      let placeError = this.checkErrorFormPlaceGeo(geo);
      if (!placeError) {
        console.log('CHECK DOUBLE: ', this.form.places_geos.filter(placeAdded => placeAdded.place_id === geo.place_id).length);
        if (this.form.places_geos.filter(placeAdded => placeAdded.place_id === geo.place_id).length > 0) {
          alert('vous avez dejà ajouté ce lieu a ce groupe de campagne !');
        } else {
          this.form.places_geos.push(geo);
          this.checkErrorForm();
          console.log('places was updated: ', this.form.places_geos);
        }
      } else {
          alert("La range pour un ville doit se situer entre 0 et 100 km.")
      }
    },
    async resetPlaces() {
      this.form.places = [];
      this.form.places_geos = [];
      this.form.map_google = null;
      this.form.snap_search_list_options = [];
      document.getElementById('place-input-snap').value = "";
      await this.updateMapGoogle();
      await this.getAllLocationSnapchat(this.form.country_selected.code);
      console.log('places & places_geos was removed: ', this.form.places, this.form.places_geos);
    },
    async removePlace(id, isMap) {
      if (isMap) {
        this.form.places = [];
        this.form.map_google = null;
        this.form.snap_search_list_options = [];
        
        let indexPlace = this.form.places_geos.findIndex(place => id === place.place_id);
        console.log('indexPlace: ', indexPlace);
        this.form.places_geos.splice(indexPlace, 1);
        await this.updateMapGoogle();

        console.log(`place with id ${id} was removed: `, this.form.places);
      } else {
        this.form.places_geos = [];
        this.form.map_google = null;
        this.form.snap_search_list_options = [];

        let indexPlace = this.form.places.findIndex(place => id === place.index);
        console.log('indexPlace: ', indexPlace);
        this.form.places.splice(indexPlace, 1);
        await this.updateMapGoogle();
        console.log(`place with id ${id} was removed: `, this.form.places);
      }
    },
    async resetCountryPlaces() {
      this.form.country_selected = null;
      await this.resetPlaces();
      this.form.map_google = null;
      await this.updateMapGoogle();
      document.getElementById('snap_country').value = "";
      console.log('resetCountryPlaces: ', this.form.country_selected);
    },
    addLocales(locales) {
      console.log('passing by addLocales: ', locales);
      console.log('CHECK DOUBLE: ', this.form.locales.filter(localeAdded => localeAdded.id === locales.id).length);
      if (this.form.locales.filter(localeAdded => localeAdded.id === locales.id).length > 0) {
        alert('vous avez dejà ajouté ce language a ce groupe de campagne !');
      } else {
        this.form.locales.push(locales);
        this.checkErrorForm();
        console.log('locales was updated: ', this.form.locales);
      }
    },
    resetLocales() {
        this.form.locales = [];
        console.log('places was removed: ', this.form.locales);
    },
    async checkFormat(ref_input, format, valueForm) {
      this.form[ref_input] = valueForm;

      let reader = new FileReader();
      let formatCalculed = format === "9x16" ? 9/16 : 1;
      var file = this.form[ref_input][0];
      var errorsFormat = [];

      const reset_upload = () => {
        console.log("reset_upload");
        this.form[ref_input] = "";
        document.getElementById(ref_input).value = "";
      }

      if (file.type.split('/')[0] === 'image') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 4999999) {
          errorsFormat.push(`Cette image est trop lourde ! (5 Mo max).`)
        }
        
        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              let img = new Image();
              img.src = e.target.result;

              img.onload = function () {
                var height = this.height;
                var width = this.width;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);

                //Check extension type
                if (!['image/png', 'image/jpeg', 'image/jpg'].includes(file.type)) {
                  errorsFormat.push(`Image have not a valid type extension => ${file.type}.`);
                }

                //Check ratio Image
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Image have not a valid ${format} format.`);
                  resolve(false);
                }

                //Check resolution Image
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1920).`);
                  }
                }

                resolve(true);
              }
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with image file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      } else if (file.type.split('/')[0] === 'video') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 9999999) {
          errorsFormat.push(`Video est trop lourd ! (10 Mo max) .`);
          document.getElementById(ref_input).value = "";
        }

        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              var dataUrl =  reader.result;
              var videoId = "videoMain";
              var $videoEl = $('<video id="' + videoId + '"></video>');
              $("body").append($videoEl);
              $videoEl.attr('src', dataUrl);

              var videoTagRef = $videoEl[0];

              videoTagRef.addEventListener('loadedmetadata', function(e) {
                e.target.dataset.width = 1080; 
                e.target.dataset.height = 1920;

                var height = videoTagRef.videoHeight;
                var width = videoTagRef.videoWidth;
                var duration = videoTagRef.duration;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);
                console.log("duration: ", duration);

                //Check extension type
                if (!['video/mp4'].includes(file.type)) {
                  errorsFormat.push(`Video have not a valid type extension => ${file.type}.`);
                }

                //Check ratio video
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Video have not a valid ratio ${format} format.`);
                }

                //Check resolution video
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1920).`);
                  }
                }

                //Check video duration
                if (duration > 20 || duration < 5) {
                  errorsFormat.push(`Video have not a valid duration (5s < video < 20).`);
                }

                resolve(true);
              })
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with video file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      }
    },
    removeAd(index) {
        console.log(this.form.ads, index);

        this.form.ads.splice(index, 1);

        console.log(this.form.ads); 
    },
    isValidUrl(_string) {
      const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
      return matchpattern.test(_string);
    },
    async addAd(ad) {
      try {
        this.errors.ads = "";
        console.log("url: ", this.isValidUrl(this.form.url_destination));
        console.log("this.form.ads: ", this.form.ads);
        console.log("this.form.name_ads: ", this.form.name_ads);
        if (this.form.name_ads === "") {
            this.errors.ads = 'Champs Nom vide';
            return true;
        } else if (this.form.ads.filter(ad => ad.name_ads === this.form.name_ads)[0] !== undefined){
            this.errors.ads = "Champs Nom visuel identique à un autre groupe de pub";
            return true;
        } else if (this.form.url_destination === "") {
            this.errors.ads = 'Champs Url vide';
            return true;
        } else if (this.isValidUrl(this.form.url_destination) === false) {
            this.errors.ads = 'Champs Url Invalide';
            return true;
        } else if (this.form.brand_name === "") {
            this.errors.ads = 'Champs brandname vide';
            return true;
        } else if (this.form.headline === "") {
            this.errors.ads = 'Champs headline vide';
            return true;
        } else if (this.form.snap_story.length === 0) {
            this.errors.ads = 'Champs placement 9x16 vide';
            return true;
        } else {
            Alpine.store('campaign').spinners.send_pic_snapchat = true;
            /* --- UPLOAD SNAPCHAT MEDIA --- */

            const media_type = this.form.snap_story[0].type.split('/')[0];
            console.log('media_type: ', media_type);

            let upload_sp_request = null;
            let upload_sp_response = null;

            if (media_type === "image") {
              // 1 - Create media & upload image with media
              var formdata = new FormData();
              formdata.append("access_token", Alpine.store('campaign').access.snapchat.token);
              formdata.append("ad_account_id", Alpine.store('campaign').access.snapchat.ad_account_id);
              formdata.append("image", this.form.snap_story[0], this.form.snap_story[0].name);

              var requestOptions = {
                method: 'POST',
                body: formdata,
                redirect: 'follow'
              };

              upload_sp_request = await fetch(`${Alpine.store('campaign').urlApi}/upload_sp_image`, requestOptions);
              upload_sp_response = await upload_sp_request.json();

              console.log("upload_sp_request: ", upload_sp_request);
              console.log("upload_sp_response: ", upload_sp_response);

              if (upload_sp_request.status !== 200)
                throw {...upload_sp_request, ...upload_sp_response};
            } else if (media_type === "video") {
              // 1 - Create media & upload video with media
              var formdata = new FormData();
              formdata.append("access_token", Alpine.store('campaign').access.snapchat.token);
              formdata.append("ad_account_id", Alpine.store('campaign').access.snapchat.ad_account_id);
              formdata.append("video", this.form.snap_story[0], this.form.snap_story[0].name);

              var requestOptions = {
                method: 'POST',
                body: formdata,
                redirect: 'follow'
              };

              upload_sp_request = await fetch(`${Alpine.store('campaign').urlApi}/upload_sp_video`, requestOptions);
              upload_sp_response = await upload_sp_request.json();

              console.log("upload_sp_request: ", upload_sp_request);
              console.log("upload_sp_response: ", upload_sp_response);

              if (upload_sp_request.status !== 200)
                throw {...upload_sp_request, ...upload_sp_response};
            } else {
              throw 'Error with media_type video / image';
            }

            //4 Update Alpine store
            this.form.ads.push({
              key: (this.form.ads.length - 1) + 1,
              name_ads: this.form.name_ads.trim(),
              url: `${this.form.url_destination}`.trim(),
              brand_name: this.form.brand_name.trim(),
              headline: this.form.headline.trim(),
              snap_story: {
                type: media_type,
                media_id: upload_sp_response.datas.media_created.id,
                url_preview: upload_sp_response.datas.download_link,
              }
            });
            Alpine.store('campaign').spinners.send_pic_snapchat = false;

            console.log('Ads was updated: ', this.form.ads);
        }
      } catch (e) {
        alert("Une erreur est survenue lors de l\'upload de media chez snapchat");
        console.log("error snapchat -> addAd: ", e);
        return false;
      }
    },
    async switchLocation(choice) {
        if(this.form.switchToMapLocation != choice) {
            this.form.switchToMapLocation = choice;
            await this.resetPlaces();
            
            await this.updateMapGoogle();
            await this.getAllLocationSnapchat(this.form.country_selected.code);
            console.log(`switchToMapLocation was updated: ${this.form.switchToMapLocation}`);
        } else {
            console.log('no switch because the same choice previously');
        }
    },
    async getAccessTokenSnapchat(refresh_token) {
      try {
        var requestOptions = {
          method: 'GET',
          redirect: 'follow'
        };

        const getRefreshTokenSP_request = await fetch(`${Alpine.store('campaign').urlApi}/sp_refresh_token?refresh_token=${refresh_token}`, requestOptions);
        const getRefreshTokenSP_response = await getRefreshTokenSP_request.json();

        console.log("getRefreshTokenSP_request: ", getRefreshTokenSP_request);
        console.log("getRefreshTokenSP_response: ", getRefreshTokenSP_response);

        if (getRefreshTokenSP_request.status !== 200) throw {...getRefreshTokenSP_request, ...getRefreshTokenSP_response};

        Alpine.store('campaign').access.snapchat.token = getRefreshTokenSP_response.datas;

        console.log("acces_token => ", getRefreshTokenSP_response.datas);
      } catch(e) {
          console.log("Error getAccessTokenSnapchat: ", e);
      }
    },
    async getLanguageSnapchat() {
      try {
        var requestOptions = {
          method: 'GET',
          redirect: 'follow'
        };

        const getLanguagesSP_request = await fetch(`${Alpine.store('campaign').urlApi}/get_sp_languages?access_token=${Alpine.store('campaign').access.snapchat.token}`, requestOptions);
        const getLanguagesSP_response = await getLanguagesSP_request.json();

        console.log("getLanguagesSP_request: ", getLanguagesSP_request);
        console.log("getLanguagesSP_response: ", getLanguagesSP_response);

        if (getLanguagesSP_request.status !== 200) throw {...getLanguagesSP_request, ...getLanguagesSP_response};

        this.form.locale_options = getLanguagesSP_response.datas;

        console.log("Snapchat language options: ", this.form.locale_options);
      } catch(e) {
          console.log("Error getLanguageSnapchat: ", e);
      }
    },
    async getCountrySnapchat() {
      try {
        var requestOptions = {
          method: 'GET',
          redirect: 'follow'
        };

        const getCountriesSP_request = await fetch(`${Alpine.store('campaign').urlApi}/get_sp_countries?access_token=${Alpine.store('campaign').access.snapchat.token}`, requestOptions);
        const getCountriesSP_response = await getCountriesSP_request.json();

        console.log("getCountriesSP_request: ", getCountriesSP_request);
        console.log("getCountriesSP_response: ", getCountriesSP_response);

        if (getCountriesSP_request.status !== 200) throw {...getCountriesSP_request, ...getCountriesSP_response};

        this.form.country_options = getCountriesSP_response.datas;

        console.log("Snapchat countries options: ", this.form.country_options);
      } catch(e) {
          console.log("Error: getCountrySnapchat", e);
      }
    },
    async getAllLocationSnapchat(country_code) {
      try {
        if (!country_code) {
          this.form.snap_search_list_options = [];
          return true;
        }
        var requestOptions = {
          method: 'GET',
          redirect: 'follow'
        };

        const getLocationsSP_request = await fetch(`${Alpine.store('campaign').urlApi}/get_sp_locations_country?access_token=${Alpine.store('campaign').access.snapchat.token}&country_code=${country_code}`, requestOptions);
        const getLocationsSP_response = await getLocationsSP_request.json();

        console.log("getLocationsSP_request: ", getLocationsSP_request);
        console.log("getLocationsSP_response: ", getLocationsSP_response);

        if (getLocationsSP_request.status !== 200) throw {...getLocationsSP_request, ...getLocationsSP_response};

        this.form.snap_search_list_options = getLocationsSP_response.datas;

        console.log("Snapchat location options: ", this.form.snap_search_list_options);

        return true;
      } catch(e) {
          console.log("Error: getCountrySnapchat", e);
      }
    }
  });

  Alpine.store('modal_add_tiktok_group', {
    form: {
        name_input: "",
        price_input: "",
        date_input: "",
        places: [],
        place_options: [],
        locales: [],
        locale_options: [],
        identity: null,
        identity_options: [],
        switch_identity_create: false,
        //---
        name_ads: "",
        url_destination: "",
        tiktok_story: "",
        ad_text: "",
        ads: [],
    },
    open: false,
    errors: {
        name: "",
        price: "",
        date: "",
        places: "",
        ads: "",
        locales: "",
        identity: ""
    },
    modify: false,
    async init() {
      //Get and set in Alpine identity options
      /*await this.getIdentitiesTiktok();
      await this.getLocationsTiktok();
      await this.getLanguagesTiktok();*/

      //Enable date picker's modals
      flatpickr(".date_tiktok", {
          enableTime: false,
          dateFormat: "Y-m-d",
          minDate: new Date().fp_incr(1),
          onChange: function(dateObj, dateStr) {
              Alpine.store('modal_add_tiktok_group').form.date_input = dateStr
          }
      });

      //Identities autocomplete
      new Autocomplete('#autocomplete-tiktok-identities', {
        search: async input => {
          await this.getIdentitiesTiktok();
          return this.form.identity_options.filter(option => {
            return option.display_name.toLowerCase()
              .startsWith(input.toLowerCase())
          });
        },
        debounceTime: 500,
        renderResult: (result, props) => {
          return `
            <li ${props} style="list-style-type: none;cursor: pointer;">
              <div class="title">
                <img src="${result.profile_image}" style="height:30px; border-radius: 30px">
                ${result.display_name}
              </div>
            </li>
          `
        },
        getResultValue: result => "",
        onSubmit: async result => {
          this.addIdentity(result);
          console.log("Identity selected: ", result);
        }
      });

      //Places autocomplete
      new Autocomplete('#autocomplete-tiktok-places', {
        search: async input => {
          return this.form.place_options.filter(option => {
            return option.name.toLowerCase()
              .startsWith(input.toLowerCase())
          });
        },
        debounceTime: 500,
        renderResult: (result, props) => {
          return `
            <li ${props} style="list-style-type: none;cursor: pointer;">
              <div class="title">
                ${result.name} <span class="snippet">(${result.region_code})</span>
              </div>
            </li>
          `
        },
        getResultValue: result => "",
        onSubmit: async result => {
          console.log("Places selected: ", result);
          this.addPlace(result);
        }
      });

      //init autocomplete for locales
      new Autocomplete('#autocomplete-locales-tiktok', {
        search: input => {
          if(input.length === 0) {
            return ([{
              name: 'French',
              code: 'fr'
            }]);
          }
          if (input.length < 2) {
            return [];
          }

          return this.form.locale_options.filter(locale => {
            return locale.name.toLowerCase()
              .startsWith(input.toLowerCase())
          });
        },
        debounceTime: 500,
        renderResult: (result, props) => {
          return `
            <li ${props} style="list-style-type: none;cursor: pointer;">
              <div class="title">
                ${result.name}
              </div>
            </li>
          `
        },
        getResultValue: result => '',
        onSubmit: result => {
          console.log('clicked on:',  result);
          this.addLocales(result);
        }
      });
    },
    async openModalCreate() {
        Alpine.store('campaign').spinners.send_open_modal_tiktok_btn = true;

        this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.tiktok.length + 1}`;
        this.form.price_input = "20";
        this.form.date_input = "";
        this.form.places = [];
        this.form.locales = [{
            "name": "French",
            "code": "fr"
        }];
        this.form.locale_options = [];
        this.form.identity = "",
        this.form.identity_options = [],
        this.form.switch_identity_create = false,
        //---
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.tiktok_story = "";
        this.form.ad_text = "";
        this.form.ads = [];

        await this.getIdentitiesTiktok();
        await this.getLocationsTiktok();
        await this.getLanguagesTiktok();

        document.getElementById('tiktok_story').value = "";

        this.open = true;
        this.modify = false;

        Alpine.store('campaign').spinners.send_open_modal_tiktok_btn = false;
    },
    async openModalModify(key) {
        Alpine.store('campaign').spinners.send_open_modal_tiktok_btn = true;
        this.modify = key;
        const ad_group_tk_toModify = Alpine.store('campaign').groups.tiktok.filter(group => group.key === key)[0];

        console.log("ad_group_tk: ", ad_group_tk_toModify);

        this.form.name_input = ad_group_tk_toModify.name;
        this.form.price_input = ad_group_tk_toModify.price;
        this.form.date_input = ad_group_tk_toModify.date;
        this.form.places = ad_group_tk_toModify.places;
        this.form.locales = ad_group_tk_toModify.locales;
        this.form.locale_options = [];
        this.form.identity = ad_group_tk_toModify.identity;
        this.form.identity_options = [];
        this.form.switch_identity_create = ad_group_tk_toModify.switch_identity_create;
        //--
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.tiktok_story = "";
        this.form.ad_text = "";

        const ads_updated = await Promise.all(ad_group_tk_toModify.ads.map(async (ad) => {
          const infos = await this.getInfosCreativeTiktok(ad.video.video_id);

          ad.video.url = infos.preview_url;
          ad.video.poster_url = infos.video_cover_url;
          
          return ad;
        }));

        console.log("ads_updated: ", ads_updated);

        this.form.ads = ads_updated;

        await this.getIdentitiesTiktok();
        await this.getLocationsTiktok();
        await this.getLanguagesTiktok();

        document.getElementById('tiktok_story').value = "";

        this.open = true;
        Alpine.store('campaign').spinners.send_open_modal_tiktok_btn = false;
    },
    closeModal() {
      this.open = false;
    },
    checkErrorForm() {
        let errorForm = false;

        this.errors = {
            name: "",
            price: "",
            date: "",
            age: "",
            places: "",
            ads: "",
            locales: "",
            identity: "",
        };

        if (this.form.name_input === ""){
            this.errors.name = "Champs Nom vide";
            errorForm = true;
        }

        console.log("modify:", this.modify);
        if (this.modify !== false) {
            const ad_group_tk_key_noCheck = Alpine.store('campaign').groups.tiktok.filter(group => group.key === this.modify)[0].key;
            console.log("ad_group_tk_key_noCheck:", ad_group_tk_key_noCheck);
            if (Alpine.store('campaign').groups.tiktok.filter(group => (group.name === this.form.name_input && group.key !== ad_group_tk_key_noCheck))[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        } else {
            if (Alpine.store('campaign').groups.tiktok.filter(group => group.name === this.form.name_input)[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        }
        if (this.form.price_input === ""){
            this.errors.price = "Champs Prix vide";
            errorForm = true;
        }
        if (this.form.price_input < 20){
            this.errors.price = "Veuillez mettre un minimum de 30 Euros";
            errorForm = true;
        } 
        if (this.form.date_input === ""){
            this.errors.date = "Champs Date vide";
            errorForm = true;
        }
        if (this.form.places.length === 0) {
          this.errors.places = "Veuillez mettre au moins un endroit.";
          errorForm = true;
        }
        if (this.form.ads.length === 0) {
            this.errors.ads = "Veuillez mettre au moins une Pub.";
            errorForm = true;
        }
        if (this.form.locales.length === 0) {
            this.errors.locales = "Veuillez mettre au moins une langue.";
            errorForm = true;
        }
        if (this.form.identity === null || this.form.identity === "") {
            this.errors.identity = "Veuillez mettre au moins une identité.";
            errorForm = true;
        }

        console.log("pass into checkErrorForm (this.form):", this.form);
        console.log("this.form:", this.errors);

        return errorForm;
    },
    async addGroup() {
        console.log('add news Group tiktok ads');

        const formValid = this.checkErrorForm();
        if(!formValid) {
            const new_ad_group_tk = {
                key: `gp_tk_${Alpine.store('campaign').groups.tiktok.length}`,
                name: this.form.name_input.trim(),
                price: this.form.price_input,
                date: this.form.date_input,
                places: this.form.places,
                identity: this.form.identity,
                switch_identity_create: this.form.switch_identity_create,
                ads: this.form.ads,
                locales: this.form.locales,
                network: "tiktok"
            };

            Alpine.store('campaign').groups.tiktok.push(new_ad_group_tk);

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_tk was updated:', Alpine.store('campaign').groups.tiktok);
        }

        return true;
    },
    async modifyGroup(key) {
        console.log('modify Group tiktok ads');
        const formValid = this.checkErrorForm();
        console.log(`group key to modify: ${key}`);
        if(!formValid) {
            Alpine.store('campaign').groups.tiktok.forEach((group, index) => {
                if(group.key === this.modify){
                    Alpine.store('campaign').groups.tiktok[index].name = this.form.name_input.trim();
                    Alpine.store('campaign').groups.tiktok[index].price = this.form.price_input;
                    Alpine.store('campaign').groups.tiktok[index].date = this.form.date_input;
                    
                    Alpine.store('campaign').groups.tiktok[index].places = this.form.places;
                    Alpine.store('campaign').groups.tiktok[index].identity = this.form.identity;
                    Alpine.store('campaign').groups.tiktok[index].ads = this.form.ads;
                    Alpine.store('campaign').groups.tiktok[index].locales = this.form.locales;
                    Alpine.store('campaign').groups.tiktok[index].switch_identity_create = this.form.switch_identity_create;
                }
            });

            this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.tiktok.length + 1}`;
            this.form.price_input = "20";
            this.form.date_input = "";
            this.form.places = [];
            this.form.locales = [{
                "name": "French",
                "code": "fr"
            }];
            this.form.locale_options = [];
            this.form.identity = "",
            this.form.identity_options = [],
            this.form.switch_identity_create = false,
            //---
            this.form.name_ads = "";
            this.form.url_destination = "";
            this.form.tiktok_story = "";
            this.form.ad_text = "";
            this.form.ads = [];

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_tk was updated:', Alpine.store('campaign').groups.tiktok);
        }

        return true;
    },
    removeGroup(index) {
        console.log(`Remove group with index ${index - 1}`);
        console.log(Alpine.store('campaign').groups.tiktok);
        Alpine.store('campaign').groups.tiktok.splice(index - 1, 1);
        console.log(Alpine.store('campaign').groups.tiktok);
    },
    addIdentity(identity) {
        console.log('passing by addIdentity: ', identity);
        this.form.identity = identity;
        console.log('identities was updated: ', this.form.identity);
    },
    resetIdentity() {
        console.log('passing by resetIdentity');
        this.form.identity = null;
        console.log('identity was removed: ', this.form.identity);
    },
    addPlace(place) {
        console.log('passing by addPlace: ', place);
        console.log('CHECK DOUBLE: ', this.form.places.filter(placeAdded => placeAdded.id === place.id).length);
        if (this.form.places.filter(placeAdded => placeAdded.id === place.id).length > 0) {
          alert('vous avez dejà ajouté ce lieu a ce groupe de campagne !');
        } else {
          this.form.places.push(place);
          this.checkErrorForm();
          console.log('places was updated: ', this.form.places);
        }
    },
    resetPlaces() {
        this.form.places = [];
        document.getElementById('place-input-tiktok').value = "";
        console.log('places was removed: ', this.form.places);
    },
    removePlace(id) {
      let indexPlace = this.form.places.findIndex(place => id === place.id);
      console.log('indexPlace: ', indexPlace);
      this.form.places.splice(indexPlace, 1);
      console.log(`place with id ${id} was removed: `, this.form.places);
    },
    addLocales(locales) {
      console.log('passing by addLocales: ', locales);
      console.log('CHECK DOUBLE: ', this.form.locales.filter(localeAdded => localeAdded.code === locales.code).length);
      if (this.form.locales.filter(localeAdded => localeAdded.code === locales.code).length > 0) {
        alert('vous avez dejà ajouté ce language a ce groupe de campagne !');
      } else {
        this.form.locales.push(locales);
        this.checkErrorForm();
        console.log('locales was updated: ', this.form.locales);
      }
    },
    resetLocales() {
        this.form.locales = [];
        console.log('locales was removed: ', this.form.locales);
    },
    async checkFormat(ref_input, format, valueForm) {
      this.form[ref_input] = valueForm;

      let reader = new FileReader();
      let formatCalculed = format === "9x16" ? 9/16 : 1;
      var file = this.form[ref_input][0];
      var errorsFormat = [];

      const reset_upload = () => {
        console.log("reset_upload");
        this.form[ref_input] = "";
        document.getElementById(ref_input).value = "";
      }

      if (file.type.split('/')[0] === 'image') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 4999999) {
          errorsFormat.push(`Cette image est trop lourde ! (5 Mo max).`)
        }
        
        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              let img = new Image();
              img.src = e.target.result;

              img.onload = function () {
                var height = this.height;
                var width = this.width;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);

                //Check extension type
                if (!['image/png', 'image/jpeg', 'image/jpg'].includes(file.type)) {
                  errorsFormat.push(`Image have not a valid type extension => ${file.type}.`);
                }

                //Check ratio Image
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Image have not a valid ${format} format.`);
                  resolve(false);
                }

                //Check resolution Image
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1920).`);
                  }
                }

                resolve(true);
              }
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with image file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      } else if (file.type.split('/')[0] === 'video') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 9999999) {
          errorsFormat.push(`Video est trop lourd ! (10 Mo max) .`);
          document.getElementById(ref_input).value = "";
        }

        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              var dataUrl =  reader.result;
              var videoId = "videoMain";
              var $videoEl = $('<video id="' + videoId + '"></video>');
              $("body").append($videoEl);
              $videoEl.attr('src', dataUrl);

              var videoTagRef = $videoEl[0];

              videoTagRef.addEventListener('loadedmetadata', function(e) {
                e.target.dataset.width = 1080; 
                e.target.dataset.height = 1920;

                var height = videoTagRef.videoHeight;
                var width = videoTagRef.videoWidth;
                var duration = videoTagRef.duration;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);
                console.log("duration: ", duration);

                //Check extension type
                if (!['video/mp4'].includes(file.type)) {
                  errorsFormat.push(`Video have not a valid type extension => ${file.type}.`);
                }

                //Check ratio video
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Video have not a valid ratio ${format} format.`);
                }

                //Check resolution video
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1920).`);
                  }
                }

                //Check video duration
                if (duration > 20 || duration < 5) {
                  errorsFormat.push(`Video have not a valid duration (5s < video < 20).`);
                }

                resolve(true);
              })
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with video file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      }
    },
    async checkFormatAvatar(ref_input, valueForm) {
        const file_input = valueForm;

        let result;
        let reader = new FileReader();
        let formatCalculed = 1;
        var file = file_input[0];

        console.log("ref_input: ", ref_input);
        console.log("file: ", file);

        //check file size
        if (file.size > 1000000) {
            alert(`This image is to heavy ! (1 Mo max) .`);
            document.getElementById(ref_input).value = "";
        } else {
            result = await new Promise((resolve, reject) => {
                var reader2 = new FileReader();
                reader2.readAsDataURL(file);
                reader2.onload = function (e) {
                    resolve(new Promise((resolve, reject) => {
                        let img = new Image();
                        img.src = e.target.result;

                        img.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            if (width/height !== formatCalculed) {
                                alert(`This is not a valid 1x1 format .`);
                              resolve(false);
                            }
                        resolve(true);
                        }
                    }));
                };
            });
        }

        console.log(result);
        if (!result) {
            document.getElementById(ref_input).value = "";
        }
        return result;
    },
    removeAd(index) {
        console.log(this.form.ads, index);

        this.form.ads.splice(index, 1);

        console.log(this.form.ads); 
    },
    isValidUrl(_string) {
      const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
      return matchpattern.test(_string);
    },
    async calculateMD5(blob) {
      return new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsArrayBuffer(blob);
        reader.onloadend = function () {
          var wordArray = CryptoJS.lib.WordArray.create(reader.result),
            hash = CryptoJS.MD5(wordArray).toString().toUpperCase();
            resolve(hash)
        };
      })
    },
    async addAd(ad) {
      try {
        this.errors.ads = "";
        console.log("url: ", this.isValidUrl(this.form.url_destination));

        if (this.form.name_ads === "") {
            this.errors.ads = 'Champs Nom vide';
            return true;
        } else if (this.form.ads.filter(ad => ad.name_ads === this.form.name_ads)[0] !== undefined){
            this.errors.ads = "Champs Nom visuel identique à un autre groupe de pub";
            return true;
        } else if (this.form.url_destination === "") {
            this.errors.ads = 'Champs Url vide';
            return true;
        } else if (this.isValidUrl(this.form.url_destination) === false) {
            this.errors.ads = 'Champs Url Invalide';
            return true;
        } else if (this.form.ad_text === "") {
            this.errors.ads = 'Champs Texte vide';
            return true;
        } else if (this.form.ad_text.length < 12 || this.form.ad_text.length > 100) {
            this.errors.ads = 'Le texte doit contenir entre 12 et 100 caratères.';
            return true;
        } else if (this.form.tiktok_story.length === 0) {
            this.errors.ads = 'Champs video vide';
            return true;
        } else {
            Alpine.store('campaign').spinners.send_pic_tiktok = true;
            /* --- UPLOAD tiktok MEDIA --- */

            const md5File = await this.calculateMD5(this.form.tiktok_story[0]);
            console.log(md5File.toLowerCase());

            //2 Upload video to media tiktok
            var formdata = new FormData();
            formdata.append("access_token", `${Alpine.store('campaign').access.tiktok.token}`);
            formdata.append("advertiser_id", `${Alpine.store('campaign').access.tiktok.ad_account_id}`);
            formdata.append("video", this.form.tiktok_story[0], this.form.tiktok_story[0].name);
            formdata.append("video_name", this.form.name_ads);
            formdata.append("md5", md5File.toLowerCase());

            var requestOptions = {
              method: 'POST',
              body: formdata,
              redirect: 'follow'
            };

            const upload_video_request = await fetch(`${Alpine.store('campaign').urlApi}/create_tk_creative`, requestOptions);
            const uplaod_video_response = await upload_video_request.json();

            console.log("upload_video_request: ", upload_video_request);
            console.log("uplaod_video_response: ", uplaod_video_response);

            if (upload_video_request.status !== 200) throw {...upload_video_request, ...uplaod_video_response};
            //4 Update Alpine store
            this.form.ads.push({
              key: (this.form.ads.length - 1) + 1,
              name_ads: this.form.name_ads.trim(),
              url: this.form.url_destination.trim(),
              ad_text: this.form.ad_text.trim(),
              video: {
                type: "video",
                video_id: uplaod_video_response.datas[0].video_id,
                material_id: uplaod_video_response.datas[0].material_id,
                url: uplaod_video_response.datas[0].url,
                poster_url: uplaod_video_response.datas[0].poster_url
              },
            });
            Alpine.store('campaign').spinners.send_pic_tiktok = false;

            console.log('Ads was updated: ', this.form.ads);
        }
      } catch (e) {
        alert("Une erreur est survenue lors de l\'upload de media chez tiktok");
        console.log("error tiktok -> addAd: ", e);
        return false;
      }
    },
    async getInfosCreativeTiktok(video_id) {
      try {
          var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          const requestTiktokVideoInfos_request = await fetch(`${Alpine.store('campaign').urlApi}/get_tk_video_infos?video_id=${video_id}&advertiser_id=${Alpine.store('campaign').access.tiktok.ad_account_id}&access_token=${Alpine.store('campaign').access.tiktok.token}`, requestOptions);
          const requestTiktokVideoInfos_response = await requestTiktokVideoInfos_request.json();

          console.log("requestTiktokVideoInfos_request: ", requestTiktokVideoInfos_request);
          console.log("requestTiktokVideoInfos_response: ", requestTiktokVideoInfos_response);

          if (requestTiktokVideoInfos_request.status !== 200) throw {...requestTiktokVideoInfos_request, ...requestTiktokVideoInfos_response};

          console.log("video infos: ", requestTiktokVideoInfos_response.datas);

          return requestTiktokVideoInfos_response.datas;
      } catch(e) {
          console.log("error: ", e);
      }
    },
    async getLanguagesTiktok() {
      try {
          var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          const requestTiktokLanguages_request = await fetch(`${Alpine.store('campaign').urlApi}/get_tk_languages?advertiser_id=${Alpine.store('campaign').access.tiktok.ad_account_id}&access_token=${Alpine.store('campaign').access.tiktok.token}`, requestOptions);
          const requestTiktokLanguages_response = await requestTiktokLanguages_request.json();

          console.log("requestTiktokLocation_request: ", requestTiktokLanguages_request);
          console.log("requestTiktokLocation_response: ", requestTiktokLanguages_response);

          if (requestTiktokLanguages_request.status !== 200) throw {...requestTiktokLanguages_request, ...requestTiktokLanguages_response};

          this.form.locale_options = requestTiktokLanguages_response.datas;

          console.log("locale_options: ", this.form.locale_options);
      } catch(e) {
          console.log("error: ", e);
      }
    },
    async getLocationsTiktok() {
      try {
          var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          const requestTiktokLocation_request = await fetch(`${Alpine.store('campaign').urlApi}/get_tk_locations?advertiser_id=${Alpine.store('campaign').access.tiktok.ad_account_id}&access_token=${Alpine.store('campaign').access.tiktok.token}`, requestOptions);
          const requestTiktokLocation_response = await requestTiktokLocation_request.json();

          console.log("requestTiktokLocation_request: ", requestTiktokLocation_request);
          console.log("requestTiktokLocation_response: ", requestTiktokLocation_response);

          if (requestTiktokLocation_request.status !== 200) throw {...requestTiktokLocation_request, ...requestTiktokLocation_response};

          this.form.place_options = requestTiktokLocation_response.datas;

          console.log("place_options: ", this.form.place_options);
      } catch(e) {
          console.log("error: ", e);
      }
    },
    async getIdentitiesTiktok() {
      try {
          var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          const requestTiktokIdentities_request = await fetch(`${Alpine.store('campaign').urlApi}/get_tk_identities?advertiser_id=${Alpine.store('campaign').access.tiktok.ad_account_id}&access_token=${Alpine.store('campaign').access.tiktok.token}`, requestOptions);
          const requestTiktokIdentities_response = await requestTiktokIdentities_request.json();

          console.log("requestTiktokLocation_request: ", requestTiktokIdentities_request);
          console.log("requestTiktokLocation_response: ", requestTiktokIdentities_response);

          if (requestTiktokIdentities_request.status !== 200) throw {...requestTiktokIdentities_request, ...requestTiktokIdentities_response};

          this.form.identity_options = requestTiktokIdentities_response.datas;

          console.log("identity_options: ", this.form.identity_options);
      } catch(e) {
          console.log("error: ", e);
      }
    },
    async switchIdentityCreate(choice) {
      try {
        await this.getIdentitiesTiktok();
        this.resetIdentity();
        document.getElementById("tk_file_avatar_identity").value = "";

        this.form.switch_identity_create = choice;
             
        console.log(`switch_identity_create variable was updated with value: ${this.form.switch_identity_create}`);
      } catch (e) {
        console.log('Error in switchIdentityCreate(): ', e);
      }
    },
    async createIdentity() {
      try {
          Alpine.store('campaign').spinners.send_create_identity_tiktok = true;
          var formdata = new FormData();

          const md5File = await this.calculateMD5(document.getElementById('tk_file_avatar_identity').files[0]);
          console.log(md5File.toLowerCase());

          console.log("token tiktok: ", Alpine.store('campaign').access.tiktok.token);

          formdata.append("access_token", `${Alpine.store('campaign').access.tiktok.token}`);
          formdata.append("advertiser_id", `${Alpine.store('campaign').access.tiktok.ad_account_id}`);
          formdata.append("avatar", document.getElementById('tk_file_avatar_identity').files[0], document.getElementById('tk_file_avatar_identity').files[0].name);
          formdata.append("avatar_name", document.getElementById('tk_name_identity').value);
          formdata.append("md5", md5File.toLowerCase());

          var requestOptions = {
            method: 'POST',
            body: formdata,
            redirect: 'follow'
          };

          const createIdentityTiktok_request = await fetch(`${Alpine.store('campaign').urlApi}/create_tk_identity`, requestOptions);
          const createIdentityTiktok_response = await createIdentityTiktok_request.json();

          console.log("createIdentityTiktok_request: ", createIdentityTiktok_request);
          console.log("createIdentityTiktok_response: ", createIdentityTiktok_response);

          if (createIdentityTiktok_request.status !== 200) throw {...createIdentityTiktok_request, ...createIdentityTiktok_response};
          Alpine.store('campaign').spinners.send_create_identity_tiktok = false;
          await this.switchIdentityCreate(false);

          console.log("passing by createIdentity()");
      } catch (e) {
          console.log('Error in createIdentity(): ', e);
      }
    }
  });

  Alpine.store('modal_add_twitter_group', {
    form: {
        name_input: "",
        price_input: "",
        date_input: "",
        places: [],
        place_options: [],
        locales: [],
        locale_options: [],
        identity: null,
        identity_options: [],
        switch_identity_create: false,
        //---
        name_ads: "",
        url_destination: "",
        twitter_story: "",
        ad_text: "",
        ads: [],
    },
    open: false,
    errors: {
        name: "",
        price: "",
        date: "",
        places: "",
        ads: "",
        locales: "",
        identity: ""
    },
    modify: false,
    async init() {
      //Enable date picker's modals
      flatpickr(".date_twitter", {
          enableTime: false,
          dateFormat: "Y-m-d",
          minDate: new Date().fp_incr(1),
          onChange: function(dateObj, dateStr) {
              Alpine.store('modal_add_twitter_group').form.date_input = dateStr
          }
      });

      const placesTwUrl = `${Alpine.store('campaign').urlApi}/get_tw_locations`;
      const placesTwParams = `access_token=${Alpine.store('campaign').access.twitter.token}`;

      //Places autocomplete
      new Autocomplete('#autocomplete-twitter-places', {
        search: async input => {
          if(input.length === 0) {
            return ([{
              id: "f3bfc7dcc928977f",
              region_code: "FR",
              level: "COUNTRIES",
              name: "France"
            }]);
          }
          const url = `${placesTwUrl}?${placesTwParams}&search=${encodeURI(input)}`

          return new Promise(resolve => {
            if (input.length < 2) {
              return resolve([])
            }

            fetch(url)
              .then(response => response.json())
              .then(data => {
                console.log("get api locations twitter: ", data.datas);
                resolve(data.datas)
              })
              .catch(e => console.log("Error autocomplete twitter locations:", e));
          });
        },
        debounceTime: 500,
        renderResult: (result, props) => {
          return `
            <li ${props} style="list-style-type: none;cursor: pointer;">
              <div class="title">
                ${result.name} <span class="snippet">(${result.level} - ${result.region_code})</span>
              </div>
            </li>
          `
        },
        getResultValue: result => "",
        onSubmit: async result => {
          console.log("Places selected: ", result);
          this.addPlace(result);
        }
      });

      const languagesTwUrl = `${Alpine.store('campaign').urlApi}/get_tw_languages`;
      const languagesTwParams = `access_token=${Alpine.store('campaign').access.twitter.token}`;

      //init autocomplete for locales
      new Autocomplete('#autocomplete-locales-twitter', {
        search: input => {
          if(input.length === 0) {
            return ([{
              name: 'French',
              id: 'fr'
            }]);
          }
          const url = `${languagesTwUrl}?${languagesTwParams}&search=${encodeURI(input)}`

          return new Promise(resolve => {
            if (input.length < 2) {
              return resolve([])
            }

            fetch(url)
              .then(response => response.json())
              .then(data => {
                console.log("get api languages twitter: ", data.datas);
                resolve(data.datas)
              })
              .catch(e => console.log("Error autocomplete twitter:", e));
          })
        },
        debounceTime: 500,
        renderResult: (result, props) => {
          return `
            <li ${props} style="list-style-type: none;cursor: pointer;">
              <div class="title">
                ${result.name}
              </div>
            </li>
          `
        },
        getResultValue: result => '',
        onSubmit: result => {
          console.log('clicked on languages twitter:',  result);
          this.addLocales(result);
        }
      });
    },
    async openModalCreate() {
        Alpine.store('campaign').spinners.send_open_modal_twitter_btn = true;

        this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.twitter.length + 1}`;
        this.form.price_input = "20";
        this.form.date_input = "";
        this.form.places = [];
        this.form.locales = [{
            "name": "French",
            "id": "fr"
        }];
        this.form.locale_options = [];
        //---
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.twitter_story = "";
        this.form.ad_text = "";
        this.form.ads = [];

        document.getElementById('twitter_story').value = "";

        this.open = true;
        this.modify = false;

        Alpine.store('campaign').spinners.send_open_modal_twitter_btn = false;
    },
    async openModalModify(key) {
        Alpine.store('campaign').spinners.send_open_modal_twitter_btn = true;
        this.modify = key;
        const ad_group_tw_toModify = Alpine.store('campaign').groups.twitter.filter(group => group.key === key)[0];

        console.log("ad_group_tw: ", ad_group_tw_toModify);

        this.form.name_input = ad_group_tw_toModify.name;
        this.form.price_input = ad_group_tw_toModify.price;
        this.form.date_input = ad_group_tw_toModify.date;
        this.form.places = ad_group_tw_toModify.places;
        this.form.locales = ad_group_tw_toModify.locales;
        this.form.locale_options = [];
        //--
        this.form.name_ads = "";
        this.form.url_destination = "";
        this.form.twitter_story = "";
        this.form.ad_text = "";

        this.form.ads = ad_group_tw_toModify.ads;

        document.getElementById('twitter_story').value = "";

        this.open = true;
        Alpine.store('campaign').spinners.send_open_modal_twitter_btn = false;
    },
    closeModal() {
      this.open = false;
    },
    checkErrorForm() {
        let errorForm = false;

        this.errors = {
            name: "",
            price: "",
            date: "",
            age: "",
            places: "",
            ads: "",
            locales: "",
        };

        if (this.form.name_input === ""){
            this.errors.name = "Champs Nom vide";
            errorForm = true;
        }

        console.log("modify:", this.modify);
        if (this.modify !== false) {
            const ad_group_tw_key_noCheck = Alpine.store('campaign').groups.twitter.filter(group => group.key === this.modify)[0].key;
            console.log("ad_group_tw_key_noCheck:", ad_group_tw_key_noCheck);
            if (Alpine.store('campaign').groups.twitter.filter(group => (group.name === this.form.name_input && group.key !== ad_group_tw_key_noCheck))[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        } else {
            if (Alpine.store('campaign').groups.twitter.filter(group => group.name === this.form.name_input)[0] !== undefined){
                this.errors.name = "Champs Nom identique à un autre group de pub";
                errorForm = true;
            }
        }
        if (this.form.price_input === ""){
            this.errors.price = "Champs Prix vide";
            errorForm = true;
        }
        if (this.form.price_input < 20){
            this.errors.price = "Veuillez mettre un minimum de 30 Euros";
            errorForm = true;
        } 
        if (this.form.date_input === ""){
            this.errors.date = "Champs Date vide";
            errorForm = true;
        }
        if (this.form.places.length === 0) {
          this.errors.places = "Veuillez mettre au moins un endroit.";
          errorForm = true;
        }
        if (this.form.ads.length === 0) {
            this.errors.ads = "Veuillez mettre au moins une Pub.";
            errorForm = true;
        }
        if (this.form.locales.length === 0) {
            this.errors.locales = "Veuillez mettre au moins une langue.";
            errorForm = true;
        }

        console.log("pass into checkErrorForm (this.form):", this.form);
        console.log("this.form:", this.errors);

        return errorForm;
    },
    async addGroup() {
        console.log('add news Group twitter ads');

        const formValid = this.checkErrorForm();
        if(!formValid) {
            const new_ad_group_tw = {
                key: `gp_tw_${Alpine.store('campaign').groups.twitter.length}`,
                name: this.form.name_input.trim(),
                price: this.form.price_input,
                date: this.form.date_input,
                places: this.form.places,
                ads: this.form.ads,
                locales: this.form.locales,
                network: "twitter"
            };

            Alpine.store('campaign').groups.twitter.push(new_ad_group_tw);

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_tw was updated:', Alpine.store('campaign').groups.twitter);
        }

        return true;
    },
    async modifyGroup(key) {
        console.log('modify Group twitter ads');
        const formValid = this.checkErrorForm();
        console.log(`group key to modify: ${key}`);
        if(!formValid) {
            Alpine.store('campaign').groups.twitter.forEach((group, index) => {
                if(group.key === this.modify){
                    Alpine.store('campaign').groups.twitter[index].name = this.form.name_input.trim();
                    Alpine.store('campaign').groups.twitter[index].price = this.form.price_input;
                    Alpine.store('campaign').groups.twitter[index].date = this.form.date_input;
                    
                    Alpine.store('campaign').groups.twitter[index].places = this.form.places;
                    Alpine.store('campaign').groups.twitter[index].ads = this.form.ads;
                    Alpine.store('campaign').groups.twitter[index].locales = this.form.locales;
                }
            });

            this.form.name_input = `${Alpine.store('campaign').name}-${Alpine.store('campaign').groups.twitter.length + 1}`;
            this.form.price_input = "20";
            this.form.date_input = "";
            this.form.places = [];
            this.form.locales = [{
                "name": "French",
                "id": "fr"
            }];
            this.form.locale_options = [];
            //---
            this.form.name_ads = "";
            this.form.url_destination = "";
            this.form.twitter_story = "";
            this.form.ad_text = "";
            this.form.ads = [];

            document.getElementById("btn_send_data").classList.remove('green');
            await Alpine.store('campaign').sendAirtable();

            this.closeModal();
            console.log('ad_group_tw was updated:', Alpine.store('campaign').groups.twitter);
        }

        return true;
    },
    removeGroup(index) {
        console.log(`Remove group with index ${index - 1}`);
        console.log(Alpine.store('campaign').groups.twitter);
        Alpine.store('campaign').groups.twitter.splice(index - 1, 1);
        console.log(Alpine.store('campaign').groups.twitter);
    },
    addPlace(place) {
        console.log('passing by addPlace: ', place);
        console.log('CHECK DOUBLE: ', this.form.places.filter(placeAdded => placeAdded.id === place.id).length);
        if (this.form.places.filter(placeAdded => placeAdded.id === place.id).length > 0) {
          alert('vous avez dejà ajouté ce lieu a ce groupe de campagne !');
        } else {
          this.form.places.push(place);
          this.checkErrorForm();
          console.log('places was updated: ', this.form.places);
        }
    },
    resetPlaces() {
        this.form.places = [];
        document.getElementById('place-input-twitter').value = "";
        console.log('places was removed: ', this.form.places);
    },
    removePlace(id) {
      let indexPlace = this.form.places.findIndex(place => id === place.id);
      console.log('indexPlace: ', indexPlace);
      this.form.places.splice(indexPlace, 1);
      console.log(`place with id ${id} was removed: `, this.form.places);
    },
    addLocales(locales) {
      console.log('passing by addLocales: ', locales);
      console.log('CHECK DOUBLE: ', this.form.locales.filter(localeAdded => localeAdded.id === locales.id).length);
      if (this.form.locales.filter(localeAdded => localeAdded.id === locales.id).length > 0) {
        alert('vous avez dejà ajouté ce language a ce groupe de campagne !');
      } else {
        this.form.locales.push(locales);
        this.checkErrorForm();
        console.log('locales was updated: ', this.form.locales);
      }
    },
    resetLocales() {
        this.form.locales = [];
        console.log('languages was removed: ', this.form.locales);
    },
    async checkFormat(ref_input, format, valueForm) {
      this.form[ref_input] = valueForm;

      let reader = new FileReader();
      let formatCalculed = format === "9x16" ? 9/16 : 1;
      var file = this.form[ref_input][0];
      var errorsFormat = [];

      const reset_upload = () => {
        console.log("reset_upload");
        this.form[ref_input] = "";
        document.getElementById(ref_input).value = "";
      }

      if (file.type.split('/')[0] === 'image') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 4999999) {
          errorsFormat.push(`Cette image est trop lourde ! (5 Mo max).`)
        }
        
        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              let img = new Image();
              img.src = e.target.result;

              img.onload = function () {
                var height = this.height;
                var width = this.width;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);

                //Check extension type
                if (!['image/png', 'image/jpeg', 'image/jpg'].includes(file.type)) {
                  errorsFormat.push(`Image have not a valid type extension => ${file.type}.`);
                }

                //Check ratio Image
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Image have not a valid ${format} format.`);
                  resolve(false);
                }

                //Check resolution Image
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Image have not a valid resolution (1080 x 1920).`);
                  }
                }

                resolve(true);
              }
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with image file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      } else if (file.type.split('/')[0] === 'video') {
        if (!file) {
          alert('No File passed');
          return false;
        }

        if (file.size > 9999999) {
          errorsFormat.push(`Video est trop lourd ! (10 Mo max) .`);
          document.getElementById(ref_input).value = "";
        }

        result = await new Promise((resolve, reject) => {
          reader.onload = function (e) {
            resolve(new Promise((resolve, reject) => {
              var dataUrl =  reader.result;
              var videoId = "videoMain";
              var $videoEl = $('<video id="' + videoId + '"></video>');
              $("body").append($videoEl);
              $videoEl.attr('src', dataUrl);

              var videoTagRef = $videoEl[0];

              videoTagRef.addEventListener('loadedmetadata', function(e) {
                e.target.dataset.width = 1080; 
                e.target.dataset.height = 1920;

                var height = videoTagRef.videoHeight;
                var width = videoTagRef.videoWidth;
                var duration = videoTagRef.duration;

                console.log("type: ", file.type);
                console.log(`ratio: ${width/height} => ${width/height === 9/16 ? '9x16' : '1'}`);
                console.log(`resolution: ${width}x${height}`);
                console.log("duration: ", duration);

                //Check extension type
                if (!['video/mp4'].includes(file.type)) {
                  errorsFormat.push(`Video have not a valid type extension => ${file.type}.`);
                }

                //Check ratio video
                if (width/height !== formatCalculed) {
                  errorsFormat.push(`Video have not a valid ratio ${format} format.`);
                }

                //Check resolution video
                if (formatCalculed === 1) {
                  if (width !== 1080 || height !== 1080) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1080).`);
                  }
                } else {
                  if (width !== 1080 || height !== 1920) {
                    errorsFormat.push(`Video have not a valid resolution (1080 x 1920).`);
                  }
                }

                //Check video duration
                if (duration > 20 || duration < 5) {
                  errorsFormat.push(`Video have not a valid duration (5s < video < 20).`);
                }

                resolve(true);
              })
            }));
          };

          reader.readAsDataURL(file);
        });

        console.log('errorsFormat: ', errorsFormat);

        if (result && errorsFormat.length === 0) {
          return result;
        } else {
          alert(`Error with video file: \n- ${errorsFormat.join('\n- ')}`);
          reset_upload();
        }
      }
    },
    removeAd(index) {
        console.log(this.form.ads, index);

        this.form.ads.splice(index, 1);

        console.log(this.form.ads); 
    },
    isValidUrl(_string) {
      const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
      return matchpattern.test(_string);
    },
    async calculateMD5(blob) {
      return new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsArrayBuffer(blob);
        reader.onloadend = function () {
          var wordArray = CryptoJS.lib.WordArray.create(reader.result),
            hash = CryptoJS.MD5(wordArray).toString().toUpperCase();
            resolve(hash)
        };
      })
    },
    async addAd(ad) {
      try {
        this.errors.ads = "";
        console.log("url: ", this.isValidUrl(this.form.url_destination));

        if (this.form.name_ads === "") {
            this.errors.ads = 'Champs Nom vide';
            return true;
        } else if (this.form.ads.filter(ad => ad.name_ads === this.form.name_ads)[0] !== undefined){
            this.errors.ads = "Champs Nom visuel identique à un autre groupe de pub";
            return true;
        } else if (this.form.url_destination === "") {
            this.errors.ads = 'Champs Url vide';
            return true;
        } else if (this.isValidUrl(this.form.url_destination) === false) {
            this.errors.ads = 'Champs Url Invalide';
            return true;
        } else if (this.form.ad_text === "") {
            this.errors.ads = 'Champs Texte vide';
            return true;
        } else if (this.form.ad_text.length < 12 || this.form.ad_text.length > 100) {
            this.errors.ads = 'Le texte doit contenir entre 12 et 100 caratères.';
            return true;
        } else if (this.form.twitter_story.length === 0) {
            this.errors.ads = 'Champs video vide';
            return true;
        } else {
            Alpine.store('campaign').spinners.send_pic_twitter = true;
            /* --- UPLOAD twitter MEDIA --- */

            // Upload video to media twitter
            var formdata = new FormData();
            formdata.append("file", this.form.twitter_story[0], this.form.twitter_story[0].name);
            formdata.append("access_token", `${Alpine.store('campaign').access.twitter.token}`);
            formdata.append("ad_account_id", `${Alpine.store('campaign').access.twitter.ad_account_id}`);
            formdata.append("file_name", this.form.name_ads);
            formdata.append("file_type", this.form.twitter_story[0].type);

            var requestOptions = {
              method: 'POST',
              body: formdata,
              redirect: 'follow'
            };

            const upload_video_request = await fetch(`${Alpine.store('campaign').urlApi}/upload_tw_file`, requestOptions);
            const uplaod_video_response = await upload_video_request.json();

            console.log("upload_video_request: ", upload_video_request);
            console.log("uplaod_video_response: ", uplaod_video_response);

            if (upload_video_request.status !== 200) throw {...upload_video_request, ...uplaod_video_response};
           
            //4 Update Alpine store
            this.form.ads.push({
              key: (this.form.ads.length - 1) + 1,
              name_ads: this.form.name_ads.trim(),
              url: this.form.url_destination.trim(),
              ad_text: this.form.ad_text.trim(),
              file: {
                name: uplaod_video_response.datas.name,
                type: uplaod_video_response.datas.media_type.toLowerCase(),
                file_media_key: uplaod_video_response.datas.media_key,
                url: uplaod_video_response.datas.media_url,
                poster_url: uplaod_video_response.datas.poster_media_url ? uplaod_video_response.datas.poster_media_url : 'none',
              },
            });
            Alpine.store('campaign').spinners.send_pic_twitter = false;

            console.log('Ads was updated: ', this.form.ads);
        }
      } catch (e) {
        alert("Une erreur est survenue lors de l\'upload de media chez twitter");
        console.log("error twitter -> addAd: ", e);
        return false;
      }
    },
  });
});