# Multiposting

Multiposting is a nodejs library in order to create ads on multiple social media platform synchronically.

## Linter

Use eslint to clean your code

npm run lint

## Run localy the cloud function

It Use functions-framework to simulate google cloud function in your machine.

npm run start

## Deploy

Use gcloud command to deploy functions with the current git branch name

npm run deploy