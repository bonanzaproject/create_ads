// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_sp_creative = async (isTrafficCampaign, ad, ad_account_id, access_token) => {
  try {
    const data = JSON.stringify({
      creatives: [
        {
          ad_account_id: ad_account_id,
          brand_name: ad.brand_name,
          headline: ad.headline,
          top_snap_media_id: ad.snap_story.media_id,
          name: `${ad.name_ads}-auto-creative`,
          type: 'WEB_VIEW',
          shareable: true,
          call_to_action: 'APPLY_NOW',
          web_view_properties: {
            url: `${ad.url}${isTrafficCampaign ? '' : '?ad_id={{ad.id}}'}`,
          },
        },
      ],
    });

    const config = {
      method: 'post',
      url: `https://adsapi.snapchat.com/v1/adaccounts/${ad_account_id}/creatives`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`,
      },
      data: data,
    };

    let createSPCreative_request;
    try {
      createSPCreative_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (createSPCreative_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat creative request failed: ${config.url} => NOT created`,
          error: createSPCreative_request.data,
        },
      };
    }

    const result = createSPCreative_request.data.creatives[0].creative.id;
    console_log(`Snapchat creative ${createSPCreative_request.data.creatives[0].creative.name} with id ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_sp_creative module.');
    throw e;
  }
};

export default create_sp_creative;
