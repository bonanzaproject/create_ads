// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_sp_group = async (env, campaign_id, group, ad_account_id, access_token) => {
  try {
    const rawAdSquad = {
      adsquads: [
        {
          campaign_id,
          name: group.name,
          type: 'SNAP_ADS',
          status: 'ACTIVE',
          placement_v2: {
            config: 'AUTOMATIC',
          },
          optimization_goal: 'PIXEL_SIGNUP',
          auto_bid: true,
          target_bid: false,
          daily_budget_micro: group.price * 1000000,
          bid_strategy: 'AUTO_BID',
          billing_event: 'IMPRESSION',
          pixel_id: `${env ? 'a04ce87d-df10-4b5d-8f0c-bb165aeb26dc' : '2fca1ad0-22b0-47b8-810b-3084c2a1393a'}`,
          delivery_constraint: 'DAILY_BUDGET',
          pacing_type: 'STANDARD',
          child_ad_type: 'REMOTE_WEBPAGE',
          targeting: {
            regulated_content: false,
            demographics: [
              {
                min_age: group.age_min,
                max_age: group.age_max,
                languages: group.locales.map((lang) => lang.id),
              },
            ],
            enable_targeting_expansion: true,
            auto_expansion_options: {
              interest_expansion_option: {
                enabled: true,
              },
              custom_audience_expansion_option: {
                enabled: true,
              },
            },
          },
          start_time: `${new Date(group.date).toISOString()}`,
        },
      ],
    };

    // Add age_min // age_max+
    if (group.age_max === '50') {
      delete rawAdSquad.adsquads[0].targeting.demographics[0].max_age;
    }

    // Use circle map
    if (group.switchToMapLocation) {
      const geos = [{ country_code: group.country_selected.code }];
      const locations_circles = group.places_geos.map((place) => (
        {
          latitude: place.lat,
          longitude: place.lng,
          radius: place.radius,
          unit: 'KILOMETERS',
          name: `${place.lat}, ${place.lng}`,
        }
      ));

      const locations = {
        circles: locations_circles,
        operation: 'INCLUDE',
      };

      rawAdSquad.adsquads[0].targeting = {
        locations: [locations],
        geos,
        ...rawAdSquad.adsquads[0].targeting,
      };
    } else {
      // Use list location
      let geos;
      if (group.places.includes(null)) {
        throw {
          expose: true,
          error_detail: {
            message: 'There is an issue with SNAPCHAT place, check this out !',
            error: 'ERROR: Snapchat form create-ads',
          },
        };
      }
      if (group.places.length === 0) {
        geos = [{
          country_code: group.country_selected.code,
          // operation: 'INCLUDE',
        }];
      } else {
        geos = group.places.map((place) => {
          let place_formatted = false;

          if (place.type === 'region') {
            place_formatted = {
              country_code: group.country_selected.code,
              region_id: [
                place.id,
              ],
              operation: 'INCLUDE',
            };
          } else if (place.type === 'metro') {
            place_formatted = {
              country_code: group.country_selected.code,
              metro_id: [
                place.id,
              ],
              operation: 'INCLUDE',
            };
          } else {
            place_formatted = {
              country_code: group.country_selected.code,
              operation: 'INCLUDE',
            };
          }

          return place_formatted;
        });
      }

      rawAdSquad.adsquads[0].targeting = {
        geos,
        ...rawAdSquad.adsquads[0].targeting,
      };
    }

    const data = JSON.stringify(rawAdSquad);

    const config = {
      method: 'post',
      url: `https://adsapi.snapchat.com/v1/campaigns/${campaign_id}/adsquads`,
      headers: {
        Authorization: `Bearer ${access_token}`,
        'Content-Type': 'application/json',
      },
      data,
    };

    let createSPGroup_request;
    try {
      createSPGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (createSPGroup_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat group: ${config.url} => NOT created`,
          error: createSPGroup_request.data,
        },
      };
    }

    const result = createSPGroup_request.data.adsquads[0].adsquad.id;
    console_log(`Snapchat group ${group.name} with id ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_sp_group module.');
    throw e;
  }
};

export default create_sp_group;
