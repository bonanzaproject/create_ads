// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const delete_sp_campaign = async (campaign_id, access_token) => {
  try {
    const config = {
      method: 'delete',
      url: `https://adsapi.snapchat.com/v1/campaigns/${campaign_id}`,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let deleteSPCampaign_request;
    try {
      deleteSPCampaign_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (deleteSPCampaign_request.data.request_status === 'ERROR') {
      throw {
        expose: false,
        error_detail: {
          message: `Snapchat delete campaign: ${config.url} => NOT deleted`,
          error: deleteSPCampaign_request.data,
        },
      };
    }

    const result = deleteSPCampaign_request.data.request_status;
    console_log(`Snapchat campaign with id: ${campaign_id} => deleted.`);

    return result;
  } catch (e) {
    console_log('Error in delete_sp_campaign module.');
    throw e;
  }
};

export default delete_sp_campaign;
