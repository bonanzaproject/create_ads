// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_sp_ad = async (env, group_id, creative_id, ad, ad_account_id, access_token) => {
  try {
    const data = JSON.stringify({
      ads: [
        {
          ad_squad_id: group_id,
          creative_id: creative_id,
          name: ad.name_ads,
          type: 'REMOTE_WEBPAGE',
          status: 'ACTIVE',
        },
      ],
    });

    const config = {
      method: 'post',
      url: `https://adsapi.snapchat.com/v1/adsquads/${group_id}/ads`,
      headers: {
        Authorization: `Bearer ${access_token}`,
        'Content-Type': 'application/json',
      },
      data,
    };

    let createSPAd_request;
    try {
      createSPAd_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (createSPAd_request.data.request_status === 'ERROR') {
      throw {
        expose: true,
        error_detail: {
          message: `Snapchat ad: ${config.url} => NOT CREATED`,
          error: createSPAd_request.data,
        },
      };
    }

    const result = createSPAd_request.data.ads[0].ad.id;
    console_log(`Snapchat ad ${ad.name_ads} with id ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_sp_ad module.');
    throw e;
  }
};

export default create_sp_ad;
