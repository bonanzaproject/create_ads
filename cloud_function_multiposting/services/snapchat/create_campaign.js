// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_sp_campaign = async (env, typeCampaign, name, groups, ad_account_id, access_token) => {
  try {
    if (groups.length > 0) {
      const objective_campaign = typeCampaign === 'conversion' ? 'WEB_CONVERSION' : 'WEB_VIEW';

      const data = JSON.stringify({
        campaigns: [
          {
            name,
            ad_account_id,
            status: `${env ? 'PAUSED' : 'ACTIVE'}`,
            start_time: `${new Date().toISOString()}`,
            objective: objective_campaign,
          },
        ],
      });

      const config = {
        method: 'post',
        url: `https://adsapi.snapchat.com/v1/adaccounts/${ad_account_id}/campaigns`,
        headers: {
          Authorization: `Bearer ${access_token}`,
          'Content-Type': 'application/json',
        },
        data,
      };

      let createSPCampaign_request;
      try {
        createSPCampaign_request = await axios(config);
      } catch (e) {
        throw {
          expose: false,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data,
          },
        };
      }

      if (createSPCampaign_request.data.request_status === 'ERROR') {
        throw {
          expose: true,
          error_detail: {
            message: `Snapchat campaign: ${config.url} => NOT created`,
            error: createSPCampaign_request.data,
          },
        };
      }

      const result = createSPCampaign_request.data.campaigns[0].campaign.id;
      console_log(`Snapchat campaign ${name} with id ${result} => created`);

      return result;
    }
    return null;
  } catch (e) {
    console_log('Error in create_sp_campaign module.');
    throw e;
  }
};

export default create_sp_campaign;
