// LIBRARY
import { WebClient } from '@slack/web-api';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// CONST
import access from '../../const/access.js';

const push_message = async (message, access_token) => {
  try {
    const slack = new WebClient(access_token);

    let text = message;
    if (message.length >= 2900) {
      text = `${message.substr(0, 2900)}\u2026`;
    }

    text = `\`\`\`${text}\`\`\``;

    try {
      await slack.chat.postMessage({
        text: 'Yolo !',
        channel: access.slack.channel,
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text,
            },
          },
        ],
      });
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: 'Slack notification => NOT send',
          error: e.response,
        },
      };
    }
    console_log('Slack notification => send');

    return true;
  } catch (e) {
    console_log('Error in push_message module.');
    throw e;
  }
};

export default push_message;
