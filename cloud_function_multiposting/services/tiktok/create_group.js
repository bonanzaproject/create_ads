// LIBRARY
import axios from 'axios';
import moment from 'moment';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_tk_group = async (
  env,
  isTrafficCampaign,
  campaign_id,
  group,
  ad_account_id,
  access_token,
) => {
  try {
    const dataConv = {
      advertiser_id: ad_account_id,
      adgroup_name: group.name,
      campaign_id,
      placement_type: 'PLACEMENT_TYPE_NORMAL',
      placements: [
        'PLACEMENT_TIKTOK',
      ],
      gender: 'GENDER_UNLIMITED',
      age_groups: [
        'AGE_18_24',
        'AGE_25_34',
        'AGE_35_44',
        'AGE_45_54',
      ],
      video_download_disabled: true,
      promotion_type: 'WEBSITE',
      is_comment_disable: 0,
      location_ids: group.places.map((place) => place.id),
      schedule_type: 'SCHEDULE_FROM_NOW',
      // schedule_start_time: new Date(group.date).toISOString(),
      schedule_start_time: moment(group.date).locale('fr').format('YYYY-MM-DD HH:MM:SS'),
      dayparting: '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',
      optimization_goal: 'CONVERT',
      optimization_event: 'FORM',
      billing_event: 'OCPM',
      bid_price: 0.0,
      conversion_bid_price: 0.0,
      budget_mode: 'BUDGET_MODE_DAY',
      budget: parseFloat(group.price),
      bid_type: 'BID_TYPE_NO_BID',
      pacing: 'PACING_MODE_SMOOTH',
      languages: group.locales.map((locale) => locale.code),
      pixel_id: `${env ? '7070178949053054977' : '6959116902736150530'}`,
      auto_targeting_enabled: `${env ? false : true}`,
    };

    const dataTraffic = {
      advertiser_id: ad_account_id,
      adgroup_name: group.name,
      campaign_id,
      placement_type: 'PLACEMENT_TYPE_NORMAL',
      placements: [
        'PLACEMENT_TIKTOK',
      ],
      gender: 'GENDER_UNLIMITED',
      age_groups: [
        'AGE_18_24',
        'AGE_25_34',
        'AGE_35_44',
        'AGE_45_54',
      ],
      video_download_disabled: true,
      promotion_type: 'WEBSITE',
      is_comment_disable: 0,
      location_ids: group.places.map((place) => place.id),
      schedule_type: 'SCHEDULE_FROM_NOW',
      // schedule_start_time: new Date(group.date).toISOString(),
      schedule_start_time: moment(group.date).locale('fr').format('YYYY-MM-DD HH:MM:SS'),
      dayparting: '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',
      optimization_goal: 'CLICK',
      billing_event: 'CPC',
      bid_price: 0.0,
      conversion_bid_price: 0.0,
      budget_mode: 'BUDGET_MODE_DAY',
      budget: parseFloat(group.price),
      bid_type: 'BID_TYPE_NO_BID',
      pacing: 'PACING_MODE_SMOOTH',
      languages: group.locales.map((locale) => locale.code),
      auto_targeting_enabled: `${env ? false : true}`,
    };

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/adgroup/create/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/json',
      },
      data: isTrafficCampaign ? JSON.stringify(dataTraffic) : JSON.stringify(dataConv),
    };

    let createTKGroup_request;
    try {
      createTKGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    if (createTKGroup_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Tiktok request failed: ${config.url}`,
          error: createTKGroup_request.data,
        },
      };
    }

    const result = createTKGroup_request.data.data.adgroup_id;
    console_log(`Tiktok group ${group.name} with id: ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_tk_group module.');
    throw e;
  }
};

export default create_tk_group;
