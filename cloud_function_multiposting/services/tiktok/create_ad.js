// LIBRARY
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

// SERVICES
import get_tk_video_infos from './get_video_infos.js';
import upload_tk_image_url from './upload_image_url.js';
import update_tk_ad from './update_ad.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_tk_ad = async (
  env,
  isTrafficCampaign,
  group_id,
  client,
  identity,
  ad,
  ad_account_id,
  access_token,
) => {
  try {
    const ad_id = uuidv4();

    const video_info = await get_tk_video_infos(ad.video.video_id, ad_account_id, access_token);
    const imageUpload = await upload_tk_image_url(
      video_info.video_cover_url,
      ad_account_id,
      access_token,
    );

    // 1 - Create ad
    const url_avatar = new URL(identity.profile_image);
    const landing_page_url = isTrafficCampaign ? ad.url : `${ad.url}?ad_id=${ad_id}`;

    const data = {
      advertiser_id: ad_account_id,
      adgroup_id: group_id,
      creatives: [
        {
          display_name: client,
          call_to_action: 'APPLY_NOW',
          ad_text: ad.ad_text,
          ad_format: 'SINGLE_VIDEO',
          video_id: ad.video.video_id,
          image_ids: [imageUpload.image_id],
          avatar_icon_web_uri: `/${url_avatar.pathname.split('/')[url_avatar.pathname.split('/').length - 2]}/${url_avatar.pathname.split('/')[url_avatar.pathname.split('/').length - 1]}`,
          landing_page_url: landing_page_url,
          deeplink_type: 'NORMAL',
          ad_name: `${ad.name_ads}-${ad_id}`,
          identity_id: identity.identity_id,
          identity_type: 'CUSTOMIZED_USER',
          fallback_type: 'WEBSITE',
          tracking_pixel_id: `${env ? '7070178949053054977' : '6959116902736150530'}`,
        },
      ],
    };

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/ad/create/',
      headers: {
        'Access-Token': '7ab95d9c78554d19e6443f26c69f391a89670a23',
        'Content-Type': 'application/json',
      },
      data: JSON.stringify(data),
    };

    let createTKAd_request;
    try {
      createTKAd_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (createTKAd_request.data.code !== 0) {
      throw {
        expose: true,
        error_detail: {
          message: `Tiktok request failed: ${config.url}`,
          error: createTKAd_request.data,
        },
      };
    }

    const resultAdCreated = createTKAd_request.data.data;
    console_log(`Tiktok ad ${ad.name_ads} with id ${resultAdCreated} => created`);

    // - Update ad with ad_id in parameter if conversion campaign
    if (!isTrafficCampaign) {
      data.creatives[0].landing_page_url = `${ad.url}?ad_id=${resultAdCreated.ad_ids[0]}`;
      data.creatives[0].ad_id = resultAdCreated.ad_ids[0];
      delete data.creatives[0].vast_moat;

      return await update_tk_ad(data, access_token);
    }

    return resultAdCreated;
  } catch (e) {
    console_log('Error in create_tk_ad module.');
    throw e;
  }
};

export default create_tk_ad;
