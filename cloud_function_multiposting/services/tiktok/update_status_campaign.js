// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const update_tk_status_campaign = async (
  status_desired,
  campaign_id,
  ad_account_id,
  access_token,
) => {
  try {
    const data = JSON.stringify({
      advertiser_id: ad_account_id,
      campaign_ids: [
        campaign_id,
      ],
      operation_status: status_desired,
    });

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/campaign/status/update/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/json',
      },
      data,
    };

    let updateTKStatusCampaign_request;
    try {
      updateTKStatusCampaign_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (updateTKStatusCampaign_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `Tiktok request failed: ${config.url}`,
          error: updateTKStatusCampaign_request.data,
        },
      };
    }

    const result = updateTKStatusCampaign_request.data.data;
    console_log(`Tiktok campaign with id ${campaign_id} => status updated ${result.status}`);

    return result;
  } catch (e) {
    console_log('Error in update_tk_status_campaign module.');
    throw e;
  }
};

export default update_tk_status_campaign;
