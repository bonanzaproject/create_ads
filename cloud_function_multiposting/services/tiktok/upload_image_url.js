// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const upload_tk_image_url = async (image_url, ad_account_id, access_token) => {
  try {
    const data = new FormData();

    data.append('advertiser_id', ad_account_id);
    data.append('upload_type', 'UPLOAD_BY_URL');
    data.append('image_url', image_url);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/file/image/ad/upload/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'multipart/form-data',
        ...data.getHeaders(),
      },
      data,
    };

    let uploadImageTiktok_request;
    try {
      uploadImageTiktok_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    if (uploadImageTiktok_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `File from url: ${image_url} => NOT send to Tiktok: ${config.url}`,
          error: uploadImageTiktok_request.data,
        },
      };
    }

    const result = uploadImageTiktok_request.data.data;
    console_log(`File from url: ${result.image_url} with id: ${result.image_id} => send to Tiktok.`);

    return result;
  } catch (e) {
    console_log('Error in upload_image_url module.');
    throw e;
  }
};

export default upload_tk_image_url;
