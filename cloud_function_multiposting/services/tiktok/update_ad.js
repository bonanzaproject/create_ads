// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const update_tk_ad = async (data_ad, access_token) => {
  try {
    const data = JSON.stringify(data_ad);

    const config = {
      method: 'post',
      url: 'https://business-api.tiktok.com/open_api/v1.3/ad/update/',
      headers: {
        'Access-Token': access_token,
        'Content-Type': 'application/json',
      },
      data,
    };

    let updateTKAd_request;
    try {
      updateTKAd_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data,
        },
      };
    }

    if (updateTKAd_request.data.code !== 0) {
      throw {
        expose: false,
        error_detail: {
          message: `Tiktok ad update request failed: ${config.url} => NOT updated`,
          error: updateTKAd_request.data,
        },
      };
    }

    const result = updateTKAd_request.data.data;
    console_log(`Tiktok ad with id ${result.ad_ids[0]} => updated`);

    return result;
  } catch (e) {
    console_log('Error in update_tk_ad module.');
    throw e;
  }
};

export default update_tk_ad;
