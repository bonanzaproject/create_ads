// SERVICE
import create_tk_group from './create_group.js';
import create_tk_ad from './create_ad.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_tk_batch_groups = async (
  env,
  isTrafficCampaign,
  client,
  campaign_id,
  groups,
  ad_account_id,
  access_token,
) => {
  try {
    // 0 - If have group(s) facebook to create
    if (groups.length > 0) {
      // 0.1 - For each group setted

      const groups_created = [];
      for (let i = 0; i < groups.length; i++) {
        try {
          // 1 - Create group
          const group_created = await create_tk_group(
            env,
            isTrafficCampaign,
            campaign_id,
            groups[i],
            ad_account_id,
            access_token,
          );

          // 1.1 - For each Ad setted
          const ads_created = [];
          for (let j = 0; j < groups[i].ads.length; j++) {
            try {
              // 2 - Create Ad
              const ad_created = await create_tk_ad(
                env,
                isTrafficCampaign,
                group_created,
                client,
                groups[i].identity,
                groups[i].ads[j],
                ad_account_id,
                access_token,
              );

              ads_created.push({
                ad: ad_created.ad_ids,
                creative: ad_created.creatives.map((creative) => creative.ad_name),
              });
            } catch (e) {
              throw (e);
            }
          }

          groups_created.push({
            group: group_created,
            ads: ads_created,
          });
        } catch (e) {
          throw e;
        }
      }
      return groups_created;
    }
    return [];
  } catch (e) {
    console_log('Error in create_tk_batch_groups module.');
    throw e;
  }
};

export default create_tk_batch_groups;
