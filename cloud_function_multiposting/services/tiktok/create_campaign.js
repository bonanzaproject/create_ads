// LIBRARY
import axios from 'axios';

// SERVICES
import update_tk_status_campaign from './update_status_campaign.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_tk_campaign = async (env, typeCampaign, name, group, ad_account_id, access_token) => {
  try {
    if (group.length > 0) {
      const objective_campaign = typeCampaign === 'conversion' ? 'CONVERSIONS' : 'TRAFFIC';

      const data = JSON.stringify({
        advertiser_id: ad_account_id,
        budget_mode: 'BUDGET_MODE_INFINITE',
        objective_type: objective_campaign,
        objective: 'LANDING_PAGE',
        campaign_name: name,
      });

      const config = {
        method: 'post',
        url: 'https://business-api.tiktok.com/open_api/v1.3/campaign/create/',
        headers: {
          'Access-Token': access_token,
          'Content-Type': 'application/json',
        },
        data,
      };

      let createTKCampaign_request;
      try {
        createTKCampaign_request = await axios(config);
      } catch (e) {
        throw {
          expose: false,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data,
          },
        };
      }

      if (createTKCampaign_request.data.code !== 0) {
        throw {
          expose: true,
          error_detail: {
            message: `Tiktok request failed: ${config.url}`,
            error: createTKCampaign_request.data,
          },
        };
      }

      const result = createTKCampaign_request.data.data.campaign_id;
      console_log(`Tiktok campaign ${name} with id ${result} => created`);

      // if env is true, then it's test or dev campaign
      if (env) {
        await update_tk_status_campaign('DISABLE', result, ad_account_id, access_token);
      } else {
        await update_tk_status_campaign('ENABLE', result, ad_account_id, access_token);
      }

      return result;
    }
    return null;
  } catch (e) {
    console_log('Error in create_tk_campaign module.');
    throw e;
  }
};

export default create_tk_campaign;
