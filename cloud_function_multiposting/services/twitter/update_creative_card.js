// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const update_tw_creative_card = async (
  ad,
  card_id,
  promoted_tweet_id,
  ad_account_id,
  access_token,
) => {
  try {
    const datas = {
      components: [
        {
          type: 'MEDIA',
          media_key: ad.file.file_media_key,
        },
        {
          type: 'DETAILS',
          title: ad.name_ads,
          destination: {
            type: 'WEBSITE',
            url: `${ad.url}?ad_id=${promoted_tweet_id}`,
          },
        },
      ],
    };

    const header = await set_oauth_header(
      access_token,
      'PUT',
      `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/cards/${card_id}`,
      {},
    );

    const config = {
      method: 'put',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/cards/${card_id}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: header,
      },
      data: JSON.stringify(datas),
    };

    let createTWCard_request;
    try {
      createTWCard_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = createTWCard_request.data.data;

    console_log(`Twitter card type ${result.card_type} with id: ${result.id} and uri: ${result.card_uri} => udpated`);

    return result;
  } catch (e) {
    console_log('Error in update_tw_creative_card module.');
    throw e;
  }
};

export default update_tw_creative_card;
