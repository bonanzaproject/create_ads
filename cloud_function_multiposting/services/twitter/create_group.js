// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const create_tw_group = async (
  env,
  isTrafficCampaign,
  campaign_id,
  group,
  ad_account_id,
  access_token,
) => {
  try {
    const goal_campaign = isTrafficCampaign ? 'LINK_CLICKS' : 'WEBSITE_CONVERSIONS';
    const web_event_tag = env ? 'qgrvy' : 'ptf0k';

    const header = await set_oauth_header(
      access_token,
      'POST',
      `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/line_items`,
      {},
    );

    const data = new FormData();

    data.append('campaign_id', campaign_id);
    data.append('objective', 'WEBSITE_CLICKS');
    data.append('goal', goal_campaign);
    data.append('placements', 'ALL_ON_TWITTER'); // 'ALL_ON_TWITTER,PUBLISHER_NETWORK'
    data.append('product_type', 'PROMOTED_TWEETS');
    data.append('name', group.name);
    data.append('start_time', `${group.date}T06:00:00-01:00Z`);
    data.append('bid_strategy', 'AUTO');
    data.append('pay_by', 'LINK_CLICK');
    data.append('currency', 'EUR');
    /* - Delete Twitter Audience Platform (PUBLISHER_NETWORK)
    data.append('advertiser_domain',`${new URL(group.ads[0].url).hostname.replace('www.', '')}`);
    data.append('categories', 'IAB13-7');
    */
    data.append('daily_budget_amount_local_micro', (group.price * 1000000).toString());
    if (!isTrafficCampaign) {
      data.append('primary_web_event_tag', web_event_tag);
    }

    const config = {
      method: 'post',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/line_items`,
      headers: {
        Authorization: header,
        ...data.getHeaders(),
      },
      data: data,
    };

    let createTWGroup_request;
    try {
      createTWGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = createTWGroup_request.data.data.id;
    console_log(`Twitter group ${group.name} with id: ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_tw_group module.');
    throw e;
  }
};

export default create_tw_group;
