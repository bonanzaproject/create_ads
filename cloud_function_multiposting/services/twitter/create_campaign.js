// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const create_tw_campaign = async (env, typeCampaign, name, group, ad_account_id, access_token) => {
  try {
    if (group.length > 0) {
      const funding_instrument_id = env ? '13x4sn' : 'nl1w1';
      const entity_status = env ? 'PAUSED' : 'ACTIVE';

      const data = new FormData();

      data.append('funding_instrument_id', funding_instrument_id);
      data.append('name', name);
      data.append('entity_status', entity_status);
      data.append('budget_optimization', 'LINE_ITEM');

      const header = await set_oauth_header(
        access_token,
        'POST',
        `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/campaigns`,
        {},
      );

      const config = {
        method: 'post',
        url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/campaigns`,
        headers: {
          Authorization: header,
          ...data.getHeaders(),
        },
        data: data,
      };

      let createTWCampaign_request;
      try {
        createTWCampaign_request = await axios(config);
      } catch (e) {
        throw {
          expose: false,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data,
          },
        };
      }

      if (createTWCampaign_request.status !== 200) {
        throw {
          expose: true,
          error_detail: {
            message: `Twitter request failed: ${config.url}`,
            error: createTWCampaign_request.errors,
          },
        };
      }

      const result = createTWCampaign_request.data.data.id;
      console_log(`Twitter campaign ${name} with id ${result} => created`);

      return result;
    }
    return null;
  } catch (e) {
    console_log('Error in create_tw_campaign module.');
    throw e;
  }
};

export default create_tw_campaign;
