// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const create_tw_tweet = async (
  env,
  ad,
  creative,
  ad_account_id,
  access_token,
) => {
  try {
    const user_id = env ? '1524730251900764160' : '768103036329787392';

    const header = await set_oauth_header(
      access_token,
      'POST',
      `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/tweet`,
      {},
    );

    const data = new FormData();

    data.append('as_user_id', user_id);
    data.append('name', `auto-tweet ${ad.name_ads}`);
    data.append('card_uri', creative.card_uri);
    data.append('nullcast', 'true');
    data.append('text', ad.ad_text);

    const config = {
      method: 'post',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/tweet`,
      headers: {
        Authorization: header,
        ...data.getHeaders(),
      },
      data: data,
    };

    let createTWGroup_request;
    try {
      createTWGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = createTWGroup_request.data.data;
    console_log(`Twitter tweet ${result.name} with id: ${result.id} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_tw_tweet module.');
    throw e;
  }
};

export default create_tw_tweet;
