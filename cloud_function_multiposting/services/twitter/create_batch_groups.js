// SERVICE
import create_tw_group from './create_group.js';
import create_tw_creative_card from './create_creative_card.js';
import create_tw_tweet from './create_tweet.js';
import create_tw_ad from './create_ad.js';
import update_tw_creative_card from './update_creative_card.js';
import link_tw_targets_group from './link_targets_group.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_tw_batch_groups = async (
  env,
  isTrafficCampaign,
  campaign_id,
  groups,
  ad_account_id,
  access_token,
) => {
  try {
    // 0 - If have group(s) twitter to create
    if (groups.length > 0) {
      // 0.1 - For each group setted
      const groups_created = await Promise.all(groups.map(async (group) => {
        try {
          // 1 - Create group
          const group_created = await create_tw_group(
            env,
            isTrafficCampaign,
            campaign_id,
            group,
            ad_account_id,
            access_token,
          );

          // 2 - Link targets criteria to group (line_item)
          await link_tw_targets_group(
            group_created,
            group.places,
            group.locales,
            ad_account_id,
            access_token,
          );

          // 2.1 - For each Ad setted
          const ads_created = await Promise.all(group.ads.map(async (ad) => {
            try {
              // 3 - create card spec
              const creative_card_created = await create_tw_creative_card(
                ad,
                ad_account_id,
                access_token,
              );

              // 4 - Create simple tweet with card
              const tweet_created = await create_tw_tweet(
                env,
                ad,
                creative_card_created,
                ad_account_id,
                access_token,
              );

              // 5 - Create promoted tweet
              const ad_tweet_promoted_created = await create_tw_ad(
                group_created,
                tweet_created.id_str,
                ad_account_id,
                access_token,
              );

              // 6 - Update card with ad_promoted_tweet_id in url if it's converison campaign
              if (!isTrafficCampaign) {
                await update_tw_creative_card(
                  ad,
                  creative_card_created.id,
                  ad_tweet_promoted_created.id,
                  ad_account_id,
                  access_token,
                );
              }

              return {
                creative: creative_card_created.id,
                tweet: tweet_created.id_str,
                ad_promoted_tweet: ad_tweet_promoted_created.id,
              };
            } catch (e) {
              throw (e);
            }
          })).catch((e) => {
            throw e;
          });

          return {
            group: group_created,
            ads: ads_created,
          };
        } catch (e) {
          throw e;
        }
      })).catch((e) => {
        throw e;
      });

      return groups_created;
    }
    return [];
  } catch (e) {
    console_log('Error in create_tw_batch_groups module.');
    throw e;
  }
};

export default create_tw_batch_groups;
