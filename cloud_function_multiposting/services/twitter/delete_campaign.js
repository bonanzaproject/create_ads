// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const delete_tw_campaign = async (
  campaign_id,
  ad_account_id,
  access_token,
) => {
  try {
    const header = await set_oauth_header(
      access_token,
      'DELETE',
      `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/campaigns/${campaign_id}`,
      {},
    );

    const config = {
      method: 'delete',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/campaigns/${campaign_id}`,
      headers: {
        Authorization: header,
      },
    };

    let deleteTWCampaign_request;
    try {
      deleteTWCampaign_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = deleteTWCampaign_request.data.data;
    console_log(`Twitter campaign with id: ${campaign_id} => deleted`);

    return result;
  } catch (e) {
    console_log('Error in delete_tw_campaign module.');
    throw e;
  }
};

export default delete_tw_campaign;
