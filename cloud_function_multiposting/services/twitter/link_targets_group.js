// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const link_tw_targets_group = async (
  group_id,
  places,
  languages,
  ad_account_id,
  access_token,
) => {
  try {
    const datas = [];

    places.forEach((place) => {
      datas.push({
        operation_type: 'Create',
        params: {
          line_item_id: group_id,
          targeting_type: 'LOCATION',
          targeting_value: place.id,
        },
      });
    });

    languages.forEach((language) => {
      datas.push({
        operation_type: 'Create',
        params: {
          line_item_id: group_id,
          targeting_type: 'LANGUAGE',
          targeting_value: language.id,
        },
      });
    });

    datas.push({
      operation_type: 'Create',
      params: {
        line_item_id: group_id,
        targeting_type: 'AGE',
        targeting_value: 'AGE_OVER_18',
      },
    });

    console_log('targets criteria to add:', datas);

    const header = await set_oauth_header(
      access_token,
      'POST',
      `https://ads-api.twitter.com/${access.twitter.version_api}/batch/accounts/${ad_account_id}/targeting_criteria`,
      {},
    );

    const config = {
      method: 'post',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/batch/accounts/${ad_account_id}/targeting_criteria`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: header,
      },
      data: JSON.stringify(datas),
    };

    let linkTWTargetGroup_request;
    try {
      linkTWTargetGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = linkTWTargetGroup_request.data.data.id;
    console_log(`Twitter targets criteria => linked to group ${group_id}`);

    return result;
  } catch (e) {
    console_log('Error in link_tw_targets_group module.');
    throw e;
  }
};

export default link_tw_targets_group;
