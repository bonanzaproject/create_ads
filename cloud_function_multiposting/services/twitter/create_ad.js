// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';
import set_oauth_header from './set_oauth_header.js';

// CONST
import access from '../../const/access.js';

const create_tw_ad = async (
  group_id,
  tweet_id,
  ad_account_id,
  access_token,
) => {
  try {
    const header = await set_oauth_header(
      access_token,
      'POST',
      `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/promoted_tweets`,
      {},
    );

    const data = new FormData();

    data.append('line_item_id', group_id);
    data.append('tweet_ids', tweet_id);

    const config = {
      method: 'post',
      url: `https://ads-api.twitter.com/${access.twitter.version_api}/accounts/${ad_account_id}/promoted_tweets`,
      headers: {
        Authorization: header,
        ...data.getHeaders(),
      },
      data: data,
    };

    let createTWGroup_request;
    try {
      createTWGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.errors,
        },
      };
    }

    const result = createTWGroup_request.data.data[0];
    console_log(`Twitter ad promoted tweet ${result.name} with id: ${result.id} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_tw_ad module.');
    throw e;
  }
};

export default create_tw_ad;
