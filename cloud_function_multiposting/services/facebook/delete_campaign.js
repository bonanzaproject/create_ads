// LIBRARY
import axios from 'axios';
import qs from 'qs';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const delete_fb_campaign = async (campaign_id, access_token) => {
  try {
    const data = qs.stringify({
      access_token,
    });

    const config = {
      method: 'delete',
      url: `https://graph.facebook.com/v15.0/${campaign_id}/`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data,
    };

    let deleteFBCampaign_request;
    try {
      deleteFBCampaign_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    const result = deleteFBCampaign_request.data.success;
    console_log(`Facebook campaign with id: ${campaign_id} => deleted`);

    return result;
  } catch (e) {
    console_log('Error in delete_fb_campaign module.');
    throw e;
  }
};

export default delete_fb_campaign;
