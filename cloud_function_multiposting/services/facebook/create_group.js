// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_fb_group = async (
  env,
  isTrafficCampaign,
  campaign_id,
  group,
  ad_account_id,
  access_token,
) => {
  try {
    const data = new FormData();

    data.append('campaign_id', campaign_id);
    data.append('name', group.name);
    data.append('age_min', group.age_min);
    data.append('age_max', group.age_max);
    data.append('optimization_goal', `${isTrafficCampaign ? 'LINK_CLICKS' : 'OFFSITE_CONVERSIONS'}`);
    data.append('bid_strategy', 'LOWEST_COST_WITHOUT_CAP');
    data.append('billing_event', 'IMPRESSIONS');
    data.append('daily_budget', `${group.price * 100}`);

    const geo = {
      location_types: [
        'home',
        'recent',
      ],
    };
    const country_groups = [];
    const countries = [];
    const regions = [];
    const cities = [];
    group.places.forEach((place) => {
      if (place.type === 'country_group') {
        country_groups.push(place.key);
      } else if (place.type === 'country') {
        countries.push(place.key);
      } else if (place.type === 'region') {
        regions.push({ key: place.key });
      } else if (place.type === 'city') {
        if (place.radius !== '') {
          cities.push({ key: place.key, radius: place.radius, distance_unit: 'kilometer' });
        } else {
          cities.push({ key: place.key });
        }
      } else {
        throw ('Error: location type unknow');
      }
    });
    if (country_groups.length > 0) {
      geo.country_groups = country_groups;
    }
    if (countries.length > 0) {
      geo.countries = countries;
    }
    if (regions.length > 0) {
      geo.regions = regions;
    }
    if (cities.length > 0) {
      geo.cities = cities;
    }

    const targeting_obj = {
      age_min: group.age_min,
      age_max: group.age_max,
      geo_locations: geo,
      publisher_platforms: ['facebook', 'instagram', 'messenger'],
      facebook_positions: ['feed', 'facebook_reels', 'story'],
      instagram_positions: ['story', 'stream', 'reels', 'explore'],
      messenger_positions: ['story'],
      locales: group.locales.map((locale) => locale.key),
    };

    let promoted_object = {
      pixel_id: '853859998351756',
      pixel_rule: '{"and":[{"event":{"eq":"submitForm"}}]}',
      custom_event_type: 'OTHER',
      custom_conversion_id: '339071704364766',
    };

    if (env) {
      promoted_object = {
        pixel_id: '2721773944794796',
        custom_event_type: 'COMPLETE_REGISTRATION',
      };
    }

    data.append('targeting', JSON.stringify(targeting_obj));
    data.append('start_time', `${group.date} 06:00:00-01:00`);
    data.append('status', 'ACTIVE');
    if (!isTrafficCampaign) {
      data.append('promoted_object', JSON.stringify(promoted_object));
    }
    data.append('access_token', access_token);

    const config = {
      method: 'post',
      url: `https://graph.facebook.com/v15.0/act_${ad_account_id}/adsets`,
      headers: {
        ...data.getHeaders(),
      },
      data,
    };

    let createFBGroup_request;
    try {
      createFBGroup_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    const result = createFBGroup_request.data.id;
    console_log(`Facebook group ${group.name} with id: ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_fb_group module.');
    throw e;
  }
};

export default create_fb_group;
