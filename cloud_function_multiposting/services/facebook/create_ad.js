// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_fb_ad = async (
  env,
  isTrafficCampaign,
  group_id,
  creative_id,
  ad,
  ad_account_id,
  access_token,
) => {
  try {
    const data = new FormData();

    data.append('name', ad.name);
    data.append('adset_id', group_id);

    const obj_creative = {
      creative_id,
    };

    data.append('creative', JSON.stringify(obj_creative));
    data.append('status', 'ACTIVE');
    data.append('access_token', access_token);
    if (!env) {
      data.append('conversion_domain', 'bonanza.co');
    }

    if (isTrafficCampaign) {
      const obj_tracking_specs = {
        'action.type': [
          'offsite_conversion',
        ],
        fb_pixel: [
          `${env ? '2721773944794796' : '853859998351756'}`,
        ],
      };

      data.append('tracking_specs', JSON.stringify(obj_tracking_specs));
    }

    const config = {
      method: 'post',
      url: `https://graph.facebook.com/v15.0/act_${ad_account_id}/ads`,
      headers: {
        ...data.getHeaders(),
      },
      data,
    };

    let createFBAd_request;
    try {
      createFBAd_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    const result = createFBAd_request.data.id;
    console_log(`Facebook ad ${ad.name} with id ${result} => created`);

    return result;
  } catch (e) {
    console_log('Error in create_fb_ad module.');
    throw e;
  }
};

export default create_fb_ad;
