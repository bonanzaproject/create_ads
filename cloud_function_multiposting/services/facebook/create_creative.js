// LIBRARY
import axios from 'axios';
import FormData from 'form-data';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_fb_creative = async (isTrafficCampaign, ad, ad_account_id, access_token) => {
  try {
    const data = new FormData();

    data.append('name', ad.name);
    data.append('access_token', access_token);
    data.append('status', 'ACTIVE');

    // - Determine type of media is for feed and story.
    const ad_type_feed = ad.feed_fb.type;
    const ad_type_story = ad.stories_inst.type;

    // - Champs: images / videos
    const images_obj = [];
    if (ad_type_feed === 'image') {
      images_obj.push({
        adlabels: [{ name: 'labelfeed' }],
        hash: `${ad.feed_fb.hash}`,
      });
    }
    if (ad_type_story === 'image') {
      images_obj.push({
        adlabels: [{ name: 'labelstory' }],
        hash: `${ad.stories_inst.hash}`,
      });
    }

    const videos_obj = [];
    if (ad_type_feed === 'video') {
      videos_obj.push({
        adlabels: [{ name: 'labelfeed' }],
        video_id: `${ad.feed_fb.hash}`,
      });
    }
    if (ad_type_story === 'video') {
      videos_obj.push({
        adlabels: [{ name: 'labelstory' }],
        video_id: `${ad.stories_inst.hash}`,
      });
    }

    // - Champs: ad_formats
    const ad_formats_obj = [];
    if (ad_type_feed === 'video' && ad_type_story === 'video') {
      ad_formats_obj.push('SINGLE_VIDEO');
    }
    if (ad_type_feed === 'image' && ad_type_story === 'image') {
      ad_formats_obj.push('SINGLE_IMAGE');
    }
    if ((ad_type_feed === 'image' && ad_type_story === 'video')
      || (ad_type_feed === 'video' && ad_type_story === 'image')) {
      ad_formats_obj.push('AUTOMATIC_FORMAT');
    }

    // - Champs: asset_customization_rules
    const asset_customization_rules_obj = [];
    if (ad_type_feed === 'video') {
      asset_customization_rules_obj.push({
        customization_spec: {
          publisher_platforms: ['facebook', 'instagram'],
          facebook_positions: ['feed', 'video_feeds'],
          instagram_positions: ['stream', 'explore'],
        },
        video_label: { name: 'labelfeed' },
      });
    }
    if (ad_type_feed === 'image') {
      asset_customization_rules_obj.push({
        customization_spec: {
          publisher_platforms: ['facebook', 'instagram'],
          facebook_positions: ['feed'],
          instagram_positions: ['stream', 'explore'],
        },
        image_label: { name: 'labelfeed' },
      });
    }
    if (ad_type_story === 'video') {
      asset_customization_rules_obj.push({
        customization_spec: {
          publisher_platforms: ['facebook', 'instagram', 'messenger'],
          facebook_positions: ['facebook_reels', 'story'],
          instagram_positions: ['story', 'reels'],
          messenger_positions: ['story'],
        },
        video_label: { name: 'labelstory' },
      });
    }
    if (ad_type_story === 'image') {
      asset_customization_rules_obj.push({
        customization_spec: {
          publisher_platforms: ['facebook', 'instagram', 'messenger'],
          facebook_positions: ['facebook_reels', 'story'],
          instagram_positions: ['story', 'reels'],
          messenger_positions: ['story'],
        },
        image_label: { name: 'labelstory' },
      });
    }

    // - Champs obj_st_spec
    const obj_st_spec = {
      instagram_actor_id: 1374855462530049,
      page_id: 1040721499284274,
    };

    const url_web_page = isTrafficCampaign ? ad.url : `${ad.url}?ad_id={{ad.id}}`;

    const asset_feed_spec = {
      images: images_obj,
      videos: videos_obj,
      bodies: [{ text: `${ad.main_text}` }],
      link_urls: [{ website_url: url_web_page }],
      titles: [{ text: `${ad.title_ads}` }],
      ad_formats: ad_formats_obj,
      call_to_action_types: ['APPLY_NOW'],
      descriptions: [{ text: `${ad.description}` }],
      asset_customization_rules: asset_customization_rules_obj,
    };

    data.append('object_story_spec', JSON.stringify(obj_st_spec));
    data.append('asset_feed_spec', JSON.stringify(asset_feed_spec));

    const config = {
      method: 'post',
      url: `https://graph.facebook.com/v15.0/act_${ad_account_id}/adcreatives`,
      headers: {
        ...data.getHeaders(),
      },
      data,
    };

    let createFBAdCreative_request;
    try {
      createFBAdCreative_request = await axios(config);
    } catch (e) {
      throw {
        expose: true,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    const result = createFBAdCreative_request.data.id;
    console_log(`Facebook creative ${ad.name} with id: ${result} => created.`);

    return result;
  } catch (e) {
    console_log('Error in create_fb_creative module.');
    throw e;
  }
};

export default create_fb_creative;
