// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_fb_campaign = async (
  env,
  typeCampaign,
  name,
  country_selected,
  group,
  ad_account_id,
  access_token,
) => {
  try {
    if (group.length > 0) {
      // Check country special category facebook
      const country_accepted = ['AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AN', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', 'BA', 'BB', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BL', 'BM', 'BN', 'BO', 'BQ', 'BR', 'BS', 'BT', 'BV', 'BW', 'BY', 'BZ', 'CA', 'CC', 'CD', 'CF', 'CG', 'CH', 'CI', 'CK', 'CL', 'CM', 'CN', 'CO', 'CR', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DE', 'DJ', 'DK', 'DM', 'DO', 'DZ', 'EC', 'EE', 'EG', 'EH', 'ER', 'ES', 'ET', 'FI', 'FJ', 'FK', 'FM', 'FO', 'FR', 'GA', 'GB', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GL', 'GM', 'GN', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GW', 'GY', 'HK', 'HM', 'HN', 'HR', 'HT', 'HU', 'ID', 'IE', 'IL', 'IM', 'IN', 'IO', 'IQ', 'IR', 'IS', 'IT', 'JE', 'JM', 'JO', 'JP', 'KE', 'KG', 'KH', 'KI', 'KM', 'KN', 'KP', 'KR', 'KW', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LI', 'LK', 'LR', 'LS', 'LT', 'LU', 'LV', 'LY', 'MA', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NC', 'NE', 'NF', 'NG', 'NI', 'NL', 'NO', 'NP', 'NR', 'NU', 'NZ', 'OM', 'PA', 'PE', 'PF', 'PG', 'PH', 'PK', 'PL', 'PM', 'PN', 'PR', 'PS', 'PT', 'PW', 'PY', 'QA', 'RE', 'RO', 'RS', 'RU', 'RW', 'SA', 'SB', 'SC', 'SD', 'SE', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SR', 'SS', 'ST', 'SV', 'SX', 'SY', 'SZ', 'TC', 'TD', 'TF', 'TG', 'TH', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TR', 'TT', 'TV', 'TW', 'TZ', 'UA', 'UG', 'UM', 'US', 'UY', 'UZ', 'VA', 'VC', 'VE', 'VG', 'VI', 'VN', 'VU', 'WF', 'WS', 'XK', 'YE', 'YT', 'ZA', 'ZM', 'ZW'];
      if (!country_accepted.includes(country_selected)) {
        throw { expose: true, error_detail: `Vous ne pouvez pas déployer de pub dans ce pays là (${country_selected}) sur Facebook / Instagram!!` };
      }

      const objective_campaign = typeCampaign === 'conversion' ? 'OUTCOME_LEADS' : 'OUTCOME_TRAFFIC';

      const config = {
        method: 'post',
        url: `https://graph.facebook.com/v15.0/act_${ad_account_id}/campaigns?name=${encodeURIComponent(name)}&objective=${objective_campaign}&status=${env ? 'PAUSED' : 'ACTIVE'}&special_ad_categories=EMPLOYMENT&special_ad_category_country=${encodeURIComponent(country_selected)}&access_token=${encodeURIComponent(access_token)}`,
        headers: {},
      };

      let createFBCampaign_request;
      try {
        createFBCampaign_request = await axios(config);
      } catch (e) {
        throw {
          expose: true,
          error_detail: {
            message: `Axios request failed: ${config.url}`,
            error: e.response.data.error,
          },
        };
      }

      const result = createFBCampaign_request.data.id;
      console_log(`Facebook campaign ${name} with id ${result} => created`);

      return result;
    }
    return null;
  } catch (e) {
    console_log('Error in create_fb_campaign module.');
    throw e;
  }
};

export default create_fb_campaign;
