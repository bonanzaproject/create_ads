// SERVICE
import create_fb_group from './create_group.js';
import create_fb_creative from './create_creative.js';
import create_fb_ad from './create_ad.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const create_fb_batch_groups = async (
  env,
  isTrafficCampaign,
  campaign_id,
  groups,
  ad_account_id,
  access_token,
) => {
  try {
    // 0 - If have group(s) facebook to create
    if (groups.length > 0) {
      // 0.1 - For each group setted
      const groups_created = await Promise.all(groups.map(async (group) => {
        try {
          // 1 - Create group
          const group_created = await create_fb_group(
            env,
            isTrafficCampaign,
            campaign_id,
            group,
            ad_account_id,
            access_token,
          );

          // 1.1 - For each Ad setted
          const ads_created = await Promise.all(group.ads.map(async (ad) => {
            try {
              // 2 - Create Creation
              const creative_created = await create_fb_creative(
                isTrafficCampaign,
                ad,
                ad_account_id,
                access_token,
              );

              // 3 - Create Ad
              const ad_created = await create_fb_ad(
                env,
                isTrafficCampaign,
                group_created,
                creative_created,
                ad,
                ad_account_id,
                access_token,
              );

              return {
                creative: creative_created,
                ad: ad_created,
              };
            } catch (e) {
              throw (e);
            }
          })).catch((e) => {
            throw e;
          });

          return {
            group: group_created,
            ads: ads_created,
          };
        } catch (e) {
          throw e;
        }
      })).catch((e) => {
        throw e;
      });

      return groups_created;
    }
    return [];
  } catch (e) {
    console_log('Error in create_fb_batch_groups module.');
    throw e;
  }
};

export default create_fb_batch_groups;
