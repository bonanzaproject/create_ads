// LIBRARY
import axios from 'axios';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const get_record = async (table_id, onglet, record_id, access_token) => {
  try {
    const config = {
      method: 'get',
      url: `https://api.airtable.com/v0/${table_id}/${onglet}/${record_id}`,
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    };

    let getRecordsAirtable_request;
    try {
      getRecordsAirtable_request = await axios(config);
    } catch (e) {
      throw {
        expose: false,
        error_detail: {
          message: `Axios request failed: ${config.url}`,
          error: e.response.data.error,
        },
      };
    }

    const result = getRecordsAirtable_request.data.fields;
    console_log(`Airtable record with id: ${record_id} getted.`);

    return result;
  } catch (e) {
    console_log('Error in get_record module.');
    throw e;
  }
};

export default get_record;
