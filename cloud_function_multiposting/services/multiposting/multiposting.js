// SERVICE
import get_record from '../airtable/get_record.js';
import sp_refresh_token from '../snapchat/refresh_token.js';
import create_fb_campaign from '../facebook/create_campaign.js';
import create_fb_batch_groups from '../facebook/create_batch_groups.js';
import create_sp_campaign from '../snapchat/create_campaign.js';
import create_sp_batch_groups from '../snapchat/create_batch_groups.js';
import create_tk_campaign from '../tiktok/create_campaign.js';
import create_tk_batch_groups from '../tiktok/create_batch_groups.js';
import create_tw_campaign from '../twitter/create_campaign.js';
import create_tw_batch_groups from '../twitter/create_batch_groups.js';

import retroaction from './retroaction.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

// CONST
import access from '../../const/access.js';

const multiposting = async (at_table_id, at_record_id, at_access_token) => {
  try {
    // 0 - Get json from Airtable
    const at_record = await get_record(at_table_id, 'Suivi%20Campagne', at_record_id, at_access_token);

    // 0.1 - Use case if camapign is not prepare on airtable.
    if (!at_record.json) return 'Prepare your campaign before asking to multipost !';
    const campaign = JSON.parse(at_record.json);

    // 0.2 - determine the environment
    if (campaign.isDev !== undefined) {
      console_log(`Environment is set, isDEV: ${campaign.isDev}`);
    } else {
      throw { expose: false, error_detail: 'dev environment is not defined' };
    }
    const DEV = campaign.isDev;

    // 0.3 - Set correct account id environment
    let fb_ad_account_id;
    let sp_ad_account_id;
    let tk_ad_account_id;
    let tw_ad_account_id;

    if (DEV) {
      fb_ad_account_id = access.facebook.ad_account_id_dev;
      sp_ad_account_id = access.snapchat.ad_account_id_dev;
      tk_ad_account_id = access.tiktok.ad_account_id_dev;
      tw_ad_account_id = access.twitter.ad_account_id_dev;
    } else {
      fb_ad_account_id = access.facebook.ad_account_id_prod;
      sp_ad_account_id = access.snapchat.ad_account_id_prod;
      tk_ad_account_id = access.tiktok.ad_account_id_prod;
      tw_ad_account_id = access.twitter.ad_account_id_prod;
    }

    // 0.4 - Authentification
    const fb_access_token = access.facebook.token;
    const sp_access_token = await sp_refresh_token(
      access.snapchat.client_id,
      access.snapchat.client_secret,
      access.snapchat.refresh_token,
    );
    const tk_access_token = access.tiktok.token;
    const tw_access_token = access.twitter.token;

    // 0.5 - Print variable setted to futur debug
    console_log('fb_ad_account_id: ', fb_ad_account_id);
    console_log('sp_ad_account_id: ', sp_ad_account_id);
    console_log('fb_ad_account_id: ', tk_ad_account_id);
    console_log('fb_access_token: ', fb_access_token);
    console_log('sp_access_token: ', sp_access_token);
    console_log('tk_access_token: ', tk_access_token);
    console_log('tw_access_token: ', tw_access_token);

    // 0.6 - Retroaction
    // Variable globale pour la retroaction en cas d'erreur.
    const campaigns_created = {
      fb_campaign_created: null,
      sp_campaign_created: null,
      tk_campaign_created: null,
      tw_campaign_created: null,
    };

    const groups_created = {
      fb_groups_created: [],
      sp_groups_created: [],
      tk_groups_created: [],
      tw_groups_created: [],
    };

    try {
      // 1 - Campaign creation
      // 1.1 - Facebook
      const fb_campaign_created = await create_fb_campaign(
        DEV,
        campaign.isTrafficCampaign ? 'traffic' : 'conversion',
        campaign.name,
        campaign.country_code_selected,
        campaign.groups.facebook,
        fb_ad_account_id,
        fb_access_token,
      );
      campaigns_created.fb_campaign_created = fb_campaign_created;
      // 1.2 - Snapchat
      const sp_campaign_created = await create_sp_campaign(
        DEV,
        campaign.isTrafficCampaign ? 'traffic' : 'conversion',
        campaign.name,
        campaign.groups.snapchat,
        sp_ad_account_id,
        sp_access_token,
      );
      campaigns_created.sp_campaign_created = sp_campaign_created;
      // 1.3 - Tiktok
      const tk_campaign_created = await create_tk_campaign(
        DEV,
        campaign.isTrafficCampaign ? 'traffic' : 'conversion',
        campaign.name,
        campaign.groups.tiktok,
        tk_ad_account_id,
        tk_access_token,
      );
      campaigns_created.tk_campaign_created = tk_campaign_created;
      // 1.4 - Twitter
      const tw_campaign_created = await create_tw_campaign(
        DEV,
        campaign.isTrafficCampaign ? 'traffic' : 'conversion',
        campaign.name,
        campaign.groups.twitter,
        tw_ad_account_id,
        tw_access_token,
      );
      campaigns_created.tw_campaign_created = tw_campaign_created;

      // 2 - Batch Ad Groups creation
      // 2.1 - Facebook
      const fb_groups_created = await create_fb_batch_groups(
        DEV,
        campaign.isTrafficCampaign,
        fb_campaign_created,
        campaign.groups.facebook,
        fb_ad_account_id,
        fb_access_token,
      );
      groups_created.fb_groups_created = fb_groups_created;
      // 2.2 - Snapchat
      const sp_groups_created = await create_sp_batch_groups(
        DEV,
        campaign.isTrafficCampaign,
        sp_campaign_created,
        campaign.groups.snapchat,
        sp_ad_account_id,
        sp_access_token,
      );
      groups_created.sp_groups_created = sp_groups_created;
      // 2.3 - Tiktok
      const tk_groups_created = await create_tk_batch_groups(
        DEV,
        campaign.isTrafficCampaign,
        campaign.client,
        tk_campaign_created,
        campaign.groups.tiktok,
        tk_ad_account_id,
        tk_access_token,
      );
      groups_created.tk_groups_created = tk_groups_created;
      // 2.4 - Twitter
      const tw_groups_created = await create_tw_batch_groups(
        DEV,
        campaign.isTrafficCampaign,
        tw_campaign_created,
        campaign.groups.twitter,
        tw_ad_account_id,
        tw_access_token,
      );
      groups_created.tw_groups_created = tw_groups_created;
    } catch (e) {
      try {
        await retroaction(
          campaigns_created,
          tk_ad_account_id,
          tw_ad_account_id,
          fb_access_token,
          sp_access_token,
          tk_access_token,
          tw_access_token,
        );
        throw e;
      } catch (err) {
        throw err;
      }
    }

    const recap = {
      isDev: DEV,
      facebook: {
        fb_campaign_created: campaigns_created.fb_campaign_created,
        groups: groups_created.fb_groups_created,
      },
      snapchat: {
        sp_campaign_created: campaigns_created.sp_campaign_created,
        groups: groups_created.sp_groups_created,
      },
      tiktok: {
        tk_campaign_created: campaigns_created.tk_campaign_created,
        groups: groups_created.tk_groups_created,
      },
      twitter: {
        tw_campaign_created: campaigns_created.tw_campaign_created,
        groups: groups_created.tw_groups_created,
      },
    };

    return recap;
  } catch (e) {
    console_log('Error in multiposting module.');
    throw e;
  }
};

export default multiposting;
