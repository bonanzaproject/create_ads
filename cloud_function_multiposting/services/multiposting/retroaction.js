// SERVICE
import delete_fb_campaign from '../facebook/delete_campaign.js';
import delete_sp_campaign from '../snapchat/delete_campaign.js';
import update_tk_status_campaign from '../tiktok/update_status_campaign.js';
import delete_tw_campaign from '../twitter/delete_campaign.js';

// RESSOURCES
import console_log from '../../ressources/console_log.js';

const retroaction = async (
  campaigns_created,
  tk_ad_account_id,
  tw_ad_account_id,
  fb_access_token,
  sp_access_token,
  tk_access_token,
  tw_access_token,
) => {
  try {
    console_log('Retroaction activated...');

    if (campaigns_created.fb_campaign_created !== null) {
      console_log('Delete Facebook campaign created...');
      await delete_fb_campaign(campaigns_created.fb_campaign_created, fb_access_token);
    }
    if (campaigns_created.sp_campaign_created !== null) {
      console_log('Delete Snapchat campaign created...');
      await delete_sp_campaign(campaigns_created.sp_campaign_created, sp_access_token);
    }
    if (campaigns_created.tk_campaign_created !== null) {
      console_log('Delete Tiktok campaign created...');
      await update_tk_status_campaign('DELETE', campaigns_created.tk_campaign_created, tk_ad_account_id, tk_access_token);
    }
    if (campaigns_created.tw_campaign_created !== null) {
      console_log('Delete Twitter campaign created...');
      await delete_tw_campaign(
        campaigns_created.tw_campaign_created,
        tw_ad_account_id,
        tw_access_token,
      );
    }

    console_log('Retroaction done...');

    return true;
  } catch (e) {
    console_log('Error in retroaction module.');
    throw e;
  }
};

export default retroaction;
