// RESSOURCES
import console_log from './console_log.js';

const header_from = async (req) => {
  try {
    const isAirtable = req.headers.hasOwnProperty('x-airtable-source');
    const isPostman = req.headers.hasOwnProperty('postman-token');

    let from = 'unknow';

    if (isAirtable) {
      if (
        req.headers['x-airtable-source']
        === 'appiIZMzM8oMyTzTq/bliyNxoPdcJKP2llV'
      ) {
        from = 'Request send from Table DEV Airtable';
      } else if (
        req.headers['x-airtable-source']
        === 'appohR6BsGlAlWkWE/bliEmpIRTKGyC56o9'
      ) {
        from = 'Request send from Table TEST Airtable';
      } else {
        from = 'Request send from Table PROD Airtable';
      }
    } else if (isPostman) {
      from = 'Alex request from local postman tool';
    } else {
      from = req.headers.origin;
    }

    return from;
  } catch (e) {
    console_log('Error in header_from module.');
    throw e;
  }
};

export default header_from;
