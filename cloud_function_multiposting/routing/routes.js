const routes = [
  {
    method: 'GET',
    param: 'multiposting',
    queries: ['at_table_id', 'at_record_id', 'at_access_token'],
  },
];

export default routes;
