// SERVICES
import routes from './routes.js';
import multiposting from '../services/multiposting/multiposting.js';

// RESSOURCES
import console_log from '../ressources/console_log.js';

const router = async (req) => {
  try {
    // Check if we have a method with param in routes
    const route_requested = routes.filter((route) => {
      if (
        route.method === req.method
        && Object.keys(req.params).length === 1
        && req.params['0'] === route.param
      ) {
        return route;
      }
      return false;
    });

    // Error if no route found
    if (route_requested.length === 0) {
      throw {
        expose: true,
        error_detail: `Route ${req.method}: /${req.params['0']} is not defined`,
      };
    }
    // Error if route_requested is more than 1
    if (route_requested.length > 1) {
      throw {
        expose: false,
        error_detail: `Several route_requested found: ${route_requested}`,
      };
    }

    // Check parameters (queries)
    if (route_requested[0].method === 'GET') {
      // Check if number parameter is correct
      if (Object.keys(req.query).length !== route_requested[0].queries.length) {
        throw {
          expose: true,
          error_detail: `Schema parameters for route ${req.method} /${req.params['0']} is not respected`,
        };
      }

      // Check for each param if it undefined or empty
      const params_check = route_requested[0].queries.filter(
        (query) => !req.query[query] || req.query[query] === '',
      );

      // Error if params_check is not good
      if (params_check.length !== 0) {
        console_log('Error with parameters: ', params_check.join());
        throw {
          expose: true,
          error_detail: `Missing or wrong parameters for route ${req.method} /${req.params['0']}`,
        };
      }

      console_log(
        `Route requested ${req.method} /${req.params['0']}`,
        req.query,
      );
    } else {
      throw {
        expose: true,
        error_detail: 'You\'re tring forbidden method request..',
      };
    }

    let job;

    try {
      switch (route_requested[0].param) {
        case 'multiposting':
          job = await multiposting(
            req.query.at_table_id,
            req.query.at_record_id,
            req.query.at_access_token,
          );
          break;
        default:
          throw {
            expose: false,
            error_detail: `${route_requested.param} not found in switch router`,
          };
      }
    } catch (e) {
      console_log('Error in switch routing module.');
      throw e;
    }

    return job;
  } catch (e) {
    console_log('Error in routing module.');
    throw e;
  }
};

export default router;
